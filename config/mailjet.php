<?php 

// config/mailjet.php
return [
    'key' => env('MAIL_MAILJET_KEY'),
    'secret' => env('MAIL_MAILJET_SECRET'),
    'transactional' => [
        'call' => true,
        'options' => [
            'version' => 'v3.1',
            'call' => true,
            'secured' => true,
        ],
    ],
    'common' => [
        'call' => true,
        'options' => [
            'version' => 'v3.1',
            'call' => true,
            'secured' => true,
        ],
    ],
];
