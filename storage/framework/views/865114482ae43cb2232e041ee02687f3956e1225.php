<html
  lang="en"
  class="light-style layout-menu-fixed"
  dir="ltr"
  data-theme="theme-default"
  data-assets-path="<?php echo e(asset('assets_admin')); ?>/"
  data-template="vertical-menu-template-free"
>
  <head>
    <meta charset="utf-8" />
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0"
    />

    <title><?php echo e(@$title ?? 'Dashboard Admin'); ?></title>

    <meta name="description" content="" />
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
    <!-- Dalam template Blade Laravel -->
    <meta name="base_url" content="<?php echo e(config('app.url')); ?>">

    <!-- Favicon -->
    <link rel="icon" type="image/x-icon" href="<?php echo e(asset('assets_admin')); ?>/img/favicon/favicon.ico" />

    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />

    <link rel="stylesheet" href="<?php echo e(asset('assets_admin')); ?>/datatable/datatable.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css">
    
    <link rel="stylesheet" href="<?php echo e(asset('assets_admin')); ?>/bootstrap/bootstrap.min.css" />

    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    
    <link href="https://fonts.googleapis.com/css2?family=Public+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&display=swap" rel="stylesheet"/>
    <!-- Icons. Uncomment required icon fonts -->
    <link rel="stylesheet" href="<?php echo e(asset('assets_admin')); ?>/vendor/fonts/boxicons.css" />
    <!-- Core CSS -->
    <link rel="stylesheet" href="<?php echo e(asset('assets_admin')); ?>/vendor/css/core.css" class="template-customizer-core-css" />
    <link rel="stylesheet" href="<?php echo e(asset('assets_admin')); ?>/vendor/css/theme-default.css" class="template-customizer-theme-css" />
    <link rel="stylesheet" href="<?php echo e(asset('assets_admin')); ?>/css/demo.css" />
    
    <link rel="stylesheet" href="<?php echo e(asset('assets_admin')); ?>/select2/select2.min.css" />
    <link rel="stylesheet" href="<?php echo e(asset('assets_admin')); ?>/select2/select2-bootstrap.min.css" />
    

    <!-- Vendors CSS -->
    <link rel="stylesheet" href="<?php echo e(asset('assets_admin')); ?>/vendor/libs/perfect-scrollbar/perfect-scrollbar.css" />

    <link rel="stylesheet" href="<?php echo e(asset('assets_admin')); ?>/vendor/libs/apex-charts/apex-charts.css" />

    
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/2.0.1/css/buttons.dataTables.min.css">
    <!-- Page CSS -->

    <!-- Helpers -->
    <script src="<?php echo e(asset('assets_admin')); ?>/vendor/js/helpers.js"></script>
    <script src="<?php echo e(asset('assets_admin')); ?>/vendor/libs/jquery/jquery.js"></script>
    <script src="<?php echo e(asset('assets_admin')); ?>/datatable/datatable.min.js"></script>
    <script src="<?php echo e(asset('assets_admin')); ?>/select2/select2.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.0.1/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.0.1/js/buttons.print.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.0.1/js/buttons.html5.min.js"></script>
    

    


    <!--! Template customizer & Theme config files MUST be included after core stylesheets and helpers.js in the <head> section -->
    <!--? Config:  Mandatory theme config file contain global vars & default theme options, Set your preferred theme option in this file.  -->
    <script src="<?php echo e(asset('assets_admin')); ?>/js/config.js"></script>
  </head><?php /**PATH D:\application\agratha\resources\views/admin/template/head.blade.php ENDPATH**/ ?>