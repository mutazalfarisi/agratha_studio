

<?php $__env->startSection('content'); ?>
    <!-- Begin Page Content -->
    <div class="container-fluid">
        <!-- Page Heading -->
        <div class="mb-4 d-sm-flex align-items-center justify-content-between">
            <h1 class="mb-0 text-gray-800 h3"><?php echo e(@$page_title); ?></h1>
            <a href="<?php echo e(url('/admin/alamat_toko')); ?>" class="shadow-sm d-none d-sm-inline-block btn btn-sm btn-primary">
                <i class="text-gray-400 fas fa-user fa-sm fa-fw"></i> Alamat Toko</a>
        </div>

        <!-- Content Row -->
        <div class="row">
            
            <div class="mb-4 col-xl-3 col-md-6">
                <div class="py-2 shadow card border-left-primary h-100">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="mr-2 col">
                                <div class="mb-1 text-xs font-weight-bold text-primary text-uppercase">
                                    Transaksi Bulan Ini</div>
                                <div class="mb-0 text-gray-800 h5 font-weight-bold"><?php echo e($transaksi_bulan_ini); ?></div>
                            </div>
                            <div class="col-auto">
                                <i class="text-gray-300 fas fa-calendar fa-2x"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Earnings (Monthly) Card Example -->
            <div class="mb-4 col-xl-3 col-md-6">
                <div class="py-2 shadow card border-left-success h-100">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="mr-2 col">
                                <div class="mb-1 text-xs font-weight-bold text-success text-uppercase">
                                    Transaksi Menunggu Validasi</div>
                                <div class="mb-0 text-gray-800 h5 font-weight-bold"><?php echo e($menunggu_validasi); ?></div>
                            </div>
                            <div class="col-auto">
                                <i class="text-gray-300 fas fa-dollar-sign fa-2x"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Earnings (Monthly) Card Example -->
            <div class="mb-4 col-xl-3 col-md-6">
                <div class="py-2 shadow card border-left-info h-100">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="mr-2 col">
                                <div class="mb-1 text-xs font-weight-bold text-info text-uppercase">
                                    Pemasukan
                                </div>
                                <div class="row no-gutters align-items-center">
                                    <div class="col-auto">
                                        <div class="mb-0 mr-3 text-gray-800 h5 font-weight-bold">Rp <?php echo e(rupiah($pemasukan)); ?>

                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="col-auto">
                                <i class="text-gray-300 fas fa-clipboard-list fa-2x"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Pending Requests Card Example -->
            <div class="mb-4 col-xl-3 col-md-6">
                <div class="py-2 shadow card border-left-warning h-100">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="mr-2 col">
                                <div class="mb-1 text-xs font-weight-bold text-warning text-uppercase">
                                    Produk Perlu Di Restok</div>
                                <div class="mb-0 text-gray-800 h5 font-weight-bold"><?php echo e($restok); ?></div>
                            </div>
                            <div class="col-auto">
                                <i class="text-gray-300 fas fa-store-slash fa-2x"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Content Row -->

        <div class="row">
            <!-- Area Chart -->
            <div class="col-xl-8 col-lg-7">
                <div class="mb-4 shadow card">
                    <!-- Card Header - Dropdown -->
                    <div class="flex-row py-3 card-header d-flex align-items-center justify-content-between">
                        <h6 class="m-0 font-weight-bold text-primary">Transaksi Bulan ini</h6>
                    </div>
                    <!-- Card Body -->
                    <div class="card-body">
                        <a href="<?php echo e(url('admin/cetak/transaksi_bulan')); ?>" target="_blank" class="btn btn-sm btn-primary">Cetak Rekap</a>
                        <table class="table table-bordered" id="table-transaksi">
                            <thead>
                                <th>No</th>
                                <th>Nama</th>
                                <th>Total</th>
                            </thead>
                            <tbody>
                                <?php
                                    $total = 0;
                                ?>
                                <?php $__currentLoopData = $dt_pemasukan; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr>
                                        <td><?php echo e($loop->iteration); ?></td>
                                        <td><?php echo e($item->name); ?></td>
                                        <td>Rp <?php echo e(rupiah($item->harga_total)); ?></td>
                                    </tr>
                                    <?php
                                        $total += $item->harga_total;
                                    ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                            <tfoot>
                                <th colspan="2">Total</th>
                                <th>Rp <?php echo e(rupiah(@$total)); ?></th>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>

            <!-- Pie Chart -->
            <div class="col-xl-4 col-lg-5">
                <div class="mb-4 shadow card">
                    <!-- Card Header - Dropdown -->
                    <div class="flex-row py-3 card-header d-flex align-items-center justify-content-between">
                        <h6 class="m-0 font-weight-bold text-primary">Produk Perlu Restok</h6>
                    </div>
                    <!-- Card Body -->
                    <div class="card-body">
                        <table class="table table-bordered" id="table-restok">
                            <thead>
                                <th>No</th>
                                <th>Nama Barang</th>
                            </thead>
                            <tbody>
                                <?php $__currentLoopData = $dt_restok; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr>
                                        <td><?php echo e($loop->iteration); ?></td>
                                        <td><?php echo e($item->nama_produk); ?></td>
                                    </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- Content Row -->
    </div>
    <!-- End of Main Content -->

    <script>
        $(document).ready(function() {
            $("#table-transaksi").DataTable({
                responsive: true
            });
            $("#table-restok").DataTable({
                responsive: true
            });
        })
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.template.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\application\basecampidn\resources\views/admin/dashboard.blade.php ENDPATH**/ ?>