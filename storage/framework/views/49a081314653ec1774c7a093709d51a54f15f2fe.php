

<?php $__env->startSection('content'); ?>
    <!-- Begin Page Content -->
    <div class="container-fluid">
        <!-- Page Heading -->
        <!-- Content Row -->
        <div class="row">
            <div class="col-12">
                <div class="row">
                    <div class="card" style="width: 100%;">
                        <div class="p-5 card-body ">
                            <?php if($errors->any()): ?>
                                <div class="alert alert-danger">
                                    <ul>
                                        <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <li><?php echo e($error); ?></li>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </ul>
                                </div>
                            <?php endif; ?>
                            <form action="<?php echo e(route('simpan_paket')); ?>" enctype="multipart/form-data" method="post">
                                <?php echo csrf_field(); ?>
                                <?php if(@$id): ?>
                                    <input type="hidden" name="id" value="<?php echo e($id); ?>">
                                <?php endif; ?>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="mb-3">
                                            <label for="paket" class="form-label">Nama Paket</label>
                                            <input type="text" class="form-control" name="paket" id="paket"
                                                value="<?php echo e(@$paket->nama_paket); ?>" placeholder="Masukkan Nama Paket">
                                        </div>
                                        <div class="mb-3">
                                            <label for="deskripsi" class="form-label">Deskripsi Paket</label>
                                            <textarea name="deskripsi" class="form-control" name="deskripsi" id="deskripsi" rows="3" placeholder="Masukkan Deskripsi Paket"><?php echo e(@$paket->deskripsi); ?></textarea>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <?php if(!empty(@$paket->foto)): ?>
                                            <a href="<?php echo e(asset('storage') . '/' . @$paket->foto); ?>" target="_blank"
                                                class="mb-3 shadow-sm d-none d-sm-inline-block btn btn-sm btn-success">Lihat
                                                Foto Sebelumnya</a>
                                        <?php endif; ?>
                                        <div class="mb-3">
                                            <label for="foto" class="form-label">Upload Foto</label>
                                            <input type="file" class="form-control" name="foto" id="foto" accept="image/*">
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="mb-3">
                                            <label for="harga" class="form-label">Harga</label>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-3">
                                                <span class="form-label">Sangat Mendesak : </span>
                                                <input type="text" class="form-control isNumber" name="sangat_mendesak" id="sangat_mendesak"
                                                    value="<?php echo e(@$paket->sangat_mendesak); ?>" placeholder="Rp">
                                            </div>
                                            <div class="col-lg-3">
                                                <span class="form-label">Mendesak : </span>
                                                <input type="text" class="form-control isNumber" name="mendesak" id="mendesak"
                                                    value="<?php echo e(@$paket->mendesak); ?>" placeholder="Rp">
                                            </div>
                                            <div class="col-lg-3">
                                                <span class="form-label">Biasa : </span>
                                                <input type="text" class="form-control isNumber" name="biasa" id="biasa"
                                                    value="<?php echo e(@$paket->biasa); ?>" placeholder="Rp">
                                            </div>
                                            <div class="col-lg-3">
                                                <span class="form-label">Tidak Mendesak : </span>
                                                <input type="text" class="form-control isNumber" name="tidak_mendesak" id="tidak_mendesak"
                                                    value="<?php echo e(@$paket->tidak_mendesak); ?>" placeholder="Rp">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="mt-3">
                                    <a href="<?php echo e(route('paket')); ?>" class="shadow-sm d-none d-sm-inline-block btn btn-sm btn-danger">Kembali</a>
                                    <button type="submit" class="shadow-sm d-none d-sm-inline-block btn btn-sm btn-primary">Simpan</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End of Main Content -->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.template.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\application\agratha\resources\views/admin/paket/tambah.blade.php ENDPATH**/ ?>