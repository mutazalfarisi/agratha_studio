

<?php $__env->startSection('content'); ?>
    <!-- Begin Page Content -->
    <div class="container-fluid">
        <!-- Page Heading -->
        <div class="mb-4 d-sm-flex align-items-center justify-content-between">
            <h1 class="mb-0 text-gray-800 h3"><?php echo e(@$page_title); ?></h1>
            <a href="<?php echo e(url('admin/kelola/kaos')); ?>" class="shadow-sm d-none d-sm-inline-block btn btn-sm btn-danger">
                <i class="text-white fas fa-arrow-left fa-sm fa-fw"></i> Kembali</a>
        </div>
        <!-- Content Row -->
        <div class="row">
            <div class="col-12">
                <div class="row">
                    <div class="card" style="width: 100%;">
                        <div class="p-5 card-body ">
                            <?php if($errors->any()): ?>
                                <div class="alert alert-danger">
                                    <ul>
                                        <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <li><?php echo e($error); ?></li>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </ul>
                                </div>
                            <?php endif; ?>
                            <form action="<?php echo e(url('admin/simpan/produk')); ?>" enctype="multipart/form-data" method="post">
                                <?php echo csrf_field(); ?>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="mb-3">
                                            <label for="produk" class="form-label">Nama Produk</label>
                                            <input type="text" class="form-control" name="produk" id="produk" value="<?php echo e(@$produk->nama_produk); ?>"
                                                placeholder="Masukkan Nama Produk">
                                            <?php if(!empty(@$produk->id)): ?>
                                                <input type="hidden" name="id" value="<?php echo e(@$produk->id); ?>">
                                            <?php endif; ?>
                                        </div>
                                        <div class="mb-3">
                                            <label for="deskripsi" class="form-label">Deskripsi Produk</label>
                                            <textarea name="deskripsi" class="form-control" name="deskripsi" id="deskripsi" rows="3"><?php echo e(@$produk->deskripsi); ?></textarea>
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="mb-3">
                                            <label for="harga" class="form-label">Harga Produk</label>
                                            <input type="text" class="form-control" name="harga" id="harga" value="<?php echo e(@$produk->harga); ?>" placeholder="Rp">
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="mb-3">
                                            <label for="kategori" class="form-label">Kategori Produk</label>
                                            <select name="kategori" id="kategori" class="form-control">
                                                <option value="">-- Pilih Kategori --</option>
                                                <?php $__currentLoopData = $ref_kategori; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($item->id); ?>" <?php echo e(@$produk->id_kategori == $item->id ? 'selected' : ''); ?>><?php echo e($item->kategori); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="mb-3">
                                            <label for="stok" class="form-label">Stok</label>
                                            <input type="text" class="form-control" name="stok" id="stok" value="<?php echo e(@$produk->stok); ?>" placeholder="Masukkan Stok">
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <?php if(!empty(@$produk->foto)): ?>
                                            
                                            <a href="<?php echo e(asset('storage') .'/'. @$produk->foto); ?>" target="_blank" class="mb-3 shadow-sm d-none d-sm-inline-block btn btn-sm btn-success">Lihat
                                                Foto Sebelumnya</a>
                                        <?php endif; ?>
                                        <div class="mb-3">
                                            <label for="foto" class="form-label">Upload Foto</label>
                                            <input type="file" class="form-control" name="foto" id="foto">
                                        </div>
                                    </div>
                                    
                                </div>
                                <button type="submit" class="shadow-sm d-none d-sm-inline-block btn btn-sm btn-primary">Simpan</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End of Main Content -->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.template.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\application\basecampidn\resources\views/admin/kelola/tambah_produk.blade.php ENDPATH**/ ?>