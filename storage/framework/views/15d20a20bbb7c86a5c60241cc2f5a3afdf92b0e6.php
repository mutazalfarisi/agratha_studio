                <table class="table table-bordered" id="table-detail">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th>Ukuran</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $__currentLoopData = $detail; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <td><?php echo e($loop->iteration); ?></td>
                                <td><?php echo e($item->nama_produk); ?></td>
                                <td><?php echo e($item->ukuran); ?></td>
                            </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </tbody>
                </table>
<?php /**PATH D:\application\basecampidn\resources\views/admin/keuangan/modal_detail.blade.php ENDPATH**/ ?>