<div class="p-4">
    <table class="table table-bordered">
        <thead>
            <tr>
                <th>No</th>
                <th>Nama Barang</th>
                <th>Gambar</th>
                <th>Ukuran</th>
                <th>Jumlah</th>
                <th>Ongkir</th>
                <th>Harga Total</th>
            </tr>
        </thead>
        <tbody>
            <?php
                $no = 1;
            ?>

            <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr>
                    <td style="width: 5%;"><?php echo e($no++); ?></td>
                    <td style="width: 20%;"><?php echo e($item->nama_produk); ?></td>
                    <td style="width: 20%;text-align:center;"><img src="<?php echo e(asset('storage/' . $item->foto)); ?>"style="width:70%;"></td>
                    <td style="width: 5%;"><?php echo e($item->ukuran); ?></td>
                    <td style="width: 5%;"><?php echo e($item->jml_barang); ?></td>
                    <td style="width: 20%;">Rp <?php echo e(rupiah($item->harga_ongkir)); ?></td>
                    <td style="width: 20%;">Rp <?php echo e(rupiah($item->harga_total)); ?></td>
                </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </tbody>
    </table>
</div>
<?php /**PATH D:\application\basecampidn\resources\views/admin/transaksi/detail_transaksi.blade.php ENDPATH**/ ?>