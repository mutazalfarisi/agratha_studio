

<?php $__env->startSection('content'); ?>
    <!-- Begin Page Content -->
    <div class="container-fluid">
        <!-- Page Heading -->
        <div class="mb-4 d-sm-flex align-items-center justify-content-between">
            <h1 class="mb-0 text-gray-800 h3"><?php echo e(@$page_title); ?></h1>
            
        </div>

        <!-- Content Row -->
        <div class="card">
            <div class="card-body">
                <form action="<?php echo e(route('pemasukan_filter')); ?>" method="get">
                    <?php echo csrf_field(); ?>
                    <div class="row">
                        <div class="col-12 col-md-4">
                            <div class="form-group">
                                <label for="tanggal_mulai">Tanggal Mulai</label>
                                <input type="date" class="form-control" name="tanggal_mulai" value="<?php echo e(@$filter['mulai']); ?>">
                            </div>
                        </div>
                        <div class="col-12 col-md-4">
                            <div class="form-group">
                                <label for="tanggal_selesai">Tanggal Selesai</label>
                                <input type="date" class="form-control" name="tanggal_selesai" value="<?php echo e(@$filter['selesai']); ?>">
                            </div>
                        </div>
                        <div class="col-12 col-md-4 align-items-end d-flex">
                            <div class="form-group me-3">
                                <button type="submit" class="btn btn-success">Terapkan</button>
                            </div>
                            <div class="form-group">
                                <a href="<?php echo e(route('pemasukan')); ?>" class="btn btn-outline-primary">Reset</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <div class="mt-3 card" style="width: 100%;">
            <div class="card-body">
                <div class="col-12">
                    <div class="row">
                        <?php if(session('success')): ?>
                            <div class="alert alert-success">
                                <?php echo e(session('success')); ?>

                            </div>
                        <?php endif; ?>

                        <?php if(session('error')): ?>
                            <div class="alert alert-danger">
                                <?php echo e(session('error')); ?>

                            </div>
                        <?php endif; ?>

                        <div class="table-responsive">
                            <a href="<?php echo e(url('admin/cetak/cetak_transaksi?tgl_mulai=' . urlencode(@$filter['mulai']) . '&tgl_selesai=' . urlencode(@$filter['selesai']))); ?>" target="_blank" class="btn btn-sm btn-primary">Cetak Rekap</a>

                            
                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Tanggal</th>
                                        <th>Nama Customer</th>
                                        <th>Detail</th>
                                        <th>Total</th>
                                        <th class="text-center">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $no = 1;
                                    ?>
                                    <?php $__currentLoopData = $pemasukan; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <tr>
                                            <td style="width:5%!important;"><?php echo e($no++); ?></td>
                                            <td style="width:20%!important;"><?php echo e(tanggal_indonesia($row->tgl_transaksi)); ?>

                                            </td>
                                            <td style="width:25%!important;"><?php echo e(@$row->name); ?></td>
                                            <td style="width:10%!important;">
                                                <button type="button" class="btn btn-sm btn-primary"
                                                    onclick="show_detail('<?php echo e($row->id); ?>')">
                                                    Lihat Detail
                                                </button>
                                            </td>
                                            <td style="width:20%!important;">Rp. <?php echo e(rupiah(@$row->harga_total)); ?>

                                            </td>
                                            <td class="text-center" style="width:10%!important;">
                                                
                                                <a
                                                    href="<?php echo e(url('/cetak/transaksi') . '/' . $row->id); ?>" target="_blank" class="shadow-sm d-none d-sm-inline-block btn btn-sm btn-warning"><i
                                                        class="text-white fas fa-print fa-sm fa-fw"></i></a>
                                            </td>
                                        </tr>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal_detail" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Detail Barang</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="modal-content">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.template.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\application\basecampidn\resources\views/admin/keuangan/pemasukan.blade.php ENDPATH**/ ?>