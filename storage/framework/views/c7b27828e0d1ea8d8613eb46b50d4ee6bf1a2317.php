

<?php $__env->startSection('content'); ?>
    <!-- Begin Page Content -->
    

        <!-- Page Heading -->
        
        <!-- Content Row -->
        <div class="row">
            <div class="mb-4 col-lg-12 order-0">
              <?php if(session('success')): ?>
                    <div class="alert alert-primary">
                        <?php echo e(session('success')); ?>

                    </div>
                    <?php endif; ?>
                    
                    <?php if(session('error')): ?>
                    <div class="alert alert-danger">
                        <?php echo e(session('error')); ?>

                    </div>
                    <?php endif; ?>
                    
                    <?php if($errors->any()): ?>
                    <div class="alert alert-danger">
                        <ul>
                            <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <li><?php echo e($error); ?></li>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </ul>
                    </div>
                </div>
            <?php endif; ?>
            <div class="mb-4 col-lg-12 order-0">
                <div class="card">
                    <div class="card-body">
                        <div class="p-3 pt-0 mb-3 justify-content-end d-flex">
                            <button class="shadow-sm d-none d-sm-inline-block btn btn-sm btn-primary" id="btn-tambah">
                                Tambah Admin
                            </button>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-fixed table-bordered" id="dataTable" width="100%"
                                cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Username</th>
                                        <th>Nama</th>
                                        <th>Email</th>
                                        <th>Nomor HP</th>
                                        
                                        <th class="text-center">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $no = 1;
                                    ?>
                                    <?php $__currentLoopData = $dt_user; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <tr>
                                            <td style="width:5%!important;"><?php echo e($no++); ?></td>
                                            <td style="width:20%!important;"><?php echo e($row->username); ?></td>
                                            <td style="width:20%!important;">
                                                <?php echo e($row->nama); ?>

                                                
                                            </td>
                                            <td><?php echo e($row->email); ?></td>
                                            <td><?php echo e($row->no_hp); ?></td>
                                            
                                            <td class="text-center" style="width:10%!important;">
                                                <?php if($jml > 1): ?>
                                                    <button
                                                        class="p-2 shadow-sm d-none d-sm-inline-block btn btn-sm btn-danger"
                                                        onclick="hapus(<?php echo e($row->id); ?>)"><i class="text-white fas fa-trash fa-sm fa-fw"></i></button>
                                                <?php endif; ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="modal-tambah" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <form action="<?php echo e(route('tambah_admin')); ?>" method="post">
                    <?php echo csrf_field(); ?>
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Tambah Admin</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="username"><b>Username</b></label>
                                    <input type="text" class="form-control" placeholder="Masukkan Username" name="username">
                                    <input type="hidden" class="form-control" placeholder="Masukkan Username" name="id" value="">
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="nama"><b>Nama</b></label>
                                    <input type="text" class="form-control" placeholder="Masukkan Nama" name="nama">
                                </div>
                            </div>
                        </div>
                        <div class="mt-3 row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="email"><b>Email</b></label>
                                    <input type="text" class="form-control" placeholder="Masukkan Email" name="email">
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="password"><b>Password</b></label>
                                    <input type="text" class="form-control" placeholder="Masukkan Password" name="password">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- End of Main Content -->
    <script src="<?php echo e(asset('admin/users/admin.js')); ?>"></script>  
    <script>
        $(document).ready(function(){
            $("#dataTable").DataTable()
        })
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.template.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\application\agratha\resources\views/admin/users/admin.blade.php ENDPATH**/ ?>