

<?php $__env->startSection('content'); ?>
    <div class="content">
        <div class="hero__content d-flex justify-content-center align-items-center" style="min-height: 200px;" id="home">
            <div class="title-hero">
                <h1 class="text-center fw-bold" style="vertical-align: middle;">Detail Kaos.</h1>
            </div>
        </div>

        <div class="p-5 main-product">
            <div class="content__product" id="tshirt">
                <div class="row">
                    <div class="col-6">
                        <img src="<?php echo e(asset('storage') . '/' . @$dt_produk->foto); ?>" class="img__detail__product"
                            style="" alt="">
                    </div>
                    <div class="col-6">
                        <div class="about-produk">
                            <div class="title-produk">
                                <span><?php echo e(@$dt_produk->nama_produk); ?></span>
                            </div>
                            <div class="price-produk">
                                <span>Rp. <?php echo e(@$dt_produk->harga); ?></span>
                            </div>
                            <div class="mt-2 deskripsi-produk">
                                <span><?php echo e(@$dt_produk->deskripsi); ?></span>
                            </div>
                            <div class="mt-3 ukuran">
                                <span>Pilih Ukuran</span>
                                <select name="ukuran" id="ukuran" class="form-control" required>
                                    <?php $__currentLoopData = @$ref_ukuran; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($item->id); ?>"><?php echo e($item->ukuran); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                            </div>
                            <div class="mt-2 jumlah">
                                <span>Jumlah Produk</span>
                                <input type="number" class="form-control" name="jumlah" id="jumlah" value="1" required>
                            </div>
                            <div class="mt-3 tombol__produk">
                                <?php if(Auth::user() == null): ?>
                                    <a href="<?php echo e(url('login')); ?>"><button class="btn-cstm-green"><i class="fa-solid fa-cart-shopping"></i>Tambah
                                        Keranjang</button></a>
                                <?php else: ?>
                                    <button class="btn-cstm-green" data-id_produk="<?php echo e(@$dt_produk->id); ?>" id="tambah_keranjang"><i class="fa-solid fa-cart-shopping"></i>Tambah
                                        Keranjang</button>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('front.layout.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\application\basecampidn\resources\views/front/produk/detail_kaos.blade.php ENDPATH**/ ?>