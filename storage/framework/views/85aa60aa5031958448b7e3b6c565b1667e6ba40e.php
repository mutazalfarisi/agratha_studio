<style>
  /* .menu-sub .menu-link{
    background-color: #102c57;
  } */
</style>

<body>
    <!-- Layout wrapper -->
    <div class="layout-wrapper layout-content-navbar">
      <div class="layout-container">
        <!-- Menu -->

        <aside id="layout-menu" class="layout-menu menu-vertical menu bg-menu-theme">
          <div class="app-brand demo">
            <a href="index.html"><span class="app-brand-text demo menu-text fw-bolder ms-2">Agratha Studio</span></a>
            

            <a href="javascript:void(0);" class="layout-menu-toggle menu-link text-large ms-auto d-block d-xl-none">
              <i class="align-middle bx bx-chevron-left bx-sm"></i>
            </a>
          </div>

          

          <ul class="py-1 menu-inner">
            <!-- Dashboard -->
            <li class="menu-item <?php echo e(@$li_active == 'dashboard' ? 'active' : ''); ?>">
              <a href="<?php echo e(url('/admin/dashboard')); ?>" class="menu-link">
                <i class="menu-icon tf-icons bx bx-home-circle"></i>
                <div data-i18n="Analytics">Dashboard</div>
              </a>
            </li>

            <!-- Layouts -->
            

            <li class="menu-header small text-uppercase">
              <span class="menu-header-text">Pemesanan</span>
            </li>
            
            

            <li class="menu-item <?php echo e(@$li_active == 'daftar_pemesanan' ? 'active open' : ''); ?>">
              <a href="javascript:void(0);" class="menu-link menu-toggle">
                <i class="menu-icon tf-icons bx bx-cart"></i>
                <div data-i18n="Daftar Pemesanan">Daftar Pemesanan</div>
              </a>
              <ul class="menu-sub">
                <li class="menu-item <?php echo e(@$li_sub_active == 'verifikasi' ? 'active' : ''); ?>">
                  <a href="<?php echo e(route('daftar_pemesanan_verifikasi')); ?>" class="menu-link">
                    <div data-i18n="Customer">Verifikasi</div>
                  </a>
                </li>
              </ul>
              <ul class="menu-sub">
                <li class="menu-item <?php echo e(@$li_sub_active == 'belum_verifikasi' ? 'active' : ''); ?>">
                  <a href="<?php echo e(route('daftar_pemesanan_belum_verifikasi')); ?>" class="menu-link">
                    <div data-i18n="Admin">Belum Verifikasi</div>
                  </a>
                </li>
              </ul>
            </li>

            <li class="menu-item <?php echo e(@$li_active == 'laporan' ? 'active' : ''); ?>">
              <a href="<?php echo e(route('laporan_pemesanan')); ?>" class="menu-link">
                <i class="menu-icon tf-icons bx bx-book-content"></i>
                <div data-i18n="Laporan">Daftar Laporan</div>
              </a>
            </li>
            
            <li class="menu-header small text-uppercase">
              <span class="menu-header-text">Manajemen</span>
            </li>
            <li class="menu-item <?php echo e(@$li_active == 'paket' ? 'active' : ''); ?>">
              <a href="<?php echo e(route('paket')); ?>" class="menu-link">
                <i class="menu-icon tf-icons bx bx-book"></i>
                <div data-i18n="Paket">Manajemen Paket</div>
              </a>
            </li>
            <li class="menu-item <?php echo e(@$li_active == 'manajemen_users' ? 'active open' : ''); ?>">
              <a href="javascript:void(0);" class="menu-link menu-toggle">
                <i class="menu-icon tf-icons bx bx-user"></i>
                <div data-i18n="Manajemen User">Manajemen User</div>
              </a>
              <ul class="menu-sub">
                <li class="menu-item <?php echo e(@$li_sub_active == 'daftar_customer' ? 'active' : ''); ?>">
                  <a href="<?php echo e(route('daftar_customer')); ?>" class="menu-link">
                    <div data-i18n="Customer">Customer</div>
                  </a>
                </li>
              </ul>
              <ul class="menu-sub">
                <li class="menu-item <?php echo e(@$li_sub_active == 'daftar_admin' ? 'active' : ''); ?>">
                  <a href="<?php echo e(route('daftar_admin')); ?>" class="menu-link">
                    <div data-i18n="Admin">Admin</div>
                  </a>
                </li>
              </ul>
            </li>


            <li class="menu-header small text-uppercase">
              <span class="menu-header-text">Masukan</span>
            </li>

            <li class="menu-item <?php echo e(@$li_active == 'masukan' ? 'active' : ''); ?>">
              <a href="<?php echo e(route('masukan')); ?>" class="menu-link">
                <i class="menu-icon tf-icons bx bx-book-content"></i>
                <div data-i18n="Masukan">Kritik dan Saran</div>
              </a>
            </li>

            


            

            
            <!-- Misc -->
            
          </ul>
        </aside><?php /**PATH D:\application\agratha\resources\views/admin/template/sidebar.blade.php ENDPATH**/ ?>