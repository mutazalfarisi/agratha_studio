

<?php $__env->startSection('content'); ?>
<div class="content">
    <div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel">
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img src="<?php echo e(asset('images/img1.jpg')); ?>" class="d-block w-100" alt="...">
            </div>
            <div class="carousel-item">
                <img src="<?php echo e(asset('images/img2.jpg')); ?>" class="d-block w-100" alt="...">
            </div>
            <div class="carousel-item">
                <img src="<?php echo e(asset('images/img3.jpg')); ?>" class="d-block w-100" alt="...">
            </div>
            <div class="carousel-item">
                <img src="<?php echo e(asset('images/img4.jpg')); ?>" class="d-block w-100" alt="...">
            </div>
        </div>
        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls"
            data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls"
            data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
        </button>
    </div>
    <div class="p-5 main-product">
        <div class="content__product" id="product_all">
            <div class="text_content_product d-flex justify-content-between">
                <div class="title_content__product" style="font-weight: bold;">
                    <h2 style="font-weight:bold;">Produk Unggulan</h2>
                </div>
                <div class="read_more">
                    <a href="#"><span>Lihat Semua <i class='bx bx-right-arrow-alt'></i></span></a>
                </div>
            </div>
            <div class="gap-3 mt-3 img_content_product owl-carousel d-flex flex-nowrap">
                <div><a href="#" style="text-decoration: none;color:black;">
                        <div class="card__product">
                            <img src="<?php echo e(asset('images/img1.jpg')); ?>" class="img__product"
                                style="" alt="">
                            <div class="p-3 judul_product">
                                <p>Kaos<br>Rp. 60.000,00</p>
                            </div>
                        </div>
                    </a></div>
                <div><a href="#" style="text-decoration: none;color:black;">
                        <div class="card__product">
                            <img src="<?php echo e(asset('images/img1.jpg')); ?>" class="img__product"
                                style="" alt="">
                            <div class="p-3 judul_product">
                                <p>Kaos<br>Rp. 60.000,00</p>
                            </div>
                        </div>
                    </a></div>
                <div><a href="#" style="text-decoration: none;color:black;">
                        <div class="card__product">
                            <img src="<?php echo e(asset('images/img1.jpg')); ?>" class="img__product"
                                style="" alt="">
                            <div class="p-3 judul_product">
                                <p>Kaos<br>Rp. 60.000,00</p>
                            </div>
                        </div>
                    </a></div>
                <div><a href="#" style="text-decoration: none;color:black;">
                        <div class="card__product">
                            <img src="<?php echo e(asset('images/img1.jpg')); ?>" class="img__product"
                                style="" alt="">
                            <div class="p-3 judul_product">
                                <p>Kaos<br>Rp. 60.000,00</p>
                            </div>
                        </div>
                    </a></div>
                <div><a href="#" style="text-decoration: none;color:black;">
                        <div class="card__product">
                            <img src="<?php echo e(asset('images/img1.jpg')); ?>" class="img__product"
                                style="" alt="">
                            <div class="p-3 judul_product">
                                <p>Kaos<br>Rp. 60.000,00</p>
                            </div>
                        </div>
                    </a></div>
                <div><a href="#" style="text-decoration: none;color:black;">
                        <div class="card__product">
                            <img src="<?php echo e(asset('images/img1.jpg')); ?>" class="img__product"
                                style="" alt="">
                            <div class="p-3 judul_product">
                                <p>Kaos<br>Rp. 60.000,00</p>
                            </div>
                        </div>
                    </a></div>
                <div><a href="#" style="text-decoration: none;color:black;">
                        <div class="card__product">
                            <img src="<?php echo e(asset('images/img1.jpg')); ?>" class="img__product"
                                style="" alt="">
                            <div class="p-3 judul_product">
                                <p>Kaos<br>Rp. 60.000,00</p>
                            </div>
                        </div>
                    </a></div>
                <div><a href="#" style="text-decoration: none;color:black;">
                        <div class="card__product">
                            <img src="<?php echo e(asset('images/img1.jpg')); ?>" class="img__product"
                                style="" alt="">
                            <div class="p-3 judul_product">
                                <p>Kaos<br>Rp. 60.000,00</p>
                            </div>
                        </div>
                    </a></div>
                <div><a href="#" style="text-decoration: none;color:black;">
                        <div class="card__product">
                            <img src="<?php echo e(asset('images/img1.jpg')); ?>" class="img__product"
                                style="" alt="">
                            <div class="p-3 judul_product">
                                <p>Kaos<br>Rp. 60.000,00</p>
                            </div>
                        </div>
                    </a></div>
            </div>
        </div>
        <hr>
        <div class="content__product" id="tshirt">
            <div class="text_content_product d-flex justify-content-between">
                <div class="title_content__product" style="font-weight: bold;">
                    <h2 style="font-weight:bold;">Tshirt</h2>
                </div>
                <div class="read_more">
                    <a href="#"><span>Lihat Semua <i class='bx bx-right-arrow-alt'></i></span></a>
                </div>
            </div>
            <div class="gap-3 mt-3 img_content_product owl-carousel d-flex flex-nowrap">
                <div><a href="#" style="text-decoration: none;color:black;">
                        <div class="card__product">
                            <img src="<?php echo e(asset('images/img1.jpg')); ?>" class="img__product"
                                style="" alt="">
                            <div class="p-3 judul_product">
                                <p>Kaos<br>Rp. 60.000,00</p>
                            </div>
                        </div>
                    </a></div>
                <div><a href="#" style="text-decoration: none;color:black;">
                        <div class="card__product">
                            <img src="<?php echo e(asset('images/img1.jpg')); ?>" class="img__product"
                                style="" alt="">
                            <div class="p-3 judul_product">
                                <p>Kaos<br>Rp. 60.000,00</p>
                            </div>
                        </div>
                    </a></div>
                <div><a href="#" style="text-decoration: none;color:black;">
                        <div class="card__product">
                            <img src="<?php echo e(asset('images/img1.jpg')); ?>" class="img__product"
                                style="" alt="">
                            <div class="p-3 judul_product">
                                <p>Kaos<br>Rp. 60.000,00</p>
                            </div>
                        </div>
                    </a></div>
                <div><a href="#" style="text-decoration: none;color:black;">
                        <div class="card__product">
                            <img src="<?php echo e(asset('images/img1.jpg')); ?>" class="img__product"
                                style="" alt="">
                            <div class="p-3 judul_product">
                                <p>Kaos<br>Rp. 60.000,00</p>
                            </div>
                        </div>
                    </a></div>
                <div><a href="#" style="text-decoration: none;color:black;">
                        <div class="card__product">
                            <img src="<?php echo e(asset('images/img1.jpg')); ?>" class="img__product"
                                style="" alt="">
                            <div class="p-3 judul_product">
                                <p>Kaos<br>Rp. 60.000,00</p>
                            </div>
                        </div>
                    </a></div>
                <div><a href="#" style="text-decoration: none;color:black;">
                        <div class="card__product">
                            <img src="<?php echo e(asset('images/img1.jpg')); ?>" class="img__product"
                                style="" alt="">
                            <div class="p-3 judul_product">
                                <p>Kaos<br>Rp. 60.000,00</p>
                            </div>
                        </div>
                    </a></div>
                <div><a href="#" style="text-decoration: none;color:black;">
                        <div class="card__product">
                            <img src="<?php echo e(asset('images/img1.jpg')); ?>" class="img__product"
                                style="" alt="">
                            <div class="p-3 judul_product">
                                <p>Kaos<br>Rp. 60.000,00</p>
                            </div>
                        </div>
                    </a></div>
                <div><a href="#" style="text-decoration: none;color:black;">
                        <div class="card__product">
                            <img src="<?php echo e(asset('images/img1.jpg')); ?>" class="img__product"
                                style="" alt="">
                            <div class="p-3 judul_product">
                                <p>Kaos<br>Rp. 60.000,00</p>
                            </div>
                        </div>
                    </a></div>
                <div><a href="#" style="text-decoration: none;color:black;">
                        <div class="card__product">
                            <img src="<?php echo e(asset('images/img1.jpg')); ?>" class="img__product"
                                style="" alt="">
                            <div class="p-3 judul_product">
                                <p>Kaos<br>Rp. 60.000,00</p>
                            </div>
                        </div>
                    </a></div>
            </div>
        </div>
        <hr>
        <div class="content__product" id="outwear">
            <div class="text_content_product d-flex justify-content-between">
                <div class="title_content__product" style="font-weight: bold;">
                    <h2 style="font-weight:bold;">Outwear</h2>
                </div>
                <div class="read_more">
                    <a href="#"><span>Lihat Semua <i class='bx bx-right-arrow-alt'></i></span></a>
                </div>
            </div>
            <div class="gap-3 mt-3 img_content_product owl-carousel d-flex flex-nowrap">
                <div><a href="#" style="text-decoration: none;color:black;">
                        <div class="card__product">
                            <img src="<?php echo e(asset('images/img1.jpg')); ?>" class="img__product"
                                style="" alt="">
                            <div class="p-3 judul_product">
                                <p>Kaos<br>Rp. 60.000,00</p>
                            </div>
                        </div>
                    </a></div>
                <div><a href="#" style="text-decoration: none;color:black;">
                        <div class="card__product">
                            <img src="<?php echo e(asset('images/img1.jpg')); ?>" class="img__product"
                                style="" alt="">
                            <div class="p-3 judul_product">
                                <p>Kaos<br>Rp. 60.000,00</p>
                            </div>
                        </div>
                    </a></div>
                <div><a href="#" style="text-decoration: none;color:black;">
                        <div class="card__product">
                            <img src="<?php echo e(asset('images/img1.jpg')); ?>" class="img__product"
                                style="" alt="">
                            <div class="p-3 judul_product">
                                <p>Kaos<br>Rp. 60.000,00</p>
                            </div>
                        </div>
                    </a></div>
                <div><a href="#" style="text-decoration: none;color:black;">
                        <div class="card__product">
                            <img src="<?php echo e(asset('images/img1.jpg')); ?>" class="img__product"
                                style="" alt="">
                            <div class="p-3 judul_product">
                                <p>Kaos<br>Rp. 60.000,00</p>
                            </div>
                        </div>
                    </a></div>
                <div><a href="#" style="text-decoration: none;color:black;">
                        <div class="card__product">
                            <img src="<?php echo e(asset('images/img1.jpg')); ?>" class="img__product"
                                style="" alt="">
                            <div class="p-3 judul_product">
                                <p>Kaos<br>Rp. 60.000,00</p>
                            </div>
                        </div>
                    </a></div>
                <div><a href="#" style="text-decoration: none;color:black;">
                        <div class="card__product">
                            <img src="<?php echo e(asset('images/img1.jpg')); ?>" class="img__product"
                                style="" alt="">
                            <div class="p-3 judul_product">
                                <p>Kaos<br>Rp. 60.000,00</p>
                            </div>
                        </div>
                    </a></div>
                <div><a href="#" style="text-decoration: none;color:black;">
                        <div class="card__product">
                            <img src="<?php echo e(asset('images/img1.jpg')); ?>" class="img__product"
                                style="" alt="">
                            <div class="p-3 judul_product">
                                <p>Kaos<br>Rp. 60.000,00</p>
                            </div>
                        </div>
                    </a></div>
                <div><a href="#" style="text-decoration: none;color:black;">
                        <div class="card__product">
                            <img src="<?php echo e(asset('images/img1.jpg')); ?>" class="img__product"
                                style="" alt="">
                            <div class="p-3 judul_product">
                                <p>Kaos<br>Rp. 60.000,00</p>
                            </div>
                        </div>
                    </a></div>
                <div><a href="#" style="text-decoration: none;color:black;">
                        <div class="card__product">
                            <img src="<?php echo e(asset('images/img1.jpg')); ?>" class="img__product"
                                style="" alt="">
                            <div class="p-3 judul_product">
                                <p>Kaos<br>Rp. 60.000,00</p>
                            </div>
                        </div>
                    </a></div>
            </div>
        </div>
        <hr>
        <div class="content__product" id="bag">
            <div class="text_content_product d-flex justify-content-between">
                <div class="title_content__product" style="font-weight: bold;">
                    <h2 style="font-weight:bold;">Bag</h2>
                </div>
                <div class="read_more">
                    <a href="#"><span>Lihat Semua <i class='bx bx-right-arrow-alt'></i></span></a>
                </div>
            </div>
            <div class="gap-3 mt-3 img_content_product owl-carousel d-flex flex-nowrap">
                <div><a href="#" style="text-decoration: none;color:black;">
                        <div class="card__product">
                            <img src="<?php echo e(asset('images/img1.jpg')); ?>" class="img__product"
                                style="" alt="">
                            <div class="p-3 judul_product">
                                <p>Kaos<br>Rp. 60.000,00</p>
                            </div>
                        </div>
                    </a></div>
                <div><a href="#" style="text-decoration: none;color:black;">
                        <div class="card__product">
                            <img src="<?php echo e(asset('images/img1.jpg')); ?>" class="img__product"
                                style="" alt="">
                            <div class="p-3 judul_product">
                                <p>Kaos<br>Rp. 60.000,00</p>
                            </div>
                        </div>
                    </a></div>
                <div><a href="#" style="text-decoration: none;color:black;">
                        <div class="card__product">
                            <img src="<?php echo e(asset('images/img1.jpg')); ?>" class="img__product"
                                style="" alt="">
                            <div class="p-3 judul_product">
                                <p>Kaos<br>Rp. 60.000,00</p>
                            </div>
                        </div>
                    </a></div>
                <div><a href="#" style="text-decoration: none;color:black;">
                        <div class="card__product">
                            <img src="<?php echo e(asset('images/img1.jpg')); ?>" class="img__product"
                                style="" alt="">
                            <div class="p-3 judul_product">
                                <p>Kaos<br>Rp. 60.000,00</p>
                            </div>
                        </div>
                    </a></div>
                <div><a href="#" style="text-decoration: none;color:black;">
                        <div class="card__product">
                            <img src="<?php echo e(asset('images/img1.jpg')); ?>" class="img__product"
                                style="" alt="">
                            <div class="p-3 judul_product">
                                <p>Kaos<br>Rp. 60.000,00</p>
                            </div>
                        </div>
                    </a></div>
                <div><a href="#" style="text-decoration: none;color:black;">
                        <div class="card__product">
                            <img src="<?php echo e(asset('images/img1.jpg')); ?>" class="img__product"
                                style="" alt="">
                            <div class="p-3 judul_product">
                                <p>Kaos<br>Rp. 60.000,00</p>
                            </div>
                        </div>
                    </a></div>
                <div><a href="#" style="text-decoration: none;color:black;">
                        <div class="card__product">
                            <img src="<?php echo e(asset('images/img1.jpg')); ?>" class="img__product"
                                style="" alt="">
                            <div class="p-3 judul_product">
                                <p>Kaos<br>Rp. 60.000,00</p>
                            </div>
                        </div>
                    </a></div>
                <div><a href="#" style="text-decoration: none;color:black;">
                        <div class="card__product">
                            <img src="<?php echo e(asset('images/img1.jpg')); ?>" class="img__product"
                                style="" alt="">
                            <div class="p-3 judul_product">
                                <p>Kaos<br>Rp. 60.000,00</p>
                            </div>
                        </div>
                    </a></div>
                <div><a href="#" style="text-decoration: none;color:black;">
                        <div class="card__product">
                            <img src="<?php echo e(asset('images/img1.jpg')); ?>" class="img__product"
                                style="" alt="">
                            <div class="p-3 judul_product">
                                <p>Kaos<br>Rp. 60.000,00</p>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

    
<?php $__env->stopSection(); ?>
<?php echo $__env->make('front.layout.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\application\marketplace\resources\views/front/home.blade.php ENDPATH**/ ?>