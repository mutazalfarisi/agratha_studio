<?php if(count($data) > 0): ?>
    <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <a href="<?php echo e(url('/detail_kaos/' . $item->id)); ?>">
            <div class="link-dropdown col-12">
                <div class="d-flex">
                    <div style="width:20%;">
                        <img src="<?php echo e(asset('storage') . '/' . $item->foto); ?>" style="width:100%;">
                    </div>
                    <div>
                        <span style="font-size: 12px;font-weight:bold;margin-left:20px;"><?php echo e($item->nama_produk); ?></span>
                    </div>
                </div>
            </div>
        </a>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <div class="text-center link-dropdown col-12">
        <a href="<?php echo e(url('/search/lihat_semua') . '/' . $keyword); ?>" style="font-size: 12px;font-weight:bold;">Lihat
            Semua</a>
    </div>
<?php else: ?>
    <?php if($keyword == '' || $keyword == null): ?>
        <div class="text-center link-dropdown col-12">
            <span style="font-size: 12px;font-weight:bold;">Apa yang sedang anda cari?</span>
        </div>
    <?php else: ?>
        <div class="text-center link-dropdown col-12">
            <div class="text-center fw-bold" style="font-size:12px;">Produk yang anda cari tidak ada</div>
        </div>
    <?php endif; ?>
<?php endif; ?>
<?php /**PATH D:\application\basecampidn\resources\views/front/result_search.blade.php ENDPATH**/ ?>