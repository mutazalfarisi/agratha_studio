

<?php $__env->startSection('content'); ?>
    <div class="content">
        <div class="row" style="padding: 50px;">
            <div class="hero__content d-flex justify-content-center align-items-center" style="min-height: 200px;">
                <div class="title-hero">
                    <h1 class="text-center fw-bold" style="vertical-align: middle;">Profil User</h1>
                </div>
            </div>
            <?php if(session('success')): ?>
                <div class="alert alert-success">
                    <?php echo e(session('success')); ?>

                </div>
            <?php endif; ?>

            <?php if(session('error')): ?>
                <div class="alert alert-danger">
                    <?php echo e(session('error')); ?>

                </div>
            <?php endif; ?>
            <?php if($errors->any()): ?>
                <div class="alert alert-danger">
                    <ul>
                        <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <li><?php echo e($error); ?></li>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </ul>
                </div>
            <?php endif; ?>
            <form action="<?php echo e(route('simpan_profil')); ?>" method="post">
                <?php echo csrf_field(); ?>
                <div class="row">
                    <div class="col-12 col-md-6">
                        <div class="mb-3">
                            <label for="username" class="form-label">Username</label>
                            <input type="text" class="form-control" name="username" id="username"
                                value="<?php echo e(Auth::user()->username); ?>" placeholder="Username">
                        </div>
                    </div>
                    <div class="col-12 col-md-6">
                        <div class="mb-3">
                            <label for="nama" class="form-label">Nama Lengkap</label>
                            <input type="text" class="form-control" name="nama" id="nama"
                                value="<?php echo e(Auth::user()->name); ?>" placeholder="Nama Lengkap">
                        </div>
                    </div>
                    <div class="col-12 col-md-6">
                        <div class="mb-3">
                            <label for="email" class="form-label">Email</label>
                            <input type="text" class="form-control" name="email" id="email"
                                value="<?php echo e(Auth::user()->email); ?>" placeholder="Masukkan Email">
                        </div>
                    </div>
                    <div class="col-12 col-md-6">
                        <div class="mb-3">
                            <label for="no_hp" class="form-label">Nomor HP</label>
                            <input type="text" class="form-control" name="no_hp" id="no_hp"
                                value="<?php echo e(Auth::user()->no_hp); ?>" placeholder="Masukkan Nomor HP">
                        </div>
                    </div>
                    <div class="col-12 col-md-4">
                        <?php if(!empty(@$alamat)): ?>
                            <input type="hidden" name="id_alamat" value="<?php echo e($alamat->id); ?>">
                        <?php endif; ?>
                        <div class="mb-3">
                            <label for="provinsi" class="form-label">Provinsi</label>
                            <select name="provinsi" id="provinsi" class="form-control">
                                <option value="">-- Pilih Provinsi --</option>
                                <?php $__currentLoopData = $ref_provinsi; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $v): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($v->province_id); ?>"
                                        <?php echo e(@$alamat->province_id == $v->province_id ? 'selected' : ''); ?>>
                                        <?php echo e($v->province_name); ?></option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-12 col-md-4">
                        <div class="mb-3">
                            <label for="kabupaten" class="form-label">Kabupaten</label>
                            <select name="kabupaten" id="kabupaten" class="form-control">
                                <?php if(!empty(@$alamat->city_id)): ?>
                                    <option value="<?php echo e($alamat->city_id); ?>"><?php echo e($alamat->kabupaten); ?></option>
                                <?php else: ?>
                                    <option value="">-- Pilih Provinsi Dahulu --</option>
                                <?php endif; ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-12 col-md-4">
                        <div class="mb-3">
                            <label for="kecamatan" class="form-label">Kecamatan</label>
                            <select name="kecamatan" id="kecamatan" class="form-control">
                                <?php if(!empty(@$alamat->subdistrict_id)): ?>
                                    <option value="<?php echo e($alamat->subdistrict_id); ?>"><?php echo e($alamat->kecamatan); ?></option>
                                <?php else: ?>
                                    <option value="">-- Pilih Kecamatan Dahulu --</option>
                                <?php endif; ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-12 col-md-12">
                        <div class="mb-3">
                            <label for="alamat" class="form-label">Alamat</label>
                            <textarea name="alamat" id="alamat" class="form-control" rows="5"><?php echo e(@$alamat->detail_alamat); ?></textarea>
                        </div>
                    </div>
                    <button type="submit" class="mt-4 btn-cstm-blue">Simpan</button>
                </div>
            </form>
        </div>
        <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    </div>
    <script>
        $(document).ready(function() {
            $("#provinsi").select2();
            $("#kabupaten").select2();
            $("#kecamatan").select2();
        })
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('front.layout.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\application\basecampidn\resources\views/customer/profil.blade.php ENDPATH**/ ?>