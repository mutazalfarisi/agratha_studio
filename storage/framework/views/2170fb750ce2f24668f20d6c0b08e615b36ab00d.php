<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.0/jquery.min.js"
        integrity="sha512-3gJwYpMe3QewGELv8k/BX9vcqhryRdzRMxVfq6ngyWXwo03GFEzjsUm8Q7RZcHPHksttq7/GFoxjCVUjkjvPdw=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <link href='https://unpkg.com/boxicons@2.1.4/css/boxicons.min.css' rel='stylesheet'>
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
    <title><?php echo e($title ?? 'Basecampidn.'); ?></title>
    <link rel="stylesheet" href="<?php echo e(asset('css/style.css')); ?>">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="<?php echo e(asset('js/script.js')); ?>"></script>
    <link rel="stylesheet" href="<?php echo e(asset('owlcarousel/dist/assets/owl.carousel.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('owlcarousel/dist/assets/owl.theme.default.min.css')); ?>">
    <script src="https://unpkg.com/boxicons@2.1.4/dist/boxicons.js"></script>
</head>

<body style="overflow-x:hidden;">
    
    <nav class="p-3 shadow-sm navbar navbar-expand-lg navbar-light bg-light fixed-top">
        <div class="container-fluid">
            <a class="ps-4 navbar-brand fw-bold" href="<?php echo e(url('/')); ?>">Basecampidn</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown"
                aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" style="margin-left:100px;font-size:13px;" id="navbarNavDropdown">
                <ul class="navbar-nav nav">
                    <li class="nav-item me-5 <?php echo e(@$li_active == 'home' ? 'active_' : ''); ?>">
                        <a class="nav-link" href="<?php echo e(@$li_active != 'home' ? url('/') : '#home'); ?>">Home</a>
                    </li>
                    <li class="nav-item me-5">
                        <a class="nav-link" href="<?php echo e(@$li_active != 'home' ? url('/') : '#tshirt'); ?>">T-Shirt</a>
                    </li>
                    <li class="nav-item me-5">
                        <a class="nav-link" href="<?php echo e(@$li_active != 'home' ? url('/') : '#bag'); ?>">Bag</a>
                    </li>
                </ul>
            </div>
            <div style="margin-right:100px;">
                <input type="text" class="form-control" placeholder="Search" id="input-search" onkeyup="search()">
            </div>
            <?php if(!empty(Auth::user())): ?>
                <?php if(auth()->user()->role == 'customer'): ?>
                    <div class="cart me-5">
                        <div class="header__menu_item <?php echo e(@$li_active == 'keranjang' ? 'active_header' : ''); ?> ">
                            <a href="<?php echo e(url('/keranjang')); ?>">
                                <div class="jumlah__barang_cart" id="jml_cart">
                                </div>
                                <i class='bx bx-shopping-bag'></i>
                            </a>
                        </div>
                    </div>
                <?php endif; ?>
                <div class="pe-5">
                    <div class="dropdown-cstm" id="profil">
                        <span class="username fw-bold"><?php echo e(Auth::user()->username); ?> <i
                                class='bx bxs-chevron-down'></i></span>
                    </div>
                </div>

                <div class="shadow-sm card-dropdown" id="profil-bar" style="display: none;">
                    <?php if(auth()->user()->role == 'admin'): ?>
                        <div class="p-3 row">
                            <a href="<?php echo e(route('dashboard_admin')); ?>">
                                <div class="link-dropdown col-12">
                                    <i class='bx bxs-dashboard me-2'></i>Dashboard
                                </div>
                            </a>
                        </div>
                    <?php else: ?>
                        <div class="p-3 row">
                            <a href="<?php echo e(route('profil_user')); ?>">
                                <div class="link-dropdown col-12">
                                    <i class='bx bx-user me-2'></i> Profil
                                </div>
                            </a>
                            <a href="<?php echo e(route('logout')); ?>"
                                onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                <div class="link-dropdown col-12">
                                    <i class='bx bx-log-out me-2'></i> Logout
                                </div>
                            </a>
                        </div>
                    <?php endif; ?>

                </div>
                <?php if(auth()->user()->role == 'admin'): ?>
                    <div class="shadow-sm card-dropdown" id="search"
                        style="right: 200px;width: 350px;top: 85px;display:none;">
                    <?php else: ?>
                        <div class="shadow-sm card-dropdown" id="search"
                            style="right: 300px;width: 350px;top: 85px;display:none;">
                <?php endif; ?>
                <div class="p-3 row">
                    <div id="result-search">
                        <div class="text-center link-dropdown col-12">
                            <span style="font-size: 12px;font-weight:bold;">Apa yang sedang anda cari?</span>
                        </div>
                    </div>
                </div>
        </div>

        <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">
            <?php echo csrf_field(); ?>
        </form>
    <?php else: ?>
        <div class="pe-5 account">
            <a href="<?php echo e(url('/login')); ?>"><button class="btn-cstm-blue">Login</button></a>
        </div>
        <?php endif; ?>
        </div>
    </nav>


    <div class="d-flex justify-content-between">
        <div class="main__content col-12">
            <?php echo $__env->yieldContent('content'); ?>
            <div class="px-5 pt-5 footer__item bg-dark text-light">
                <div class="row d-flex">
                    <div class="order-2 col-12 order-lg-1 order-xl-1 col-lg-6 col-xl-6">
                        <div class="contact__us">
                            <h5>Contact Us</h5>
                            <p style="font-size: 12px;">Phone: 08123456789 <br>
                                Email: basecampidn@gmail.com <br>
                                Jl. Anggrek No.40, Kutu Tegal, Sinduadi, Kec. Mlati, Kabupaten Sleman, Daerah Istimewa
                                Yogyakarta 55284</p>
                        </div>
                        <div class="pb-5 menu__footer">
                            <a href="#">Home</a>
                            <a href="#">FAQ</a>
                            <a href="#">Track and Shipping</a>
                        </div>
                        <div class="mt-3 copyright__footer">
                            <p style="font-size: 12px;">© 2023 Basecampidn. All rights reserved.</p>
                        </div>
                    </div>
                    <div class="order-1 col-12 order-lg-2 order-xl-2 col-lg-6 col-xl-6 d-flex justify-content-center">
                        <iframe
                            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3953.328067412893!2d110.36208290981509!3d-7.754986792231696!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e7a59ad7f9e75d3%3A0x52723d3490da8b71!2shomebase%20papagenk!5e0!3m2!1sid!2sid!4v1706626493832!5m2!1sid!2sid"
                            style="border:0;border-radius:20px;width:100%;height:80%" allowscreen="" loading="lazy"
                            referrerpolicy="no-referrer-when-downgrade"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"
        integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js"
        integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous">
    </script>
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.0/jquery.min.js"
        integrity="sha512-3gJwYpMe3QewGELv8k/BX9vcqhryRdzRMxVfq6ngyWXwo03GFEzjsUm8Q7RZcHPHksttq7/GFoxjCVUjkjvPdw=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="<?php echo e(asset('owlcarousel/dist/owl.carousel.min.js')); ?>"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script src="<?php echo e(asset('customer/script.js')); ?>"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <?php if(!empty(@$script_js)): ?>
        <script src="<?php echo e(asset(@$script_js)); ?>"></script>
    <?php endif; ?>
    <script>
        $(document).ready(function() {
            $(".dropdown-cstm").click(function() {
                // $("#profil").fadeToggle(200)
                $("#profil-bar").toggle();
            })

            $("#input-search").click(function() {
                // $("#search").fadeToggle(200);
                $("#search").toggle();
            })

            // $("#input-search").on("focus", function () {
            //     $("#search").fadeIn(200);
            // });

            // $("#input-search").on("blur", function () {
            //     $("#search").fadeOut(200);
            // });
        })

        $(document).on("click", function(event) {
            if (!$(event.target).closest("#profil-bar, #profil").length) {
                $("#profil-bar").hide();
            }
        });

        $("#profil-bar").on("click", function(event) {
            event.stopPropagation();
        });

        $(document).on("click", function(event) {
            if (!$(event.target).closest("#search, #input-search").length) {
                $("#search").hide();
            }
        });

        $("#search").on("click", function(event) {
            event.stopPropagation();
        });

        var delayTimer;

        function search() {
            clearTimeout(delayTimer);
            $("#result-search").html('<div class="text-center fw-bold" style="font-size:12px;">Loading...</div>');
            delayTimer = setTimeout(function() {
                $.ajax({
                    url: '/search-produk',
                    type: 'get',
                    data: {
                        keyword: $("#input-search").val()
                    },
                    success: function(res) {
                        $("#result-search").html(res);
                    }
                })
            }, 500);
        }
        $(document).on('click', '#mobile-menu', function() {
            $('.mobile-navbar').css('transform', 'translateX(0px)');
        })
        $(document).on('click', '#close-mobile-menu', function() {
            $('.mobile-navbar').css('transform', 'translateX(400px)');
        })
    </script>

</body>

</html>
<?php /**PATH D:\application\basecampidn\resources\views/front/layout/default.blade.php ENDPATH**/ ?>