

<?php $__env->startSection('content'); ?>
    <div class="content">
        <div class="container__keranjang">
            <div class="bg-white tab-nav keranjang__navbar">
                <div class="p-4 row">
                    <div class="col-12 col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <span style="font-size: 24px;font-weight:500;">Detail Order</span>
                                <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $dt): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <div class="d-flex" style="margin-top: 30px; padding: 10px;border-radius: 20px;border: 2px solid #ff8038;">
                                        <div>
                                            <img src="<?php echo e(asset('storage') . '/' . $dt->foto); ?>" class="img__product"
                                                style="border-radius:20px;width:100%important;" alt="">
                                        </div>
                                        <div style="width: 100%;">
                                            <div class="p-3 judul_product">
                                                <span style="font-size: 20px;font-weight:500;"><?php echo e(@$dt->nama_produk); ?></span>
                                                    <hr>
                                                <span style="font-size: 12px;"><?php echo e(@$dt->deskripsi); ?></span><br>
                                                <span style="font-size: 18px;" class="rupiah">Rp.
                                                    <?php echo e(rupiah(@$dt->harga)); ?></span><br>
                                                <?php if(@$dt->ukuran != null): ?>
                                                    <span style="font-size: 14px;">Ukuran <?php echo e(@$dt->ukuran); ?></span><br>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6">
                        
                                <div class="alamat-pengirim">
                                    <div class="row" style="padding: 10px;">
                                        <span style="font-size: 14px;font-weight:500;margin-bottom:10px;">Alamat Pengiriman</span>
                                        <div class="col-4">
                                            <b><label>Provinsi</label></b><br>
                                            <span><?php echo e($alamat->provinsi); ?></span>
                                        </div>
                                        <div class="col-4">
                                            <b><label>Kabupaten</label></b><br>
                                            <span><?php echo e($alamat->kabupaten); ?></span>
                                        </div>
                                        <div class="col-4">
                                            <b><label>Alamat</label></b><br>
                                            <span><?php echo e($alamat->detail_alamat); ?></span>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="alamat-pengirim">
                                    <div class="row" style="padding: 10px;">
                                        <span style="font-size: 14px;font-weight:500;margin-bottom:10px;">Pilih Pengiriman</span>
                                        <div class="col-12 col-md-6">
                                            <div class="mt-2 form-group">
                                                <label>Kurir</label>
                                                <select name="kurir" id="kurir" onchange="cek_ongkir(this)"
                                                    class="form-control">
                                                    <option value="">Pilih Kurir</option>
                                                    <option value="jne">JNE</option>
                                                    <option value="pos">POS</option>
                                                    <option value="tiki">TIKI</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-12 col-md-6">
                                            <div class="mt-2 form-group">
                                                <label>Jenis Pengiriman</label>
                                                <select name="jenis_pengiriman" id="jenis_pengiriman"
                                                    onchange="jenis_pengiriman(this)" class="form-control">
                                                    <option value="">Pilih Jenis Pengiriman</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="estimasi-hari">
                                        <b><label>Estimasi Hari</label></b><br>
                                        <span id="estimasi">Pilih Pengiriman Dahulu</span>
                                    </div>
                                </div>
                        <div class="mt-3 card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-12">
                                        <span style="font-size: 24px;font-weight:500;">Pembayaran</span>
                                        <table style="width:90%;margin:auto;margin-top:30px;">
                                            <thead>
                                                <tr>
                                                    <th style="width:40%;">Item</th>
                                                    <th style="width:20%;">Jumlah</th>
                                                    <th style="width:40%;text-align:right;">Harga</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php $harga_total = 0; ?>
                                                <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <?php $harga_item = @$dt->jumlah * @$dt->harga; ?>
                                                    <tr style="border-bottom:0.3px solid black;">
                                                        <td><?php echo e(@$dt->nama_produk); ?></td>
                                                        <td><?php echo e(@$dt->jumlah); ?></td>
                                                        <td style="text-align:right;">Rp. <?php echo e(rupiah(@$harga_item)); ?></td>
                                                    </tr>
                                                    <?php $harga_total += $harga_item; ?>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </tbody>
                                        </table>
                                        <table style="width:90%;margin:auto;margin-top:30px;display:none;" id="dt_ongkir">
                                            <thead>
                                                <tr>
                                                    <th style="vertical-align:top:40%;">Kurir</th>
                                                    <th style="vertical-align:top:20%;">Jenis Pengiriman</th>
                                                    <th style="vertical-align:top:40%;text-align:right;">Harga</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr  style="border-bottom:0.3px solid black;" id="ongkir">
                                                </tr>
                                            </tbody>
                                        </table>
                                        <table style="width:90%;margin:auto;margin-top:30px;">
                                            <tr>
                                                <td colspan="2"><b>Total</b></td>
                                                <td style="text-align: right" id="total">Rp.
                                                    <?php echo e(rupiah(@$harga_total)); ?></td>
                                            </tr>
                                        </table>
                                    </div>
                                    <input type="hidden" id="harga_item" value="<?php echo e(@$harga_total); ?>">
                                    <input type="hidden" id="harga_total">
                                    <input type="hidden" id="id_keranjang" value="<?php echo e(@$id_keranjang); ?>">
                                    <input type="hidden" id="harga_ongkir" value="">
                                    <div class="p-3 col-12" style="margin-top:30px;">
                                        <button class="btn-cstm-blue" onclick="simpan()"
                                            style="width:100%;">Buat Pembayaran</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        let harga_total = '<?php echo e($harga_total); ?>';
        let total_item = '<?php echo e(count($data)); ?>';
        let alamat_tujuan = '<?php echo e($alamat->city_id); ?>';
        $(".nav-pills .nav-item .nav-link").click(function() {
            id = $(this).data('id');
            $("#myTabContent .tab-pane").removeClass('show active')
            $(`#myTabContent #tab${id}`).addClass('show active');
        })
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('front.layout.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\application\basecampidn\resources\views/front/checkout.blade.php ENDPATH**/ ?>