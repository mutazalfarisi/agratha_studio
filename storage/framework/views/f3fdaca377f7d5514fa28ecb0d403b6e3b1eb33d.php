

<?php $__env->startSection('content'); ?>

    <div class="content">
        <div class="container__keranjang">
            <div class="bg-white tab-nav keranjang__navbar">
                <ul class="pt-3 nav nav-pills nav-justified">
                    <li class="nav-item m-3 <?php echo e(@$li_active == 'keranjang' ? 'active_' : ''); ?> d-flex align-items-center" style="cursor:pointer;">
                        <a class="nav-link" data-id="1" id="tab1-tab">Keranjang</a>
                    </li>
                    <li class="m-3 nav-item d-flex align-items-center" style="cursor:pointer;">
                        <a class="nav-link" data-id="2" id="tab2-tab">Sedang Proses</a>
                    </li>
                    <li class="m-3 nav-item d-flex align-items-center" style="cursor:pointer;">
                        <a class="nav-link" data-id="3" id="tab3-tab">Riwayat Pemesanan</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="tab-content" id="myTabContent" style="margin-top:90px; ">
            <div class="p-4 tab-pane fade show active" id="tab1" role="tabpanel" aria-labelledby="tab1-tab">
                <div class="mt-2 shadow-sm card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12 col-md-2">
                                <img src="<?php echo e(asset('images/img1.jpg')); ?>" style="width: 100%;object-fit:cover;">
                            </div>
                            <div class="col-12 col-md-7">
                                <div class="title__produk__cart">
                                    <h5>Kaos Oversized</h5>
                                </div>
                                <div class="keterangan__produk">
                                    <p>
                                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Vero, totam. Maxime ad
                                        accusantium consequatur a velit repellendus nihil deserunt, rem numquam harum
                                        facilis ipsum perspiciatis autem blanditiis tempore sequi dicta voluptatem aliquid
                                        veniam vitae minima temporibus reiciendis consectetur incidunt! Explicabo molestias
                                        praesentium corporis quibusdam deserunt fuga sint doloremque adipisci voluptatem?
                                    </p>
                                </div>
                            </div>
                            <div class="col-12 col-md-3 d-flex justify-content-center align-items-center">
                                <div class="button__checkout">
                                    <button class="btn w-75 btn-sm btn-success">CheckOut</button>
                                    <button class="mt-2 btn w-75 btn-sm btn-danger">Batal</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mt-2 shadow-sm card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12 col-md-2">
                                <img src="<?php echo e(asset('images/img1.jpg')); ?>" style="width: 100%;object-fit:cover;">
                            </div>
                            <div class="col-12 col-md-7">
                                <div class="title__produk__cart">
                                    <h5>Kaos Oversized</h5>
                                </div>
                                <div class="keterangan__produk">
                                    <p>
                                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Vero, totam. Maxime ad
                                        accusantium consequatur a velit repellendus nihil deserunt, rem numquam harum
                                        facilis ipsum perspiciatis autem blanditiis tempore sequi dicta voluptatem aliquid
                                        veniam vitae minima temporibus reiciendis consectetur incidunt! Explicabo molestias
                                        praesentium corporis quibusdam deserunt fuga sint doloremque adipisci voluptatem?
                                    </p>
                                </div>
                            </div>
                            <div class="col-12 col-md-3 d-flex justify-content-center align-items-center">
                                <div class="button__checkout">
                                    <button class="btn w-75 btn-sm btn-success">CheckOut</button>
                                    <button class="mt-2 btn w-75 btn-sm btn-danger">Batal</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mt-2 shadow-sm card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12 col-md-2">
                                <img src="<?php echo e(asset('images/img1.jpg')); ?>" style="width: 100%;object-fit:cover;">
                            </div>
                            <div class="col-12 col-md-7">
                                <div class="title__produk__cart">
                                    <h5>Kaos Oversized</h5>
                                </div>
                                <div class="keterangan__produk">
                                    <p>
                                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Vero, totam. Maxime ad
                                        accusantium consequatur a velit repellendus nihil deserunt, rem numquam harum
                                        facilis ipsum perspiciatis autem blanditiis tempore sequi dicta voluptatem aliquid
                                        veniam vitae minima temporibus reiciendis consectetur incidunt! Explicabo molestias
                                        praesentium corporis quibusdam deserunt fuga sint doloremque adipisci voluptatem?
                                    </p>
                                </div>
                            </div>
                            <div class="col-12 col-md-3 d-flex justify-content-center align-items-center">
                                <div class="button__checkout">
                                    <button class="btn w-75 btn-sm btn-success">CheckOut</button>
                                    <button class="mt-2 btn w-75 btn-sm btn-danger">Batal</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mt-2 shadow-sm card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12 col-md-2">
                                <img src="<?php echo e(asset('images/img1.jpg')); ?>" style="width: 100%;object-fit:cover;">
                            </div>
                            <div class="col-12 col-md-7">
                                <div class="title__produk__cart">
                                    <h5>Kaos Oversized</h5>
                                </div>
                                <div class="keterangan__produk">
                                    <p>
                                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Vero, totam. Maxime ad
                                        accusantium consequatur a velit repellendus nihil deserunt, rem numquam harum
                                        facilis ipsum perspiciatis autem blanditiis tempore sequi dicta voluptatem aliquid
                                        veniam vitae minima temporibus reiciendis consectetur incidunt! Explicabo molestias
                                        praesentium corporis quibusdam deserunt fuga sint doloremque adipisci voluptatem?
                                    </p>
                                </div>
                            </div>
                            <div class="col-12 col-md-3 d-flex justify-content-center align-items-center">
                                <div class="button__checkout">
                                    <button class="btn w-75 btn-sm btn-success">CheckOut</button>
                                    <button class="mt-2 btn w-75 btn-sm btn-danger">Batal</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mt-2 shadow-sm card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12 col-md-2">
                                <img src="<?php echo e(asset('images/img1.jpg')); ?>" style="width: 100%;object-fit:cover;">
                            </div>
                            <div class="col-12 col-md-7">
                                <div class="title__produk__cart">
                                    <h5>Kaos Oversized</h5>
                                </div>
                                <div class="keterangan__produk">
                                    <p>
                                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Vero, totam. Maxime ad
                                        accusantium consequatur a velit repellendus nihil deserunt, rem numquam harum
                                        facilis ipsum perspiciatis autem blanditiis tempore sequi dicta voluptatem aliquid
                                        veniam vitae minima temporibus reiciendis consectetur incidunt! Explicabo molestias
                                        praesentium corporis quibusdam deserunt fuga sint doloremque adipisci voluptatem?
                                    </p>
                                </div>
                            </div>
                            <div class="col-12 col-md-3 d-flex justify-content-center align-items-center">
                                <div class="button__checkout">
                                    <button class="btn w-75 btn-sm btn-success">CheckOut</button>
                                    <button class="mt-2 btn w-75 btn-sm btn-danger">Batal</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="p-5 pagination d-flex justify-content-center">
                    <a href=""><div class="rounded page_number">
                        <i class='bx bxs-chevron-left'></i>
                    </div></a>
                    <a href=""><div class="rounded page_number active_num">
                        1
                    </div></a>
                    <a href=""><div class="rounded page_number">
                        2
                    </div></a>
                    <a href=""><div class="rounded page_number">
                        ...
                    </div></a>
                    <a href=""><div class="rounded page_number">
                        5
                    </div></a>
                    <a href=""><div class="rounded page_number">
                        <i class='bx bxs-chevron-right'></i>
                    </div></a>
                </div>
            </div>
            <div class="p-4 tab-pane fade" id="tab2" role="tabpanel" aria-labelledby="tab2-tab">
                <div class="mt-2 shadow-sm card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-2">
                                <img src="<?php echo e(asset('images/img1.jpg')); ?>" style="width: 100%;object-fit:cover;">
                            </div>
                            <div class="col-7">
                                <div class="title__produk__cart">
                                    <h5>Kaos Oversized</h5>
                                </div>
                                <div class="keterangan__produk">
                                    <p>
                                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Vero, totam. Maxime ad
                                        accusantium consequatur a velit repellendus nihil deserunt, rem numquam harum
                                        facilis ipsum perspiciatis autem blanditiis tempore sequi dicta voluptatem aliquid
                                        veniam vitae minima temporibus reiciendis consectetur incidunt! Explicabo molestias
                                        praesentium corporis quibusdam deserunt fuga sint doloremque adipisci voluptatem?
                                    </p>
                                </div>
                            </div>
                            <div class="col-3 d-flex justify-content-center align-items-center">
                                <div class="button__checkout">
                                    <button class="btn w-75 btn-sm btn-success">CheckOut</button>
                                    <button class="mt-2 btn w-75 btn-sm btn-danger">Batal</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="p-5 pagination d-flex justify-content-center">
                </div>
            </div>
            <div class="p-4 tab-pane fade" id="tab3" role="tabpanel" aria-labelledby="tab3-tab">
                <div class="p-5 pagination d-flex justify-content-center" style="min-height: 70vh;">
                    <h3 class="text-center" style="opacity:80%;">Anda Belum Mempunyai Riwayat Pemesanan</h3>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(".nav-pills .nav-item .nav-link").click(function() {
            id = $(this).data('id');
            $("#myTabContent .tab-pane").removeClass('show active')
            $(`#myTabContent #tab${id}`).addClass('show active');
        })
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('front.layout.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\application\marketplace\resources\views/front/keranjang.blade.php ENDPATH**/ ?>