
<!DOCTYPE html>
<html lang="en">
<head>
	<title><?php echo e(@$title ?? 'Agratha Studio.'); ?></title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo e(asset('assets_login')); ?>/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo e(asset('assets_login')); ?>/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo e(asset('assets_login')); ?>/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo e(asset('assets_login')); ?>/vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="<?php echo e(asset('assets_login')); ?>/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo e(asset('assets_login')); ?>/vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo e(asset('assets_login')); ?>/vendor/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="<?php echo e(asset('assets_login')); ?>/vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo e(asset('assets_login')); ?>/css/util.css">
	<link rel="stylesheet" type="text/css" href="<?php echo e(asset('assets_login')); ?>/css/main.css">
<!--===============================================================================================-->
</head>
<body style="background-color: #666666;">
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<form action="<?php echo e(route('register.store')); ?>" method="post" class="login100-form validate-form">
				<?php echo csrf_field(); ?>	
					<span class="login100-form-title p-b-43">
						Registrasi to continue
					</span>
					
					<?php if(session('success')): ?>
						<div class="m-3 alert alert-success">
							<span style="font-size: 12px;"><?php echo e(session('success')); ?></span>
						</div>
					<?php endif; ?>

					<?php if(session('error')): ?>
						<div class="m-3 alert alert-danger">
							<span style="font-size: 12px;"><?php echo e(session('error')); ?></span>
						</div>
					<?php endif; ?>
					<?php if($errors->any()): ?>
						<div class="m-3 alert alert-danger">
							<ul>
								<?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
									<li style="font-size: 12px;"><?php echo e($error); ?></li>
								<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
							</ul>
						</div>
					<?php endif; ?>
				
					<div class="wrap-input100 validate-input">
						<input class="input100" type="text" name="nama" value="<?php echo e(old('nama')); ?>">
						<span class="focus-input100"></span>
						<span class="label-input100">Nama</span>
					</div>

					<div class="wrap-input100 validate-input">
						<input class="input100" type="text" name="username" value="<?php echo e(old('username')); ?>">
						<span class="focus-input100"></span>
						<span class="label-input100">Username</span>
					</div>
                    
					<div class="wrap-input100 validate-input">
						<input class="input100" type="text" name="no_hp" value="<?php echo e(old('no_hp')); ?>">
						<span class="focus-input100"></span>
						<span class="label-input100">Nomor HP</span>
					</div>

					<div class="wrap-input100 validate-input">
						<input class="input100" type="text" name="email" value="<?php echo e(old('email')); ?>">
						<span class="focus-input100"></span>
						<span class="label-input100">Email</span>
					</div>
                    
					<div class="wrap-input100 validate-input">
						<input class="input100" type="password" name="password" value="<?php echo e(old('password')); ?>">
						<span class="focus-input100"></span>
						<span class="label-input100">Password</span>
					</div>
                    
					<div class="wrap-input100 validate-input">
						<input class="input100" type="password" name="confirm_password" value="<?php echo e(old('confirm_password')); ?>">
						<span class="focus-input100"></span>
						<span class="label-input100">Konfirmasi Password</span>
					</div>
					
					<div class="container-login100-form-btn">
						<button type="submit" class="login100-form-btn">
							Daftar
						</button>
					</div>
                    <div class="text-center" style="margin-top: 30px;font-size:14px;">
                        <div>Sudah Punya Akun?</div>
                        <div><a href="<?php echo e(url('login')); ?>" style="text-decoration: none;">Masuk Sekarang</a>
                        </div>
                    </div>
				</form>

				<div class="login100-more" style="background-image: url('<?php echo e(asset('assets_login')); ?>/images/login.jpg');">
				</div>
			</div>
		</div>
	</div>
	
<!--===============================================================================================-->
	<script src="<?php echo e(asset('assets_login')); ?>/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo e(asset('assets_login')); ?>/vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo e(asset('assets_login')); ?>/vendor/bootstrap/js/popper.js"></script>
	<script src="<?php echo e(asset('assets_login')); ?>/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo e(asset('assets_login')); ?>/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo e(asset('assets_login')); ?>/vendor/daterangepicker/moment.min.js"></script>
	<script src="<?php echo e(asset('assets_login')); ?>/vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo e(asset('assets_login')); ?>/vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo e(asset('assets_login')); ?>/js/main.js"></script>

</body>
</html><?php /**PATH D:\application\agratha\resources\views/register/index.blade.php ENDPATH**/ ?>