

<?php $__env->startSection('content'); ?>
    <!-- Begin Page Content -->
    <div class="container-fluid">
        <!-- Page Heading -->
        <div class="mb-4 d-sm-flex align-items-center justify-content-between">
            <h1 class="mb-0 text-gray-800 h3"><?php echo e(@$page_title); ?></h1>
            <a href="<?php echo e(url('/kelola/kaos')); ?>" class="shadow-sm d-none d-sm-inline-block btn btn-sm btn-danger">
                <i class="text-white fas fa-arrow-left fa-sm fa-fw"></i> Kembali</a>
        </div>
        <!-- Content Row -->
        <div class="row">
            <div class="col-12">
                <div class="row">
                    <div class="card" style="width: 100%;">
                        <div class="card-body">
                            <form action="<?php echo e(url('/simpan/produk')); ?>" enctype="multipart/form-data" method="post">
                            <?php echo csrf_field(); ?>
                            <div class="row">
                                <div class="form-group">
                                    <label>Nama Produk</label>
                                    <input type="text" name="produk" >
                                </div>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End of Main Content -->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.template.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\application\basecampidn\resources\views/admin/kelola/tambah_kaos.blade.php ENDPATH**/ ?>