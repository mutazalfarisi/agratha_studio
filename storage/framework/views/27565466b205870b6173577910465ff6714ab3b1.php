

<?php $__env->startSection('content'); ?>
    
    <div class="content">
        <div class="container__keranjang">
            <div class="bg-white tab-nav keranjang__navbar">
                <ul class="pt-3 nav nav-pills nav-justified">
                    <li class="nav-item m-3 <?php echo e(@$li_active == 'keranjang' ? 'active_' : ''); ?> d-flex align-items-center"
                        style="cursor:pointer;">
                        <a class="nav-link" data-id="1" id="tab1-tab">Keranjang</a>
                    </li>
                    <li class="m-3 nav-item d-flex align-items-center" style="cursor:pointer;">
                        <a class="nav-link" data-id="2" id="tab2-tab">Belum Pembayaran</a>
                    </li>
                    <li class="m-3 nav-item d-flex align-items-center" style="cursor:pointer;">
                        <a class="nav-link" data-id="3" id="tab3-tab">Produk Expired</a>
                    </li>
                    <li class="m-3 nav-item d-flex align-items-center" style="cursor:pointer;">
                        <a class="nav-link" data-id="4" id="tab4-tab">Sedang Proses</a>
                    </li>
                    <li class="m-3 nav-item d-flex align-items-center" style="cursor:pointer;">
                        <a class="nav-link" data-id="5" id="tab5-tab">Riwayat Pemesanan</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="p-4 row">
            <div class="col-8">
                <?php if($errors->any()): ?>
                    <div class="alert alert-danger">
                        <ul>
                            <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <li><?php echo e($error); ?></li>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </ul>
                    </div>
                <?php endif; ?>
                <?php if(session('success')): ?>
                    <div class="alert alert-success">
                        <?php echo e(session('success')); ?>

                    </div>
                <?php endif; ?>

                <?php if(session('error')): ?>
                    <div class="alert alert-danger">
                        <?php echo e(session('error')); ?>

                    </div>
                <?php endif; ?>
            </div>
        </div>

        <div class="tab-content" id="myTabContent">
            <div class="p-4 tab-pane fade show active" id="tab1" role="tabpanel" aria-labelledby="tab1-tab"
                id="keranjang">
                <?php if(count(@$dt_keranjang) > 0): ?>
                    <?php $__currentLoopData = @$dt_keranjang; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="mt-2 shadow-sm card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-12 col-md-2">
                                        <img src="<?php echo e(asset('storage') . '/' . @$item->foto); ?>"
                                            style="width: 100%;object-fit:cover;">
                                    </div>
                                    <div class="col-12 col-md-7">
                                        <div class="title__produk__cart">
                                            <h3><?php echo e(@$item->nama_produk); ?></h3>
                                        </div>
                                        <div class="keterangan__produk">
                                            <h5>Rp. <?php echo e(@$item->harga); ?>,00</h5>
                                        </div>
                                        <div class="keterangan__produk">
                                            <h5><?php echo e(@$item->jumlah); ?></h5>
                                        </div>
                                        <div class="keterangan__produk">
                                            <p>
                                                <?php echo e(@$item->deskripsi); ?>

                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-3 d-flex justify-content-center align-items-center">
                                        <div class="button__checkout">
                                            <button class="btn w-75 btn-cstm-green" data-id_cart="<?php echo e(@$item->id); ?>"
                                                onclick="checkout(this)">CheckOut</button>
                                            <button class="mt-2 btn w-75 btn-cstm-red" data-id_cart="<?php echo e(@$item->id); ?>"
                                                onclick="hapus(this)">Batal</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php else: ?>
                    <div class="p-5 pagination d-flex justify-content-center" style="min-height: 70vh;">
                        <h3 class="text-center" style="opacity:80%;">Keranjang Anda Kosong.</h3>
                    </div>
                <?php endif; ?>
            </div>
            <div class="p-4 tab-pane fade" id="tab2" role="tabpanel" aria-labelledby="tab2-tab">
                <?php if(count(@$dt_belum_bayar) > 0): ?>
                    <?php $__currentLoopData = @$dt_belum_bayar; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="mt-2 shadow-sm card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-12 col-md-2">
                                        <img src="<?php echo e(asset('storage') . '/' . @$item->foto); ?>"
                                            style="width: 100%;object-fit:cover;">
                                    </div>
                                    <div class="col-12 col-md-7">
                                        <div class="title__produk__cart">
                                            <h3><?php echo e(@$item->nama_produk); ?></h3>
                                        </div>
                                        <div class="keterangan__produk">
                                            <h5>Rp. <?php echo e(@$item->harga); ?>,00</h5>
                                        </div>
                                        <div class="keterangan__produk">
                                            <h5><?php echo e(@$item->jumlah); ?></h5>
                                        </div>
                                        <div class="keterangan__produk">
                                            <p>
                                                <?php echo e(@$item->deskripsi); ?>

                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-3 d-flex justify-content-center align-items-center">
                                        <div class="button__checkout">
                                            <?php if(@$item->status_pembayaran == '0'): ?>
                                                <p class="badge bg-danger" style="margin-bottom:5px;">Belum Upload Bukti</p>
                                            <?php else: ?>
                                                <p class="badge bg-primary" style="margin-bottom:5px;">Menunggu Persetujuan
                                                </p>
                                                
                                            <?php endif; ?>
                                            <button class="mt-2 btn w-75 btn-cstm-blue"
                                                data-id_transaksi="<?php echo e(@$item->id); ?>" data-link="<?php echo e(asset('storage') . '/' . @$item->bukti_transfer); ?>" data-bukti="<?php echo e(@$item->bukti_transfer); ?>" onclick="uploadPay(this)">Upload
                                                Bukti Pembayaran</button>
                                            <div class="mt-2 tgl-exp">
                                                <p class="badge bg-info" style="margin-bottom:5px;">Terakhir Pembayaran
                                                    :<br> <?php echo e(tanggal_indonesia(@$item->tgl_exp)); ?></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <div class="modal fade" id="uploadPay" tabindex="-1" aria-labelledby="uploadPayLabel"
                            aria-hidden="true">
                            <div class="modal-dialog">
                                <form action="<?php echo e(url('upload_bukti')); ?>" method="post"
                                    enctype="multipart/form-data">
                                    <?php echo csrf_field(); ?>
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="uploadPayLabel">Upload Bukti
                                                Pembayaran
                                            </h5>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body">
                                            <div id="bukti" style="display: none;">
                                                <a href="" target="_blank"
                                                    class="btn btn-warning btn-sm" id="lihat_bukti">Lihat
                                                    Bukti</a>
                                            </div>
                                            <div>
                                                <label>Upload File</label>
                                                <input type="file" name="upload_bukti"
                                                    class="form-control" id="upload_bukti" value=""
                                                    accept="image/*" capture>
                                                <input type="hidden" name="id_transaksi" id="id_transaksi">
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary"
                                                data-bs-dismiss="modal">Tutup</button>
                                            <button type="submit" class="btn btn-primary">Simpan</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                <?php else: ?>
                    <div class="p-5 pagination d-flex justify-content-center" style="min-height: 70vh;">
                        <h3 class="text-center" style="opacity:80%;">Anda Belum Mempunyai Transaksi.</h3>
                    </div>
                <?php endif; ?>
            </div>
            <div class="p-4 tab-pane fade" id="tab3" role="tabpanel" aria-labelledby="tab3-tab">
                <?php if(count(@$dt_exp) > 0): ?>
                    <?php $__currentLoopData = @$dt_exp; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="mt-2 shadow-sm card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-12 col-md-2">
                                        <img src="<?php echo e(asset('storage') . '/' . @$item->foto); ?>"
                                            style="width: 100%;object-fit:cover;">
                                    </div>
                                    <div class="col-12 col-md-7">
                                        <div class="title__produk__cart">
                                            <h3><?php echo e(@$item->nama_produk); ?></h3>
                                        </div>
                                        <div class="keterangan__produk">
                                            <h5>Rp. <?php echo e(@$item->harga); ?>,00</h5>
                                        </div>
                                        <div class="keterangan__produk">
                                            <h5><?php echo e(@$item->jumlah); ?></h5>
                                        </div>
                                        <div class="keterangan__produk">
                                            <p>
                                                <?php echo e(@$item->deskripsi); ?>

                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-3 d-flex justify-content-center align-items-center">
                                        <div class="button__checkout">
                                            <div class="mt-2 tgl-exp">
                                                <p class="badge bg-info" style="margin-bottom:5px;">Terakhir Pembayaran
                                                    :<br> <?php echo e(tanggal_indonesia(@$item->tgl_exp)); ?></p>
                                            </div>
                                            <button class="btn w-75 btn-cstm-green" data-id_cart="<?php echo e(@$item->id); ?>"
                                                onclick="checkout(this)">CheckOut Ulang</button>
                                            <button class="mt-2 btn w-75 btn-cstm-red" data-id_cart="<?php echo e(@$item->id); ?>"
                                                onclick="hapus(this)">Hapus</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php else: ?>
                    <div class="p-5 pagination d-flex justify-content-center" style="min-height: 70vh;">
                        <h3 class="text-center" style="opacity:80%;">Anda Belum Mempunyai Produk Expired.</h3>
                    </div>
                <?php endif; ?>
            </div>
            <div class="p-4 tab-pane fade" id="tab4" role="tabpanel" aria-labelledby="tab4-tab">
                <?php if(count(@$dt_proses) > 0): ?>
                    <?php $__currentLoopData = @$dt_proses; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="mt-2 shadow-sm card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-12 col-md-2">
                                        <img src="<?php echo e(asset('storage') . '/' . @$item->foto); ?>"
                                            style="width: 100%;object-fit:cover;">
                                    </div>
                                    <div class="col-12 col-md-7">
                                        <div class="title__produk__cart">
                                            <h3><?php echo e(@$item->nama_produk); ?></h3>
                                        </div>
                                        <div class="keterangan__produk">
                                            <h5>Rp. <?php echo e(@$item->harga); ?>,00</h5>
                                        </div>
                                        <div class="keterangan__produk">
                                            <h5><?php echo e(@$item->jumlah); ?></h5>
                                        </div>
                                        <div class="keterangan__produk">
                                            <p>
                                                <?php echo e(@$item->deskripsi); ?>

                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-3 d-flex justify-content-center align-items-center">
                                        <div class="button__checkout">
                                            <a href="<?php echo e(url('/admin/cetak/transaksi') . '/' . $item->id); ?>" target="_blank" class="text-white shadow-sm d-none d-sm-inline-block btn btn-sm btn-warning"><i
                                                class="text-white fas fa-print fa-sm fa-fw"></i> Cetak Invoice</a><br>
                                            <?php if(@$item->status_barang == '0'): ?>
                                                <p class="badge bg-danger" style="margin-bottom:5px;">Menunggu Persetujuan
                                                </p>
                                            <?php elseif(@$item->status_barang == '1'): ?>
                                                <p class="badge bg-warning" style="margin-bottom:5px;">Sedang Dikemas
                                                </p>
                                            <?php elseif(@$item->status_barang == '2'): ?>
                                            <div>
                                                <p class="badge bg-primary" style="margin-bottom:5px;">Proses Pengiriman
                                                </p>
                                            </div>
                                            <div>
                                                <button onclick="terima(<?php echo e(@$item->id); ?>)" class="btn btn-sm btn-secondary">Pesanan diterima</button>
                                            </div>
                                            <?php endif; ?>
                                                <input type="hidden" id="link_bukti"
                                                    value="<?php echo e(asset('storage') . '/' . @$item->bukti_transfer); ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php else: ?>
                    <div class="p-5 pagination d-flex justify-content-center" style="min-height: 70vh;">
                        <h3 class="text-center" style="opacity:80%;">Belum ada Transaksi.</h3>
                    </div>
                <?php endif; ?>
            </div>
            <div class="p-4 tab-pane fade" id="tab5" role="tabpanel" aria-labelledby="tab5-tab">
                <?php if(count(@$dt_riwayat) > 0): ?>
                    <?php $__currentLoopData = @$dt_riwayat; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="mt-2 shadow-sm card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-12 col-md-2">
                                        <img src="<?php echo e(asset('storage') . '/' . @$item->foto); ?>"
                                            style="width: 100%;object-fit:cover;">
                                    </div>
                                    <div class="col-12 col-md-7">
                                        <div class="title__produk__cart">
                                            <h3><?php echo e(@$item->nama_produk); ?></h3>
                                        </div>
                                        <div class="keterangan__produk">
                                            <h5>Rp. <?php echo e(@$item->harga); ?>,00</h5>
                                        </div>
                                        <div class="keterangan__produk">
                                            <h5><?php echo e(@$item->jumlah); ?></h5>
                                        </div>
                                        <div class="keterangan__produk">
                                            <p>
                                                <?php echo e(@$item->deskripsi); ?>

                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-3 d-flex justify-content-center align-items-center">
                                        <div class="button__checkout">
                                            <p class="badge bg-success" style="margin-bottom:5px;">Pesanan Diterima pada Tanggal <br> <?php echo e(tanggal_indonesia(@$item->tgl_terima)); ?>

                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php else: ?>
                    <div class="p-5 pagination d-flex justify-content-center" style="min-height: 70vh;">
                        <h3 class="text-center" style="opacity:80%;">Anda Belum Mempunyai Riwayat Pemesanan.</h3>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>

    <script>
        $(".nav-pills .nav-item .nav-link").click(function() {
            id = $(this).data('id');
            $("#myTabContent .tab-pane").removeClass('show active')
            $(`#myTabContent #tab${id}`).addClass('show active');
        })
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('front.layout.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\application\basecampidn\resources\views/front/keranjang.blade.php ENDPATH**/ ?>