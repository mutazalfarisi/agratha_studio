

<?php $__env->startSection('content'); ?>
    <!-- Begin Page Content -->
    
    <!-- Content Row -->
    <div class="row">
        <?php if(session('success')): ?>
            <div class="alert alert-primary">
                <?php echo e(session('success')); ?>

            </div>
        <?php endif; ?>

        <?php if(session('error')): ?>
            <div class="alert alert-danger">
                <?php echo e(session('error')); ?>

            </div>
        <?php endif; ?>

        <?php if($errors->any()): ?>
            <div class="alert alert-danger">
                <ul>
                    <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <li><?php echo e($error); ?></li>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </ul>
            </div>
        <?php endif; ?>
        <div class="mb-4 col-lg-12 order-0">
            <div class="card">
                <div class="card-body">
                    <div class="p-3 pt-0 mb-3 justify-content-end d-flex">
                        <a href="<?php echo e(route('tambah_paket')); ?>"
                            class="shadow-sm d-none d-sm-inline-block btn btn-sm btn-primary">
                            Tambah Paket
                        </a>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-fixed table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama</th>
                                    <th>Deskripsi</th>
                                    <th>Foto</th>
                                    <th>Harga</th>
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <script>
        var dataPaket = "<?php echo e(route('dataPaket')); ?>"
        var HapusPaket = "<?php echo e(route('HapusPaket')); ?>"
        var token = '<?php echo e(csrf_token()); ?>';
    </script>
    <script src="<?php echo e(asset('admin/paket/index.js')); ?>"></script>


    <!-- End of Main Content -->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.template.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\application\agratha\resources\views/admin/paket/index.blade.php ENDPATH**/ ?>