

<?php $__env->startSection('content'); ?>
    <!-- Begin Page Content -->
    <div class="container-fluid">
        <!-- Page Heading -->
        <div class="mb-4 d-sm-flex align-items-center justify-content-between">
            <h1 class="mb-0 text-gray-800 h3"><?php echo e(@$page_title); ?></h1>
            
        </div>
        

        <!-- Content Row -->
        <div class="row">
            <div class="col-12">
                <div class="row">
                    <div class="card" style="width: 100%;">
                        <div class="card-body">
                            <?php if(session('success')): ?>
                                <div class="alert alert-success">
                                    <?php echo e(session('success')); ?>

                                </div>
                            <?php endif; ?>

                            <?php if(session('error')): ?>
                                <div class="alert alert-danger">
                                    <?php echo e(session('error')); ?>

                                </div>
                            <?php endif; ?>

                            <?php if($errors->any()): ?>
                                <div class="alert alert-danger">
                                    <ul>
                                        <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <li><?php echo e($error); ?></li>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </ul>
                                </div>
                            <?php endif; ?>

                            <div class="table-responsive">
                                <table class="table table-fixed table-bordered" id="dataTable" width="100%"
                                    cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Username</th>
                                            <th>Nama</th>
                                            <th>Alamat</th>
                                            <th class="text-center">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                            $no = 1;
                                        ?>
                                        <?php $__currentLoopData = $dt_user; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr>
                                                <td style="width:5%!important;"><?php echo e($no++); ?></td>
                                                <td style="width:20%!important;"><?php echo e($row->username); ?></td>
                                                <td style="width:20%!important;">
                                                    <?php echo e($row->name); ?><br>
                                                    <b>Email : </b><?php echo e($row->email); ?><br>
                                                    <b>Nomor HP : </b><?php echo e($row->no_hp); ?>

                                                </td>
                                                <td style="width:30%!important;">
                                                    <?php echo e($row->detail_alamat); ?>

                                                </td>
                                                <td class="text-center" style="width:10%!important;">
                                                    <button class="shadow-sm d-none d-sm-inline-block btn btn-sm btn-danger"
                                                        data-id="<?php echo e($row->id); ?>" onclick="hapus(this)"><i
                                                            class="text-white fas fa-trash fa-sm fa-fw"></i></button>
                                                    <a
                                                        href="<?php echo e(url('/edit/produk') . '/' . $row->id); ?>"class="shadow-sm d-none d-sm-inline-block btn btn-sm btn-warning"><i
                                                            class="text-white fas fa-edit fa-sm fa-fw"></i></a>
                                                </td>
                                            </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End of Main Content -->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.template.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\application\basecampidn\resources\views/admin/users/customer.blade.php ENDPATH**/ ?>