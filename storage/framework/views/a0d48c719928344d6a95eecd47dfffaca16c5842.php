

<?php $__env->startSection('content'); ?>
    <!-- Begin Page Content -->
    <div class="container-fluid">
        <!-- Page Heading -->
        <div class="mb-4 d-sm-flex align-items-center justify-content-between">
            <h1 class="mb-0 text-gray-800 h3"><?php echo e(@$page_title); ?></h1>
        </div>
        <!-- Content Row -->
        <div class="row">
            <div class="col-12">
                <div class="row">
                    <div class="card" style="width: 100%;">
                        <div class="card-body">
                            <?php if(session('success')): ?>
                                <div class="alert alert-success">
                                    <?php echo e(session('success')); ?>

                                </div>
                            <?php endif; ?>

                            <?php if(session('error')): ?>
                                <div class="alert alert-danger">
                                    <?php echo e(session('error')); ?>

                                </div>
                            <?php endif; ?>
                            <ul class="mb-3 nav nav-pills" id="pills-tab" role="tablist">
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link active" id="tab-persetujuan" data-toggle="pill"
                                        data-target="#pills-home" type="button" role="tab" aria-controls="pills-home"
                                        aria-selected="true">Persetujuan</button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link" id="tab-proses" data-toggle="pill" data-target="#pills-profile"
                                        type="button" role="tab" aria-controls="pills-profile"
                                        aria-selected="false">Proses</button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link" id="tab-selesai" data-toggle="pill"
                                        data-target="#pills-contact" type="button" role="tab"
                                        aria-controls="pills-contact" aria-selected="false">Selesai</button>
                                </li>
                            </ul>
                            <div class="tab-content" id="pills-tabContent">
                                <div class="tab-pane fade show active" id="pills-home" role="tabpanel"
                                    aria-labelledby="tab-persetujuan">
                                    <div class="table-responsive">
                                        <table class="table table-bordered" id="data-acc" width="100%" cellspacing="0">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Nomor Transaksi</th>
                                                    <th>Nama</th>
                                                    <th>Nomor HP</th>
                                                    <th>Alamat</th>
                                                    <th>Tanggal Transaksi</th>
                                                    <th>Detail Transaksi</th>
                                                    <th>Bukti Pembayaran</th>
                                                    <th>Status</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody style="font-size: 12px;">
                                                <?php
                                                    $no = 1;
                                                ?>
                                                <?php $__currentLoopData = $dt_acc; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <tr>
                                                        <td style="width:5%!important;"><?php echo e($no++); ?></td>
                                                        <td style="width:10%!important;"><?php echo e($row->no_transaksi); ?></td>
                                                        <td style="width:10%!important;"><?php echo e($row->nama); ?></td>
                                                        <td style="width:10%!important;"><?php echo e($row->no_hp); ?></td>
                                                        <td style="width:15%!important;">
                                                            <font style="font-weight: bold">Provinsi : </font>
                                                            <?php echo e($row->provinsi); ?><br>
                                                            <font style="font-weight: bold">Kabupaten : </font>
                                                            <?php echo e($row->kabupaten); ?><br>
                                                            <font style="font-weight: bold">Alamat Lengkap : </font>
                                                            <?php echo e($row->detail_alamat); ?>

                                                        </td>
                                                        <td style="width:10%!important;"><?php echo e(tanggal_indonesia($row->tgl_transaksi)); ?></td>
                                                        <td
                                                            style="width:10%!important;text-align:center;vertical-align:middle;">
                                                            <button class="btn btn-primary btn-sm"
                                                                onclick="detailtransaksi('<?php echo e($row->id); ?>')">Lihat
                                                                Detail</button>
                                                        </td>
                                                        <td
                                                            style="width:10%!important;text-align:center;vertical-align:middle;">
                                                            <a class="btn btn-sm btn-primary"
                                                                href="<?php echo e(asset('storage/' . $row->bukti_transfer)); ?>"
                                                                target="_blank">Lihat Gambar</a>
                                                        </td>
                                                        <td
                                                            style="width:10%!important;text-align:center;vertical-align:middle;">
                                                            <p class="badge badge-danger badge-sm">Menunggu Validasi</p>
                                                        </td>
                                                        <td class="text-center"
                                                            style="width:10%!important;vertical-align:middle;">
                                                            <button
                                                                class="shadow-sm d-none d-sm-inline-block btn btn-sm btn-success"
                                                                data-id="<?php echo e($row->id); ?>" data-tipe="setuju"
                                                                onclick="validasi(this)"><i
                                                                    class="text-white fas fa-check fa-sm fa-fw"></i></button>
                                                            <button onclick="validasi(this)" data-id="<?php echo e($row->id); ?>"
                                                                data-tipe="tolak"
                                                                class="shadow-sm d-none d-sm-inline-block btn btn-sm btn-danger"><i
                                                                    class="text-white fas fa-times fa-sm fa-fw"></i></button>
                                                        </td>
                                                    </tr>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="tab-proses">
                                    <div class="table-responsive">
                                        <table class="table table-bordered" id="data-proses" width="100%" cellspacing="0">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Nomor Transaksi</th>
                                                    <th>Nama</th>
                                                    <th>Nomor HP</th>
                                                    <th>Alamat</th>
                                                    <th>Tanggal Transaksi</th>
                                                    <th>Detail Transaksi</th>
                                                    <th>Status</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody style="font-size: 12px;">
                                                <?php
                                                    $no = 1;
                                                ?>
                                                <?php $__currentLoopData = $dt_dikirim; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <tr>
                                                        <td style="width:5%!important;"><?php echo e($no++); ?></td>
                                                        <td style="width:10%!important;"><?php echo e($row->no_transaksi); ?></td>
                                                        <td style="width:10%!important;"><?php echo e($row->nama); ?></td>
                                                        <td style="width:10%!important;"><?php echo e($row->no_hp); ?></td>
                                                        <td style="width:15%!important;">
                                                            <font style="font-weight: bold">Provinsi : </font>
                                                            <?php echo e($row->provinsi); ?><br>
                                                            <font style="font-weight: bold">Kabupaten : </font>
                                                            <?php echo e($row->kabupaten); ?><br>
                                                            <font style="font-weight: bold">Alamat Lengkap : </font>
                                                            <?php echo e($row->detail_alamat); ?>

                                                        </td>
                                                        <td style="width:10%!important;"><?php echo e(tanggal_indonesia($row->tgl_transaksi)); ?></td>
                                                        <td
                                                            style="width:10%!important;text-align:center;vertical-align:middle;">
                                                            <button class="btn btn-primary btn-sm"
                                                                onclick="detailtransaksi('<?php echo e($row->id); ?>')">Lihat
                                                                Detail</button>
                                                        </td>
                                                        <td style="width:10%!important;text-align:center;vertical-align:middle;">
                                                            <?php if($row->status_barang == '1'): ?>
                                                                <p class="badge badge-primary badge-sm">Sedang diKemas</p>
                                                            <?php elseif($row->status_barang == '2'): ?>
                                                                <p class="badge badge-info badge-sm">Sedang Perjalanan</p>
                                                            <?php endif; ?>
                                                        </td>
                                                        <td style="width:10%!important;text-align:center;vertical-align:middle;">
                                                            <?php if($row->status_barang == '1'): ?>
                                                                <button class="btn btn-info btn-sm" onclick="ganti_status(2,'<?php echo e($row->id); ?>')">Kirim Paket</button>
                                                            <?php elseif($row->status_barang == '2'): ?>
                                                                <button class="btn btn-info btn-sm" onclick="ganti_status(3,'<?php echo e($row->id); ?>')">Selesaikan</button>
                                                            <?php endif; ?>
                                                        </td>
                                                    </tr>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="pills-contact" role="tabpanel"
                                    aria-labelledby="tab-selesai">
                                    <div class="table-responsive">
                                        <table class="table table-bordered" id="data-selesai" width="100%"
                                            cellspacing="0">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Nomor Transaksi</th>
                                                    <th>Nama</th>
                                                    <th>Nomor HP</th>
                                                    <th>Alamat</th>
                                                    <th>Tanggal Transaksi</th>
                                                    <th>Detail Transaksi</th>
                                                    <th>Status</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody style="font-size: 12px;">
                                                <?php
                                                    $no = 1;
                                                ?>
                                                <?php $__currentLoopData = $dt_selesai; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <tr>
                                                    <td style="width:5%!important;"><?php echo e($no++); ?></td>
                                                    <td style="width:10%!important;"><?php echo e($row->no_transaksi); ?></td>
                                                    <td style="width:10%!important;"><?php echo e($row->nama); ?></td>
                                                    <td style="width:10%!important;"><?php echo e($row->no_hp); ?></td>
                                                    <td style="width:15%!important;">
                                                        <font style="font-weight: bold">Provinsi : </font>
                                                        <?php echo e($row->provinsi); ?><br>
                                                        <font style="font-weight: bold">Kabupaten : </font>
                                                        <?php echo e($row->kabupaten); ?><br>
                                                        <font style="font-weight: bold">Alamat Lengkap : </font>
                                                        <?php echo e($row->detail_alamat); ?>

                                                    </td>
                                                    <td style="width:10%!important;"><?php echo e(tanggal_indonesia($row->tgl_transaksi)); ?></td>
                                                    <td
                                                        style="width:10%!important;text-align:center;vertical-align:middle;">
                                                        <button class="btn btn-primary btn-sm"
                                                            onclick="detailtransaksi('<?php echo e($row->id); ?>')">Lihat
                                                            Detail</button>
                                                    </td>
                                                    <td style="width:10%!important;text-align:center;vertical-align:middle;">
                                                        <p class="badge badge-success badge-sm">Selesai</p>
                                                    </td>
                                                    <td style="width:10%!important;text-align:center;vertical-align:middle;">
                                                        <a href="<?php echo e(url('/cetak/transaksi') . '/' . $row->id); ?>" target="_blank" class="shadow-sm d-none d-sm-inline-block btn btn-sm btn-warning"><i
                                                            class="text-white fas fa-print fa-sm fa-fw"></i></a>
                                                    </td>
                                                </tr>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    
    <div class="modal fade" id="modal-transaksi" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Detail Transaksi</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="table-detail-transaksi"></div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Tutup</button>
                </div>
            </div>
        </div>
    </div>

    <script>
        function detailtransaksi(id) {
            $("#modal-transaksi").modal('show');
            $("#table-detail-transaksi").html(
                '<div style="text-align:center;"><img src="<?php echo e(asset('loading.gif')); ?>"></div>');
            $.ajax({
                url: `<?php echo e(route('detail_transaksi')); ?>`,
                type: 'get',
                dataType: 'json',
                data: {
                    id: id
                },
                success: function(res) {
                    $("#table-detail-transaksi").html(res);
                }
            })
        }
    </script>
    <!-- End of Main Content -->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.template.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\application\basecampidn\resources\views/admin/transaksi/tas.blade.php ENDPATH**/ ?>