<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Invoice</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            margin: 20px;
        }

        #invoice {
            width: 100%;
            max-width: 800px;
            margin: 0 auto;
            padding: 20px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
        }

        #invoice header {
            text-align: center;
            margin-bottom: 20px;
        }

        #invoice table {
            width: 100%;
            border-collapse: collapse;
            margin-top: 20px;
        }

        #invoice th,
        #invoice td {
            border: none;
            /* Menghilangkan garis tepi */
            padding: 10px;
            text-align: left;
        }

        #invoice tfoot {
            margin-top: 20px;
            text-align: right;
        }
    </style>
</head>

<body>
    <div id="invoice">
        <header>
            <h2><?php echo e(@$store->nama_toko); ?></h2>
            <p style="font-size:10px;"><?php echo e(ucwords(strtolower($store->alamat_toko))); ?>, <?php echo e($store->kabupaten.' Kecamatan '.$store->kecamatan.' Provinsi ' .$store->provinsi.', '.$store->no_hp); ?></p>
        </header>
        <p>----------------------------------------------------------------------------------------------------------------------------
        </p>
        <table>
            <thead>
                <tr>
                    <th style="border-bottom: 1px solid black">No</th>
                    <th style="border-bottom: 1px solid black">Nama Produk</th>
                    <th style="border-bottom: 1px solid black">Jumlah</th>
                    <th style="border-bottom: 1px solid black">Harga</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $total_harga = 0;
                    $harga_ongkir = 0;
                    $harga_barang = 0;
                ?>
                <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr>
                        <td style="border-bottom: 1px solid black"><?php echo e($loop->iteration); ?></td>
                        <td style="border-bottom: 1px solid black"><?php echo e($item->nama_produk); ?></td>
                        <td style="border-bottom: 1px solid black"><?php echo e($item->jml_barang); ?></td>
                        <td style="border-bottom: 1px solid black">Rp. <?php echo e(rupiah($item->harga)); ?></td>
                    </tr>
                    <?php
                        $harga_barang += $item->harga * $item->jml_barang;
                        $harga_ongkir += $item->harga_ongkir;
                    ?>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <tr>
                    <td colspan="4"></td>
                </tr>
                <tr>
                    <td style="border-bottom: 1px solid black" colspan="3">Harga Barang</td>
                    <td style="border-bottom: 1px solid black">Rp. <?php echo e(rupiah($harga_barang)); ?></td>
                </tr>
                <tr>
                    <td style="border-bottom: 1px solid black" colspan="3">Harga Ongkir</td>
                    <td style="border-bottom: 1px solid black">Rp. <?php echo e(rupiah($harga_ongkir)); ?></td>
                </tr>
            </tbody>
            <tfoot>
                <tr>
                    <td  colspan="3">Total</td>
                    <td >Rp. <?php echo e(rupiah($harga_barang + $harga_ongkir)); ?></td> <!-- Ganti dengan total yang sesuai -->
                </tr>
            </tfoot>
        </table>

        <p>-----------------------------------------------------------------------------------------------------------------------
        </p>
        <footer>
            <p style="text-align: center">Terima kasih atas pembelian Anda di Basecampidn.</p>
        </footer>
    </div>
</body>

</html>
<?php /**PATH D:\application\basecampidn\resources\views/admin/cetak/invoice.blade.php ENDPATH**/ ?>