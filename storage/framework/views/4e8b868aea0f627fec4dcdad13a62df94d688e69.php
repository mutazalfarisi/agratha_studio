

<?php $__env->startSection('content'); ?>
    <!-- Begin Page Content -->
    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between">
            <h1 class="mb-0 text-gray-800 h3"><?php echo e(@$page_title); ?></h1>
        </div>
        <!-- Content Row -->
        <div class="row">
            <?php if(session('success')): ?>
                <div class="alert alert-primary">
                    <?php echo e(session('success')); ?>

                </div>
            <?php endif; ?>

            <?php if(session('error')): ?>
                <div class="alert alert-danger">
                    <?php echo e(session('error')); ?>

                </div>
            <?php endif; ?>
            <?php if($errors->any()): ?>
                <div class="alert alert-danger">
                    <ul>
                        <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <li><?php echo e($error); ?></li>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </ul>
                </div>
            <?php endif; ?>
            <div class="col-12">
                <form action="<?php echo e(url('/admin/simpan_profil')); ?>" method="post">
                    <?php echo csrf_field(); ?>
                    <div class="row">
                        <div class="mt-3 col-12 col-md-6">
                            <label>Username</label>
                            <input type="text" class="form-control" name="username" value="<?php echo e(Auth::user()->username); ?>">
                        </div>
                        <div class="mt-3 col-12 col-md-6">
                            <label>Nama</label>
                            <input type="text" class="form-control" name="nama" value="<?php echo e(Auth::user()->nama); ?>">
                        </div>
                        <div class="mt-3 col-12 col-md-6">
                            <label>Email</label>
                            <input type="text" class="form-control" name="email" value="<?php echo e(Auth::user()->email); ?>">
                        </div>
                        <div class="mt-3 col-12 col-md-6">
                            <label>Nomor HP</label>
                            <input type="text" class="form-control" name="no_hp" value="<?php echo e(Auth::user()->no_hp); ?>">
                        </div>
                        <div class="mt-3 col-4">
                            <label>Provinsi</label>
                            <select name="provinsi" id="provinsi" class="form-control select2">
                                <option value="">-- Pilih Provinsi --</option>
                                <?php $__currentLoopData = $ref_provinsi; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $v): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($v->province_id); ?>"
                                        <?php echo e(@$alamat->province_id == $v->province_id ? 'selected' : ''); ?>>
                                        <?php echo e($v->province_name); ?></option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </select>
                        </div>
                        <div class="mt-3 col-4">
                            <label>Kabupaten</label>
                            <select name="kabupaten" id="kabupaten" class="form-control select2">
                                <option value="">-- Pilih Kabupaten --</option>
                                <?php if(count(@$ref_kab) > 0): ?>
                                    <?php $__currentLoopData = $ref_kab; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $v): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($v->city_id); ?>"
                                            <?php echo e(@$alamat->city_id == $v->city_id ? 'selected' : ''); ?>>
                                            <?php echo e($v->city_name); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endif; ?>
                            </select>
                        </div>
                        <div class="mt-3 col-4">
                            <label>Kecamatan</label>
                            <select name="kecamatan" id="kecamatan" class="form-control select2">
                                <option value="">-- Pilih Kecamatan --</option>
                                <?php if(count(@$ref_kec) > 0): ?>
                                    <?php $__currentLoopData = $ref_kec; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $v): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($v->subdistrict_id); ?>"
                                            <?php echo e(@$alamat->subdistrict_id == $v->subdistrict_id ? 'selected' : ''); ?>>
                                            <?php echo e($v->subdistrict_name); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endif; ?>
                            </select>
                        </div>
                        <div class="mt-3 col-12">
                            <label>Alamat</label>
                            <textarea name="alamat" id="alamat" class="form-control" cols="30" rows="5"><?php echo e(@$alamat->detail_alamat); ?></textarea>
                        </div>
                        <div class="mt-5 col-12">
                            <button type="submit" class="btn btn-sm btn-primary w-100">Simpan</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- End of Main Content -->
    <script>
        $(document).ready(function() {
            $(".select2").select2({
                theme: 'bootstrap-5'
            }) 
            $("#provinsi").change(function() {
                $("#kabupaten").html("<option>Loading</option>")
                $("#kecamatan").html(`<option value="">-- Pilih Kecamatan --</option>`)
                $.ajax({
                    url: '/get_kabupaten',
                    type: 'get',
                    dataType: 'json',
                    data: {
                        province_id: $("#provinsi").val()
                    },
                    success: function(res) {
                        $("#kabupaten").empty();
                        // Tambahkan pilihan kabupaten berdasarkan data yang diterima dari server
                        let kabupaten = '<option value="">-- Pilih Kabupaten --</option>';
                        if (res.length > 0) {
                            $.each(res, function(index, kab) {
                                kabupaten += '<option value="' + kab.city_id + '">' +
                                    kab.city_name + '</option>';
                            });
                            // $("#kabupaten").append('<option value="' + kab.city_id + '">' + kab.city_name + '</option>');
                            $("#kabupaten").append(kabupaten);
                        } else {
                            // Tampilkan pesan jika tidak ada kabupaten yang ditemukan
                            $("#kabupaten").append(
                                '<option value="">Tidak ada kabupaten ditemukan</option>');
                        }
                    }
                })
            })

            $("#kabupaten").change(function() {
                $("#kecamatan").html("<option>Loading</option>")
                $.ajax({
                    url: '/get_kecamatan',
                    type: 'get',
                    dataType: 'json',
                    data: {
                        city_id: $("#kabupaten").val()
                    },
                    success: function(res) {
                        $("#kecamatan").empty();
                        let kecamatan = '<option value="">-- Pilih kecamatan --</option>';
                        if (res.length > 0) {
                            $.each(res, function(index, kec) {
                                kecamatan += '<option value="' + kec.subdistrict_id +
                                    '">' + kec.subdistrict_name + '</option>';
                            });
                            $("#kecamatan").append(kecamatan);
                        } else {
                            $("#kecamatan").append(
                                '<option value="">Tidak ada kecamatan ditemukan</option>');
                        }
                    }
                })
            })
        })
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.template.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\application\agratha\resources\views/admin/profil.blade.php ENDPATH**/ ?>