


<?php $__env->startSection('content'); ?>
    <div class="container-xxl flex-grow-1 container-p-y">
        <div class="row">
            <div class="mb-4 col-lg-12 order-0">
                <div class="card">
                    <div class="d-flex align-items-start row">
                        <div class="col-sm-7">
                            <div class="card-body">
                                <h5 class="card-title text-primary">Selamat Datang <?php echo e(ucwords(Auth::user()->nama)); ?>! 🎉</h5>
                                

                                <a href="<?php echo e(route('profil_admin')); ?>" class="btn btn-sm btn-outline-primary">Profil</a>
                            </div>
                        </div>
                        <div class="text-center col-sm-5 text-sm-left">
                            <div class="px-0 pb-0 card-body px-md-4">
                                <img src="<?php echo e(asset('assets_admin')); ?>/img/illustrations/man-with-laptop-light.png"
                                    height="140" alt="View Badge User"
                                    data-app-dark-img="illustrations/man-with-laptop-dark.png"
                                    data-app-light-img="illustrations/man-with-laptop-light.png" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="order-1 col-lg-12 col-md-4">
                <div class="row">
                    <div class="mb-4 col-lg-3 col-md-12 col-6">
                        <div class="card">
                            <div class="card-body">
                                <div class="card-title d-flex align-items-start justify-content-between">
                                    <div class="flex-shrink-0 avatar">
                                        <img src="<?php echo e(asset('assets_admin')); ?>/img/icons/unicons/wallet-info.png"
                                            alt="Credit Card" class="rounded" />
                                    </div>
                                    <div class="dropdown">
                                        <button class="p-0 btn" type="button" id="cardOpt6" data-bs-toggle="dropdown"
                                            aria-haspopup="true" aria-expanded="false">
                                            <i class="bx bx-dots-vertical-rounded"></i>
                                        </button>
                                        <div class="dropdown-menu dropdown-menu-end" aria-labelledby="cardOpt6">
                                            <a class="dropdown-item" href="<?php echo e(route('paket')); ?>">View More</a>
                                        </div>
                                    </div>
                                </div>
                                <span>Paket</span>
                                <h3 class="mb-1 card-title text-nowrap"><?php echo e($paket); ?></h3>
                            </div>
                        </div>
                    </div>
                    <div class="mb-4 col-lg-3 col-md-12 col-6">
                        <div class="card">
                            <div class="card-body">
                                <div class="card-title d-flex align-items-start justify-content-between">
                                    <div class="flex-shrink-0 avatar">
                                        <img src="<?php echo e(asset('assets_admin')); ?>/img/icons/unicons/cc-primary.png"
                                            alt="Credit Card" class="rounded" />
                                    </div>
                                    <div class="dropdown">
                                        <button class="p-0 btn" type="button" id="cardOpt1" data-bs-toggle="dropdown"
                                            aria-haspopup="true" aria-expanded="false">
                                            <i class="bx bx-dots-vertical-rounded"></i>
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="cardOpt1">
                                            <a class="dropdown-item" href="<?php echo e(route('daftar_customer')); ?>">View More</a>
                                        </div>
                                    </div>
                                </div>
                                <span class="mb-1 fw-semibold d-block">Customer</span>
                                <h3 class="mb-2 card-title"><?php echo e($customer); ?></h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="order-2 mb-4 col-lg-12 order-0">
                <div class="card">
                    <div class="pt-3 pb-3 ps-3">
                        <div class="form-group">
                            <div class="row d-flex align-items-end">
                                <div class="col-4">
                                    <label for="">Data Bulan</label>
                                    <select name="bulan" id="bulan" class="form-control select2">
                                        <option value="">-- Pilih Bulan --</option>
                                    </select>
                                </div>
                                <div class="col-2">
                                    <button class="btn btn-primary" id="terapkan">Terapkan</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="order-3 col-lg-12 col-md-4">
                <div class="row">
                    <div class="mb-4 col-lg-3 col-md-12 col-6">
                        <div class="card">
                            <div class="card-body">
                                <div class="card-title d-flex align-items-start justify-content-between">
                                    <div class="flex-shrink-0 avatar">
                                        <img src="<?php echo e(asset('assets_admin')); ?>/img/icons/unicons/chart-success.png"
                                            alt="chart success" class="rounded" />
                                    </div>
                                    <div class="dropdown">
                                        <button class="p-0 btn" type="button" id="cardOpt3" data-bs-toggle="dropdown"
                                            aria-haspopup="true" aria-expanded="false">
                                            <i class="bx bx-dots-vertical-rounded"></i>
                                        </button>
                                        <div class="dropdown-menu dropdown-menu-end" aria-labelledby="cardOpt3">
                                            <a class="dropdown-item" href="javascript:void(0);">View More</a>
                                        </div>
                                    </div>
                                </div>
                                <span class="mb-1 fw-semibold d-block">Pemesanan</span>
                                <h3 class="mb-2 card-title"><?php echo e($pemesanan); ?></h3>
                            </div>
                        </div>
                    </div>
                    <div class="mb-4 col-lg-3 col-md-12 col-6">
                        <div class="card">
                            <div class="card-body">
                                <div class="card-title d-flex align-items-start justify-content-between">
                                    <div class="flex-shrink-0 avatar">
                                        <img src="<?php echo e(asset('assets_admin')); ?>/img/icons/unicons/chart-success.png"
                                            alt="chart success" class="rounded" />
                                    </div>
                                    <div class="dropdown">
                                        <button class="p-0 btn" type="button" id="cardOpt3" data-bs-toggle="dropdown"
                                            aria-haspopup="true" aria-expanded="false">
                                            <i class="bx bx-dots-vertical-rounded"></i>
                                        </button>
                                        <div class="dropdown-menu dropdown-menu-end" aria-labelledby="cardOpt3">
                                            <a class="dropdown-item" href="javascript:void(0);">View More</a>
                                        </div>
                                    </div>
                                </div>
                                <span class="mb-1 fw-semibold d-block">Pemasukan</span>
                                <h3 class="mb-2 card-title">Rp. <?php echo e(rupiah($pemasukan)); ?></h3>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
            <!-- Total Revenue -->
            
            <!--/ Total Revenue -->
            
        </div>
        
    </div>


    <script>
        $(document).ready(function() {
            $(".select2").select2({
                theme: 'bootstrap-5'
            })
        })
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('template.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\application\agratha\resources\views/admin/dashboard.blade.php ENDPATH**/ ?>