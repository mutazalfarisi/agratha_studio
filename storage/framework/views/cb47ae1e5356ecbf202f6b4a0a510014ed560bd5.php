<!-- resources/views/pdf/example.blade.php -->

<!DOCTYPE html>
<html>

<head>
    <title>Rekap Traksaksi</title>
</head>
<style>
    table{
        width: 100%;
        border-collapse: collapse;
    }

    table, th, td{
        border: 1px solid black;
    }

    th, td{
        padding: 8px;
        text-align: left;
    }
</style>
<body>
    <div style="text-align: center;">
        <header>
            <h2 style="margin-bottom: 0px;"><?php echo e(@$store->nama_toko); ?></h2>
            <p style="font-size:10px;"><?php echo e(ucwords(strtolower($store->alamat_toko))); ?>,
                <?php echo e($store->kabupaten . ' Kecamatan ' . $store->kecamatan . ' Provinsi ' . $store->provinsi . ', ' . $store->no_hp); ?>

            </p>
        </header>
        <hr>
        <div class="content" style="margin-top: 30px;">
            <table class="">
                <thead>
                    <th>No</th>
                    <th>Tanggal Transaksi</th>
                    <th>Nama Customer</th>
                    <th>Total</th>
                </thead>
                <tbody>
                    <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            <td style="width: 5px;"><?php echo e($loop->iteration); ?></td>
                            <td style="width: 30px;"><?php echo e(tanggal_indonesia($item->tgl_transaksi)); ?></td>
                            <td style="width: 50px;"><?php echo e(@$item->name); ?></td>
                            <td style="width: 25px;">Rp <?php echo e(rupiah(@$item->harga_total)); ?></td>
                        </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
            </table>
        </div>
    </div>
</body>

</html>
<?php /**PATH D:\application\basecampidn\resources\views/admin/cetak/rekap_transaksi.blade.php ENDPATH**/ ?>