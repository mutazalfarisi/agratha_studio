

<?php $__env->startSection('content'); ?>
    <!-- Begin Page Content -->
    <div class="container-fluid">
        <!-- Page Heading -->
        <div class="mb-4 d-sm-flex align-items-center justify-content-between">
            <h1 class="mb-0 text-gray-800 h3"><?php echo e(@$page_title); ?></h1>
            <a href="<?php echo e(url('admin/tambah/produk')); ?>" class="shadow-sm d-none d-sm-inline-block btn btn-sm btn-primary">
                <i class="text-white fas fa-plus fa-sm fa-fw"></i> Tambah Produk</a>
        </div>
        <!-- Content Row -->
        <div class="row">
            <div class="col-12">
                <div class="row">
                    <div class="card" style="width: 100%;">
                        <div class="card-body">
                            <?php if(session('success')): ?>
                                <div class="alert alert-success">
                                    <?php echo e(session('success')); ?>

                                </div>
                            <?php endif; ?>

                            <?php if(session('error')): ?>
                                <div class="alert alert-danger">
                                    <?php echo e(session('error')); ?>

                                </div>
                            <?php endif; ?>
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama Produk</th>
                                            <th>Stok</th>
                                            <th>Ukuran</th>
                                            <th>Gambar</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $no = 1;
                                    ?> 
                                    <?php $__currentLoopData = $dt_tas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <tr>
                                            <td style="width:5%!important;"><?php echo e($no++); ?></td>
                                            <td style="width:20%!important;"><?php echo e($row->nama_produk); ?></td>
                                            <td style="width:25%!important;"><?php echo e($row->deskripsi); ?></td>
                                            <td style="width:10%!important;"><?php echo e($row->stok); ?></td>
                                            <td style="width:20%!important;"> 
                                                <img src="<?php echo e(asset('storage/'.$row->foto)); ?>" style="width:70%;">
                                            </td>
                                            <td class="text-center" style="width:10%!important;">
                                                <button class="shadow-sm d-none d-sm-inline-block btn btn-sm btn-danger"><i class="text-white fas fa-trash fa-sm fa-fw"></i></button>
                                                <a href="<?php echo e(url('/edit/produk') .'/' . $row->id); ?>"class="shadow-sm d-none d-sm-inline-block btn btn-sm btn-warning"><i class="text-white fas fa-edit fa-sm fa-fw"></i></a>
                                            </td>
                                        </tr>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End of Main Content -->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.template.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\application\basecampidn\resources\views/admin/kelola/tas.blade.php ENDPATH**/ ?>