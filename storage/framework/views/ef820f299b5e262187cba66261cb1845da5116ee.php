<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.0/jquery.min.js"
        integrity="sha512-3gJwYpMe3QewGELv8k/BX9vcqhryRdzRMxVfq6ngyWXwo03GFEzjsUm8Q7RZcHPHksttq7/GFoxjCVUjkjvPdw=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <link href='https://unpkg.com/boxicons@2.1.4/css/boxicons.min.css' rel='stylesheet'>
    <title><?php echo e($title ?? "HUTSORGNL."); ?></title>
    <link rel="stylesheet" href="<?php echo e(asset('css/style.css')); ?>">
    <script src="<?php echo e(asset('js/script.js')); ?>"></script>
    <link rel="stylesheet" href="<?php echo e(asset('owlcarousel/dist/assets/owl.carousel.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('owlcarousel/dist/assets/owl.theme.default.min.css')); ?>">
    <script src="https://unpkg.com/boxicons@2.1.4/dist/boxicons.js"></script>
</head>

<body style="overflow-x:hidden;">
    <div class="d-block d-md-none mobile-bar">
        <img src="<?php echo e(@$li_active == 'keranjang' ? asset('images/icon/burger-bar.png'):asset('images/icon/burgerbar_white.png')); ?>" id="mobile-menu" style="width:24px;height:24px;">
    </div>
    <div class="shadow-sm mobile-navbar d-block d-md-none bg-light">
        <div class="close__mobile_nav">
            <img src="<?php echo e(asset('images/icon/close.png')); ?>" id="close-mobile-menu" alt="">
        </div>
        <div class="title-market d-flex justify-content-center" style="padding-top: 50px;">
            <a href="" style="text-decoration:none;color:black;">
                <h3 class="fw-bold">HUTS ORGNL.</h3>
            </a>
        </div>
        <div class="gap-3 p-2 header-menu d-flex justify-content-center" style="font-size: 12px;">
            <div class="header__menu_item <?php echo e(@$li_active == 'keranjang' ? 'active_header' :''); ?>">
                <a href="<?php echo e(url('/keranjang')); ?>"><i class='bx bx-shopping-bag'></i></a>
            </div>
            <div class="header__menu_item">
                <a href="<?php echo e(url('/login')); ?>"><i class='bx bx-user'></i></a>
            </div>
            <div class="header__menu_item">
                <a href="#"><i class='bx bx-search'></i></a>
            </div>
        </div>
        <div>
            <ul class="p-4 text-center nav flex-column">
                <li class="p-2 mb-4 nav-item <?php echo e(@$li_active == 'home' ? 'active_' :''); ?>">
                    <a class="nav-link" aria-current="page" href="<?php echo e(@$li_active != 'home' ? url('/') :'#product_all'); ?>">
                        <span data-feather="home"></span>
                        All Product
                    </a>
                </li>
                <li class="p-2 mb-4 nav-item ">
                    <a class="nav-link text-dark" aria-current="page" href="<?php echo e(@$li_active != 'home' ? url('/') :'#tshirt'); ?>">
                        <span data-feather="home"></span>
                        Tshirt
                    </a>
                </li>
                <li class="p-2 mb-4 nav-item ">
                    <a class="nav-link text-dark" aria-current="page" href="<?php echo e(@$li_active != 'home' ? url('/') :'#outwear'); ?>">
                        <span data-feather="home"></span>
                        Outwear
                    </a>
                </li>
                <li class="p-2 mb-4 nav-item ">
                    <a class="nav-link text-dark" aria-current="page" href="<?php echo e(@$li_active != 'home' ? url('/') :'#bag'); ?>">
                        <span data-feather="home"></span>
                        Bag
                    </a>
                </li>
            </ul>
        </div>
    </div>

    <div class="d-flex justify-content-between">
        <div class="sidebar__menu d-none d-md-block d-lg-block d-xl-block col-md-4 col-lg-4 col-xl-3">
            <div class="pt-3 shadow sticky-top min-vh-100" style="z-index:99;">
                <div class="title-market d-flex justify-content-center" style="padding: 50px;">
                    <a href="<?php echo e(asset('/')); ?>" style="text-decoration:none;color:black;">
                        <h3 class="fw-bold">HUTS ORGNL.</h3>
                    </a>
                </div>
                <div class="gap-3 p-2 header-menu d-flex justify-content-center" style="font-size: 12px;">
                    <div class="header__menu_item <?php echo e(@$li_active == 'keranjang' ? 'active_header' :''); ?> ">
                        <a href="<?php echo e(url('/keranjang')); ?>"><i class='bx bx-shopping-bag'></i></a>
                    </div>
                    <div class="header__menu_item">
                        <a href="<?php echo e(url('/login')); ?>"><i class='bx bx-user'></i></a>
                    </div>
                    <div class="header__menu_item">
                        <a href="#"><i class='bx bx-search'></i></a>
                    </div>
                </div>
                <div>
                    <ul class="p-2 nav flex-column">
                        <li class="p-2 mb-2 nav-item <?php echo e(@$li_active == 'home' ? 'active_' :''); ?>">
                            <a href="<?php echo e(@$li_active != 'home' ? url('/') :'#product_all'); ?>" class="nav-link" aria-current="page">
                                <span data-feather="home"></span>
                                All Product
                            </a>
                        </li>
                        <li class="p-2 mb-2 nav-item ">
                            <a href="<?php echo e(@$li_active != 'home' ? url('/') :'#tshirt'); ?>" class="nav-link text-dark" aria-current="page">
                                <span data-feather="home"></span>
                                Tshirt
                            </a>
                        </li>
                        <li class="p-2 mb-2 nav-item ">
                            <a href="<?php echo e(@$li_active != 'home' ? url('/') :'#outwear'); ?>" class="nav-link text-dark" aria-current="page">
                                <span data-feather="home"></span>
                                Outwear
                            </a>
                        </li>
                        <li class="p-2 mb-2 nav-item ">
                            <a href="<?php echo e(@$li_active != 'home' ? url('/') :'#bag'); ?>" class="nav-link text-dark" aria-current="page" >
                                <span data-feather="home"></span>
                                Bag
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="p-4 social__media" style="font-size:12px;">
                    <div>
                        <p>Sosial Media</p>
                    </div>
                    <div class="gap-3 social_media_icon d-flex">
                        <a href="https://web.facebook.com/mutaz.a.farisi/?locale=id_ID"><img src="<?php echo e(asset('images/icon/fb_gray.svg')); ?>" style="width: 25px;"
                                alt=""></a>
                        <a href="https://www.instagram.com/mutazalfarisii/"><img src="<?php echo e(asset('images/icon/instagram_black.svg')); ?>"
                                style="width: 25px;" alt=""></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="main__content col-12 col-md-8 col-lg-8 col-xl-9">
            <?php echo $__env->yieldContent('content'); ?>
            
            <div class="px-5 pt-5 footer__item bg-dark text-light">
                <div class="row d-flex">
                    <div class="order-2 col-12 order-lg-1 order-xl-1 col-lg-6 col-xl-6">
                        <div class="contact__us">
                            <h5>Contact Us</h5>
                            <p style="font-size: 12px;">Phone: 08123456789 <br>
                                Email: hutsoriginal@gmail.com <br>
                                Jl. Merak No 52 Gonilan, Kartasura, Jawa Tengah, Indonesia</p>
                        </div>
                        <div class="pb-5 menu__footer">
                            <a href="#">Home</a>
                            <a href="#">FAQ</a>
                            <a href="#">Track and Shipping</a>
                        </div>
                        <div class="mt-3 copyright__footer">
                            <p style="font-size: 12px;">© 2023 HutsOrgnl. All rights reserved.</p>
                        </div>
                    </div>
                    <div class="order-1 col-12 order-lg-2 order-xl-2 col-lg-6 col-xl-6 d-flex justify-content-center">
                        <iframe
                            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3955.2461933824025!2d110.76262987384015!3d-7.548109974519224!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e7a1460d7710d89%3A0x720df7e2ed718bd2!2sPustaka%20Qur&#39;an%20Sunnah!5e0!3m2!1sid!2sid!4v1693233735605!5m2!1sid!2sid"
                            style="border:0;border-radius:20px;width:100%;height:80%;" allowfullscreen=""
                            loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"
        integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js"
        integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous">
    </script>
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.0/jquery.min.js"
        integrity="sha512-3gJwYpMe3QewGELv8k/BX9vcqhryRdzRMxVfq6ngyWXwo03GFEzjsUm8Q7RZcHPHksttq7/GFoxjCVUjkjvPdw=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="<?php echo e(asset('owlcarousel/dist/owl.carousel.min.js')); ?>"></script>

    <script>
        $(document).on('click', '#mobile-menu', function() {
            $('.mobile-navbar').css('transform', 'translateX(0px)');
        })
        $(document).on('click', '#close-mobile-menu', function() {
            $('.mobile-navbar').css('transform', 'translateX(400px)');
        })
    </script>

</body>

</html>
<?php /**PATH D:\application\marketplace\resources\views/front/layout/default.blade.php ENDPATH**/ ?>