<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Alamatuser;
use App\Models\cart;
use App\Models\detail_transaksi;
use App\Models\ref_bank;
use App\Models\t_kab;
use App\Models\t_kec;
use App\Models\t_provinsi;
use App\Models\t_transaksi;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class CheckoutController extends Controller
{
    // function index($id = null)
    // {
    //     $dt_keranjang = Cart::select('a.*', 'b.nama_produk', 'b.deskripsi', 'b.harga', 'b.foto', 'b.stok', 'c.ukuran')->from('cart as a')
    //         ->leftJoin('t_produk as b', 'a.id_produk', '=', 'b.id')->leftJoin('ref_ukuran as c', 'a.ukuran', '=', 'c.id')
    //         ->where('a.id_user', auth()->user()->id)->where('a.id', @$id)->get();

    //     $dt_alamat = DB::table('alamatuser')->where('id_user', auth()->user()->id)->first();
    //     $jml_barang = 0;
    //     foreach ($dt_keranjang as $v) {
    //         $jml_barang += $v->jumlah;
    //     }
    //     $dt_bank = DB::table('ref_bank')->get();
    //     $data = [
    //         'title' => 'Checkout | Basecampidn.',
    //         'li_active' => "checkout",
    //         'jml_barang' => $jml_barang,
    //         'data' => $dt_keranjang,
    //         'dt_bank' => $dt_bank,
    //         'alamat' => $dt_alamat,
    //         'id_keranjang' => $id,
    //         'script_js' => 'customer/checkout.js'
    //     ];
    //     return view('front/checkout', $data);
    // }

    // function cek_ongkir(Request $request)
    // {
    //     $origin = DB::table('store')->where('id', 1)->first()->city_id;
    //     $tujuan = $request->tujuan;
    //     $berat = $request->berat;
    //     $kurir = $request->kurir;
    //     $curl = curl_init();

    //     curl_setopt_array($curl, array(
    //         CURLOPT_URL => "https://api.rajaongkir.com/starter/cost",
    //         CURLOPT_RETURNTRANSFER => true,
    //         CURLOPT_ENCODING => "",
    //         CURLOPT_MAXREDIRS => 10,
    //         CURLOPT_TIMEOUT => 30,
    //         CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    //         CURLOPT_CUSTOMREQUEST => "POST",
    //         CURLOPT_POSTFIELDS => "origin=" . $origin . "&destination=" . $tujuan . "&weight=" . $berat . "&courier=" . $kurir,
    //         CURLOPT_HTTPHEADER => array(
    //             "content-type: application/x-www-form-urlencoded",
    //             "key: 182f8720309712addbbb52b595eb7292"
    //         ),
    //     ));

    //     $response = curl_exec($curl);
    //     $err = curl_error($curl);

    //     curl_close($curl);

    //     $response = curl_exec($curl);
    //     $err = curl_error($curl);
    //     $dt_ongkir = json_decode($response);
    //     $costs = $dt_ongkir->rajaongkir->results[0]->costs;
    //     curl_close($curl);

    //     if ($err) {
    //         echo "cURL Error #:" . $err;
    //     } else {
    //         echo json_encode($costs);
    //     }
    // }

    // function jml_keranjang()
    // {
    //     $jml = cart::where('id_user', Auth::user()->id)->whereNull('deleted_at')->count();
    //     echo json_encode($jml);
    // }

    function upload_bukti(Request $request)
    {
        $request->validate(
            [
                'foto' => 'image|mimes:jpeg,png,jpg|max:5048', // Validasi tipe dan ukuran file
            ],
            [
                'foto.image' => 'File harus berupa gambar.',
                'foto.mimes' => 'Format file harus jpeg, png, atau jpg.',
                'foto.max' => 'Ukuran file tidak boleh melebihi 5 MB.',
            ],
        );

        $file = $request->file('bukti_transfer');
        $path = 'bukti_transfer';
        $file_foto = $this->upload_foto($path, $file);
        $dt = [
            'bukti_transfer' => @$file_foto,
            // 'status_pembayaran' => '1',
            'updated_at' => now()->format('Y-m-d H:i:s')
        ];
        $insert = t_transaksi::where('id', $request->id_transaksi)->update($dt);
        if ($insert) {
            session()->flash('success', 'Berhasil upload Bukti Transfer.');
        } else {
            session()->flash('error', 'Gagal upload Bukti Transfer.');
        }
        // return redirect()->route('keranjang');
        return redirect()->route('riwayat_pemesanan');
    }

    function upload_foto($path, $file)
    {
        if (!is_dir(storage_path("app/public/{$path}"))) {
            mkdir(storage_path("app/public/{$path}"), 0777, true);
        }

        $nama = $file->getClientOriginalName();
        $ext = pathinfo($nama, PATHINFO_EXTENSION);
        $namaFile = 'Bukti_transfer_' . time() . '.' . $ext;
        $file->storeAs("public/{$path}", $namaFile);

        return $path . '/' . $namaFile;
    }


    // function simpan(Request $request)
    // {
    //     $dt = [
    //         'id_user' => Auth::user()->id,
    //         'id_produk' => $request->id_produk,
    //         'jumlah' => $request->jumlah,
    //         'ukuran' => $request->ukuran,
    //         'status' => '0',
    //         'created_at' => now()->format('Y-m-d H:i:s')
    //     ];

    //     $insert = cart::insert($dt);
    //     if ($insert) {
    //         $res = [
    //             'status' => true,
    //             'pesan' => "Berhasil Menambahkan Ke Keranjang."
    //         ];
    //     } else {
    //         $res = [
    //             'status' => false,
    //             'pesan' => "Gagal Menambahkan Ke Keranjang."
    //         ];
    //     }

    //     echo json_encode($res);
    // }

    function hapus(Request $request)
    {
        // return $request->all();
        $id = $request->id;
        $dt = ['deleted_at' => now()->format('Y-m-d H:i:s')];
        $insert = t_transaksi::where('id', $id)->update($dt);
        if ($insert) {
            $res = [
                'status' => true,
                'pesan' => "Berhasil Menghapus."
            ];
        } else {
            $res = [
                'status' => false,
                'pesan' => "Gagal Menghapus."
            ];
        }

        echo json_encode($res);
    }

    function terima_pesanan(Request $request)
    {
        $id = $request->id;
        $dt = [
            'status_barang' => '3',
            'tgl_terima' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s')
        ];
        // print_r($dt);die;
        $update = t_transaksi::where('id', $id)->update($dt);
        if ($update) {
            $res = [
                'status' => true,
                'pesan' => "Berhasil Menyimpan."
            ];
        } else {
            $res = [
                'status' => false,
                'pesan' => "Gagal Menyimpan."
            ];
        }

        echo json_encode($res);
    }

    // function buat_pembayaran_old(Request $request)
    // {
    //     $harga_total = $request->harga_total;
    //     $harga_item = $request->harga_item;
    //     $harga_ongkir = $request->harga_ongkir;
    //     $id = $request->id;
    //     $kurir = $request->kurir;
    //     $jenis_pengiriman = $request->jenis_pengiriman;
    //     $dt_cart = [
    //         'status' => '1'
    //     ];
    //     if ($kurir == NULL || $jenis_pengiriman == NULL) {
    //         $res = [
    //             'status' => false,
    //             'pesan' => "Mohon Isi Data Pengiriman.",
    //         ];
    //     } else {


    //         cart::where('id', $id)->update($dt_cart);
    //         $get_data_produk = cart::where('id', $id)->first();
    //         $dt_transaksi = [
    //             'id_user' => auth()->user()->id,
    //             'no_transaksi' => uniqid(),
    //             'tgl_transaksi' => now()->format('Y-m-d H:i:s'),
    //             'tgl_exp' => now()->addHours(24)->format('Y-m-d H:i:s'),
    //             'harga_ongkir' => $harga_ongkir,
    //             'harga_item' => $harga_item,
    //             'harga_total' => $harga_total,
    //             'id_bank' => $request->id_bank,
    //             'kurir' => $kurir,
    //             'jenis_pengiriman' => $jenis_pengiriman,
    //             'status_barang' => '0',
    //             'status_pembayaran' => '0',
    //             'created_at' => now()->format('Y-m-d H:i:s'),
    //         ];

    //         $id_transaksi = t_transaksi::insertGetId($dt_transaksi);
    //         $dt_detail = [
    //             'id_transaksi' => $id_transaksi,
    //             'id_produk' => $get_data_produk->id_produk,
    //             'id_ukuran' => $get_data_produk->ukuran,
    //             'jml_barang' => $get_data_produk->jumlah,
    //             'created_at' => now()->format('Y-m-d H:i:s')
    //         ];
    //         $insert = detail_transaksi::insert($dt_detail);
    //         if ($insert) {
    //             $res = [
    //                 'status' => true,
    //                 'pesan' => "Berhasil CheckOut.",
    //                 'url' => route('keranjang')
    //             ];
    //         } else {
    //             $res = [
    //                 'status' => false,
    //                 'pesan' => "Gagal CheckOut."
    //             ];
    //         }
    //     }

    //     echo json_encode($res);
    // }

    function buat_pemesanan(Request $request)
    {
        // return $request->all();
        $request->validate([
            'id_paket' => ['required'],
            'paket' => ['required'],
            'prioritas' => ['required'],
            'harga' => ['required'],
            'nama' => ['required'],
            'provinsi' => ['required'],
            'kabupaten' => ['required'],
            'kecamatan' => ['required'],
            'alamat' => ['required'],
            'email' => ['required', 'email'],
            'no_hp' => ['required'],
            'bank' => ['required'],
        ],[
            'paket.required'=>'Paket tidak boleh kosong.',
            'prioritas.required'=>'Prioritas tidak boleh kosong.',
            'harga.required'=>'Harga tidak boleh kosong.',
            'nama.required'=>'Nama tidak boleh kosong.',
            'provinsi.required'=>'Provinsi tidak boleh kosong.',
            'kabupaten.required'=>'Kabupaten tidak boleh kosong.',
            'kecamatan.required'=>'Kecamatan tidak boleh kosong.',
            'alamat.required'=>'Alamat tidak boleh kosong.',
            'email.required'=>'Email tidak boleh kosong.',
            'no_hp.required'=>'Nomor HP tidak boleh kosong.',
            'tanggal_mulai.required'=>'Tanggal Mulai tidak boleh kosong.',
            'tanggal_selesai.required'=>'Tanggal Selesai tidak boleh kosong.',
            'anggaran_transport.required'=>'Anggaran Transport tidak boleh kosong.',
            'bank.required'=>'Pembayaran tidak boleh kosong.',
        ]);

        $provinsi = t_provinsi::where('province_id',$request->provinsi)->first()->province_name;
        $kabupaten = t_kab::where('city_id',$request->kabupaten)->first()->city_name;
        $kecamatan = t_kec::where('subdistrict_id',$request->kecamatan)->first()->subdistrict_name;
        $harga = str_replace('Rp ','',$request->harga);
        $harga = str_replace('.','',$harga);
        $harga = str_replace(' ','',$harga);

        $anggaran_transport = str_replace('Rp ','',$request->anggaran_transport);
        $anggaran_transport = str_replace('.','',$anggaran_transport);
        $anggaran_transport = str_replace(' ','',$anggaran_transport);

        $dt = [
            'id_user' => $request->id_user,
            'id_paket' => $request->id_paket,
            'no_transaksi' => "TR_" . Str::random(5).date('Ymd'),
            'paket' => $request->paket,
            'prioritas' => $request->prioritas,
            'harga' => $harga,
            'nama' => $request->nama,
            'provinsi' => $provinsi,
            'kabupaten' => $kabupaten,
            'kecamatan' => $kecamatan,
            'provinsi_id' => $request->provinsi,
            'kabupaten_id' => $request->kabupaten,
            'kecamatan_id' => $request->kecamatan,
            'alamat' => $request->alamat,
            'email' => $request->email,
            'id_bank' => $request->bank,
            'no_hp' => $request->no_hp,
            'tanggal_mulai' => $request->tanggal_mulai,
            'tanggal_selesai' => $request->tanggal_selesai,
            // 'anggaran_transport' => $request->anggaran_transport,
            'anggaran_transport' => $anggaran_transport,
        ];
        
        if ($request->id_transaksi) {
            $insert = t_transaksi::where('id',$request->id_transaksi)->update($dt);
        }else{
            $insert = t_transaksi::create($dt);
        }
        
        if ($insert) {
            session()->flash('success', 'Pesanan Berhasil di tambah.');
        } else {
            session()->flash('error', 'Pesanan Gagal di tambah.');
        }
        if ($request->id_transaksi) {
            return redirect('riwayat_pemesanan');
        }else{
            return redirect('detail_paket/'.$request->id_paket);
        }
    }
}
