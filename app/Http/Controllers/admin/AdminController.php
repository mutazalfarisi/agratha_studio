<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\t_provinsi;
use App\Models\t_kab;
use App\Models\t_kec;
use App\Models\Alamatuser;
use App\Models\Paket;
use App\Models\User;
use App\Models\store;
use App\Models\t_produk;
use App\Models\t_transaksi;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Ramsey\Uuid\Type\Integer;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $transaksi_bulan_ini = DB::table('t_transaksi')
            ->whereMonth('tgl_transaksi', now()->format('m'))
            ->whereYear('tgl_transaksi', now()->format('Y'))
            ->count();

        // $menunggu_validasi = DB::table('t_transaksi as a')
        //     ->select('a.*')
        //     ->whereNull('a.deleted_at')
            // ->where('a.status_pembayaran', '=', '1')
            // ->whereNull('a.id_admin')
            // ->where('a.status_barang', '=', '0')
            // ->count();


        $pemasukan = t_transaksi::where('is_verif', '1')->whereMonth('tgl_verif', '=', now()->format('m'))->sum('harga');
        $pemesanan = t_transaksi::whereMonth('created_at', '=', now()->format('m'))->count();

        $paket = Paket::count();
        $customer = User::where('role','customer')->count();

        // $dt_pemasukan = t_transaksi::select('a.*', 'b.nama')->from('t_transaksi as a')->leftjoin('users as b', 'a.id_user', '=', 'b.id')->where('a.status_barang', '3')->whereMonth('a.tgl_terima', '=', now()->format('m'))->where('a.tgl_terima', '!=', 'null')->get();

        // $rekap_pemasukan = DB::table('t_transaksi')
        //     ->select('harga_total', DB::raw('MONTH(tgl_transaksi) AS bulan_transaksi'))
        //     ->whereYear('tgl_transaksi', now()->format('Y'))
        //     ->where('status_barang', '3')
        //     ->get();

        // $bulan = ['januari', 'february', 'maret', 'april', 'mei', 'juni', 'july', 'agustus', 'september', 'oktober', 'november', 'desember'];
        // $dt_rekap = [];
        // foreach ($bulan as $index => $nama_bulan) {
        //     $bulan_ditemukan = false;

        //     foreach ($rekap_pemasukan as $rekap) {
        //         if ($rekap->bulan_transaksi == $index + 1) {
        //             $dt_rekap[] = ['harga' => (int) $rekap->harga_total, 'bulan' => ucfirst($nama_bulan)];
        //             $bulan_ditemukan = true;
        //             break;
        //         }
        //     }

        //     if (!$bulan_ditemukan) {
        //         $dt_rekap[] = ['harga' => (int) 0, 'bulan' => ucfirst($nama_bulan)];
        //     }
        // }

        $pilih_bulan = now()->format('m');
        // $restok = t_produk::where('stok', 0)->get();
        $data = [
            'title' => 'Dashboard',
            'menu' => 'Dashboard',
            'li_active' => 'dashboard',
            'transaksi_bulan_ini' => $transaksi_bulan_ini,

            // 'menunggu_validasi' => $menunggu_validasi,
            'pemasukan' => $pemasukan,
            'pemesanan' => $pemesanan,
            
            'customer' => $customer,
            'paket' => $paket,

            // 'dt_pemasukan' => $dt_pemasukan,
            // 'dt_rekap' => $dt_rekap,
            // 'restok' => count($restok),
            // 'dt_restok' => $restok,
            'pilih_bulan' => $pilih_bulan
        ];
        return view('admin/dashboard', $data);
    }

    public function get_rekap_bulan(Request $request)
    {
        $dt_pemasukan = t_transaksi::select('a.*', 'b.name')->from('t_transaksi as a')->leftjoin('users as b', 'a.id_user', '=', 'b.id')->where('a.status_barang', '3')->whereMonth('a.tgl_terima', '=', $request->bulan)->where('a.tgl_terima', '!=', 'null')->get();
        $data = [
            'dt_pemasukan' => $dt_pemasukan,
            'bulan' => $request->bulan
        ];

        return response()->json(['view' => view('admin/get_rekap_bulan', $data)->render()]);
    }

    public function profil()
    {
        $alamat = Alamatuser::where('id_user', Auth::user()->id)
            ->whereNull('deleted_at')
            ->first();
        $ref_provinsi = t_provinsi::all();
        $ref_kab = t_kab::where('province_id', @$alamat->province_id)->get();
        $ref_kec = t_kec::where('city_id', @$alamat->city_id)->get();

        $data = [
            'li_active' => 'dashboard',
            'menu' => 'Profil',
            'title' => 'Profil Admin',
            'alamat' => $alamat,
            'ref_provinsi' => $ref_provinsi,
            'ref_kab' => $ref_kab,
            'ref_kec' => $ref_kec,
        ];
        return view('admin/profil', $data);
    }

    public function alamat_toko()
    {
        $alamat = store::where('id', 1)->first();
        $ref_provinsi = t_provinsi::all();
        $ref_kab = t_kab::where('province_id', @$alamat->province_id)->get();
        $ref_kec = t_kec::where('city_id', @$alamat->city_id)->get();
        $data = [
            'page_title' => 'Alamat Toko',
            'li_active' => 'dashboard',
            'alamat' => $alamat,
            'ref_provinsi' => $ref_provinsi,
            'ref_kab' => $ref_kab,
            'ref_kec' => $ref_kec,
        ];
        return view('admin/alamat_toko', $data);
    }

    function simpan_profil(Request $request)
    {
        $dt = [
            'username' => $request->username,
            'nama' => $request->nama,
            'email' => $request->email,
            'no_hp' => $request->no_hp,
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ];

        if (!empty($request->provinsi)) {
            $provinsi = t_provinsi::where('province_id', $request->provinsi)->first();
        }
        if (!empty($request->kabupaten)) {
            $kabupaten = t_kab::where('city_id', $request->kabupaten)->first();
        }
        if (!empty($request->kecamatan)) {
            $kecamatan = t_kec::where('subdistrict_id', $request->kecamatan)->first();
        }

        $dt_alamat = [
            'id_user' => Auth::user()->id,
            'province_id' => @$request->provinsi,
            'city_id' => @$request->kabupaten,
            'subdistrict_id' => @$request->kecamatan,
            'provinsi' => @$provinsi->province_name,
            'kabupaten' => @$kabupaten->city_name,
            'kecamatan' => @$kecamatan->subdistrict_name,
            'detail_alamat' => @$request->alamat,
        ];
        User::where('id', Auth::user()->id)->update($dt);
        $cek_alamat = DB::table('alamatuser')
            ->where('id_user', Auth::user()->id)
            ->first();
        if (!empty($cek_alamat)) {
            $dt_alamat['updated_at'] = now()->format('Y-m-d H:i:s');
            $insert = Alamatuser::where('id', $cek_alamat->id)->update($dt_alamat);
        } else {
            $dt_alamat['created_at'] = now()->format('Y-m-d H:i:s');
            $insert = Alamatuser::insert($dt_alamat);
        }

        if ($insert) {
            session()->flash('success', 'Profil berhasil di simpan.');
        } else {
            session()->flash('error', 'Profil gagal di simpan.');
        }
        return redirect()->route('profil_admin');
    }

    function simpan_toko(Request $request)
    {
        $dt = [
            'nama_toko' => $request->nama_toko,
            'no_hp' => $request->no_hp,
            'province_id' => $request->provinsi,
            'city_id' => $request->kabupaten,
            'subdistrict_id' => $request->kecamatan,
            'alamat_toko' => $request->alamat_toko,
        ];

        if (!empty($request->provinsi)) {
            $provinsi = t_provinsi::where('province_id', $request->provinsi)->first();
            $dt['provinsi'] = $provinsi->province_name;
        }
        if (!empty($request->kabupaten)) {
            $kabupaten = t_kab::where('city_id', $request->kabupaten)->first();
            $dt['kabupaten'] = $kabupaten->city_name;
        }
        if (!empty($request->kecamatan)) {
            $kecamatan = t_kec::where('subdistrict_id', $request->kecamatan)->first();
            $dt['kecamatan'] = $kecamatan->subdistrict_name;
        }

        $cek_data = DB::table('store')
            ->where('id', 1)
            ->first();
        if (!empty($cek_data)) {
            $dt['updated_at'] = now()->format('Y-m-d H:i:s');
            $insert = store::where('id', $cek_data->id)->update($dt);
        } else {
            $dt['created_at'] = now()->format('Y-m-d H:i:s');
            $insert = store::insert($dt);
        }

        if ($insert) {
            session()->flash('success', 'Informasi Toko berhasil di simpan.');
        } else {
            session()->flash('error', 'Informasi Toko gagal di simpan.');
        }
        return redirect()->route('alamat_toko');
    }
}
