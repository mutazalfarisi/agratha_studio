<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Kategori;
use App\Models\Paket;
use App\Models\t_masukan;
use App\Models\t_produk;
use App\Models\t_transaksi;
use Carbon\Carbon;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class MasukanController extends Controller
{
    public function index()
    {
        // $dt_user = DB::table('users as a')
        //     ->leftJoin('alamatuser as b', 'a.id', '=', 'b.id_user')
        //     ->leftJoin('ref_provinsi as c', 'b.province_id', '=', 'c.province_id')
        //     ->leftJoin('ref_kab as d', 'b.city_id', '=', 'd.city_id')
        //     ->leftJoin('ref_kec as e', 'b.subdistrict_id', '=', 'e.subdistrict_id')
        //     ->select('a.*', 'c.province_name', 'd.city_name', 'e.subdistrict_name', 'b.detail_alamat')
        //     ->where('role','admin')
        //     ->whereNull('a.deleted_at')
        //     ->get();

        $dt = t_masukan::get();

        $data = [
            'title' => 'Kritik dan Saran',
            'menu' => 'Masukan',
            'li_active' => 'masukan',
            'data' => $dt,
        ];
        return view('admin/masukan/index', $data);
    }

    public function dataPaket(){
        $query = Paket::select('*'); // Select all columns from Paket model
        $data = $query->get();

        return DataTables::of($data)
            ->addIndexColumn()
            ->make(true);
    }

    function ceklist_masukan(Request $request){
        $insert = t_masukan::where('id',$request->id)->update(['status'=>'1']);
        if ($insert) {
            session()->flash('success', 'Berhasil Menyimpan.');
        }else{
            session()->flash('error', 'Gagal Menyimpan.');
        }
        
        return redirect()->route('masukan');
    }

}
