<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\detail_transaksi;
use App\Models\Kategori;
use App\Models\t_produk;
use App\Models\t_transaksi;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class KelolaController extends Controller
{
    public function kelola_kaos()
    {
        $dt_kaos = t_produk::whereNull('deleted_at')
            ->where('id_kategori', 1)
            ->get();
        $data = [
            'page_title' => 'Kelola Kaos',
            'li_active' => 'produk',
            'li_sub_active' => 'kelola_kaos',
            'dt_kaos' => $dt_kaos,
            'script_js' => 'admin/kelola/kaos.js',
        ];
        return view('admin/kelola/kaos', $data);
    }

    public function kelola_tas()
    {
        $dt_tas = t_produk::whereNull('deleted_at')
            ->where('id_kategori', 2)
            ->get();
        $data = [
            'page_title' => 'Kelola Tas',
            'dt_tas' => $dt_tas,
            'li_active' => 'produk',
            'li_sub_active' => 'kelola_tas',
            'script_js' => 'admin/kelola/tas.js',
        ];
        return view('admin/kelola/tas', $data);
    }

    public function transaksi_kaos()
    {
        $dt_acc = $this->get_data_transaksi('validasi', '1');
        $dt_dikirim = $this->get_data_transaksi('proses', '1');
        $dt_selesai = $this->get_data_transaksi('selesai', '1');

        $data = [
            'page_title' => 'Transaksi Kaos',
            'li_active' => 'transaksi',
            'li_sub_active' => 'transaksi_kaos',
            'dt_acc' => $dt_acc,
            'dt_dikirim' => $dt_dikirim,
            'dt_selesai' => $dt_selesai,
            'script_js' => 'admin/transaksi/kaos.js',
        ];
        return view('admin/transaksi/kaos', $data);
    }

    public function transaksi_tas()
    {
        $dt_acc = $this->get_data_transaksi('validasi', '2');
        // print_r($dt_acc);die;
        $dt_dikirim = $this->get_data_transaksi('proses', '2');
        $dt_selesai = $this->get_data_transaksi('selesai', '2');

        $data = [
            'page_title' => 'Transaksi Tas',
            'li_active' => 'transaksi',
            'li_sub_active' => 'transaksi_tas',
            'dt_acc' => $dt_acc,
            'dt_dikirim' => $dt_dikirim,
            'dt_selesai' => $dt_selesai,
            'script_js' => 'admin/transaksi/tas.js',
        ];
        return view('admin/transaksi/tas', $data);
    }

    public function pemasukan()
    {
        $filter = [];
        $data = $this->get_data_pemasukan(@$filter);
        $data = [
            'page_title' => 'Pemasukan',
            'li_active' => 'pemasukan',
            'li_sub_active' => '',
            'pemasukan' => $data,
            'script_js' => 'admin/keuangan/pemasukan.js',
        ];
        return view('admin/keuangan/pemasukan', $data);
    }

    public function pemasukan_filter(Request $request)
    {
        $filter = [
            'mulai' => $request->input('tanggal_mulai'),
            'selesai' => $request->input('tanggal_selesai'),
        ];
        $data = $this->get_data_pemasukan(@$filter);
        $data = [
            'page_title' => 'Pemasukan',
            'li_active' => 'pemasukan',
            'li_sub_active' => '',
            'filter' => $filter,
            'pemasukan' => $data,
            'script_js' => 'admin/keuangan/pemasukan.js',
        ];
        return view('admin/keuangan/pemasukan', $data);
    }

    function get_data_pemasukan($filter)
    {
        $columns = ['a.id', 'a.no_transaksi', 'c.jml_barang', 'a.tgl_transaksi', 'a.tgl_exp', 'a.tgl_terima', 'a.harga_total', 'a.harga_item', 'a.bukti_transfer', 'b.name'];
        $query = t_transaksi::from('t_transaksi as a')
            ->select($columns)
            ->leftJoin('users as b', 'a.id_user', '=', 'b.id')
            ->leftJoin('detail_transaksi as c', 'a.id', '=', 'c.id_transaksi');

        if (!empty($filter['mulai']) && !empty($filter['selesai'])) {
            $query->whereBetween('a.tgl_transaksi', [$filter['mulai'], $filter['selesai']]);
        }

        $query->where('a.status_barang', '3');

        $results = $query->get();
        return $results;
    }

    function get_data_transaksi($status, $kategori)
    {
        $query = DB::table('t_transaksi as a')
            ->select('a.*', 'e.name AS nama', 'e.no_hp', 'f.provinsi', 'f.kabupaten', 'f.kecamatan', 'f.detail_alamat', 'b.id_produk', 'd.harga', 'd.stok', 'd.foto', 'd.nama_produk', 'd.deskripsi', 'b.jml_barang')
            ->leftJoin('detail_transaksi as b', 'a.id', '=', 'b.id_transaksi')
            ->leftJoin('ref_ukuran as c', 'b.id_ukuran', '=', 'c.id')
            ->leftJoin('t_produk as d', 'b.id_produk', '=', 'd.id')
            ->leftJoin('users as e', 'a.id_user', '=', 'e.id')
            ->leftJoin('alamatuser as f', 'e.id', '=', 'f.id_user')
            ->whereNull('a.deleted_at')
            ->where('a.status_pembayaran', '=', '1');
        if ($status == 'validasi') {
            $query->whereNull('a.id_admin')->where('a.status_barang', '=', '0');
        } elseif ($status == 'proses') {
            $query->whereIn('a.status_barang', ['1', '2']);
        } else {
            $query->where('a.status_barang', '=', '3');
        }
        if ($kategori == '2') {
            $query->where('d.id_kategori', '=', '2');
        } else {
            $query->where('d.id_kategori', '=', '1');
        }
        return $query->get();
        // return $query->toSql();
    }

    function get_detail_transaksi(Request $request)
    {
        $id = $request->id;
        $data = DB::table('t_transaksi as a')
            ->select(
                'b.id_produk',
                'd.harga',
                'd.stok',
                'd.foto',
                'd.nama_produk',
                'd.deskripsi',
                'b.jml_barang',
                'c.ukuran',
                'a.harga_ongkir',
                'a.harga_total'
            )
            ->leftJoin('detail_transaksi as b', 'a.id', '=', 'b.id_transaksi')
            ->leftJoin('ref_ukuran as c', 'b.id_ukuran', '=', 'c.id')
            ->leftJoin('t_produk as d', 'b.id_produk', '=', 'd.id')
            ->where('a.id', $id)
            ->get();

        $table = view('admin/transaksi/detail_transaksi', ['data' => $data])->render();

        echo json_encode($table);
    }

    public function validasi_transaksi(Request $request)
    {
        $id = $request->id;
        $tipe = $request->tipe;
        $transaksi = DB::table('t_transaksi as a')
            ->select('a.*', 'b.id_produk', 'b.jml_barang')
            ->leftJoin('detail_transaksi as b', 'a.id', '=', 'b.id_transaksi')
            ->where('a.id', $id)
            ->get();

        if (count($transaksi) == 0) {
            $res = [
                'status' => false,
                'pesan' => 'Data tidak ditemukan',
            ];
        } else {
            if ($tipe == 'setuju') {
                $update = t_transaksi::where('id', $id)->update(['id_admin' => auth()->user()->id, 'status_barang' => '1']);
                foreach ($transaksi as $key) {
                    $data_produk = t_produk::where('id', $key->id_produk)->first();
                    $jml = $data_produk->stok;
                    t_produk::where('id', $key->id_produk)->update(['stok' => ($jml - $key->jml_barang)]);
                }
            } else {
                $update = t_transaksi::where('id', $id)->update(['id_admin' => '0']);
            }
            if ($update) {
                $res = [
                    'status' => true,
                    'pesan' => 'Berhasil Di Simpan.',
                ];
            }
        }
        echo json_encode($res);
    }

    public function ganti_status(Request $request)
    {
        $id = $request->id;
        $value = $request->value;
        $cek_id = DB::table('t_transaksi as a')
            ->where('a.id', $id)
            ->get();

        if (count($cek_id) == 0) {
            $res = [
                'status' => false,
                'pesan' => 'Data tidak ditemukan',
            ];
        } else {
            $dt = [
                'status_barang' => $value,
            ];
            if ($value == '3') {
                $dt['tgl_terima'] = now()->format('Y-m-d H:i:s');
                $dt['updated_at'] = now()->format('Y-m-d H:i:s');
            }
            $update = t_transaksi::where('id', $id)->update($dt);
            if ($update) {
                $res = [
                    'status' => true,
                    'pesan' => 'Berhasil Di Simpan.',
                ];
            }
        }
        echo json_encode($res);
    }

    public function tambah_kaos()
    {
        $data = [
            'page_title' => 'Tambah Kaos',
            'li_active' => 'produk',
            'li_sub_active' => 'kelola_kaos',
            'script_js' => 'admin/kelola/kaos.js',
        ];
        return view('admin/kelola/tambah_kaos', $data);
    }

    public function tambah_produk()
    {
        $ref_kategori = Kategori::all();
        $data = [
            'title' => 'Tambah Produk',
            'page_title' => 'Tambah Produk',
            'li_active' => 'produk',
            'li_sub_active' => 'kelola_kaos',
            'ref_kategori' => $ref_kategori,
            'script_js' => 'admin/kelola/produk.js',
        ];
        return view('admin/kelola/tambah_produk', $data);
    }

    public function edit_produk($id)
    {
        $produk = t_produk::where('id', $id)->first();
        $ref_kategori = Kategori::all();
        $data = [
            'title' => 'Tambah Produk',
            'page_title' => 'Tambah Produk',
            'li_active' => 'produk',
            'li_sub_active' => 'kelola_kaos',
            'produk' => $produk,
            'ref_kategori' => $ref_kategori,
            'script_js' => 'admin/kelola/produk.js',
        ];
        return view('admin/kelola/tambah_produk', $data);
    }

    function simpan(Request $request)
    {
        $request->validate(
            [
                'produk' => 'required|unique:t_produk,nama_produk',
                'deskripsi' => 'required',
                'harga' => 'required',
                'kategori' => 'required',
                'foto' => 'image|mimes:jpeg,png,jpg|max:5048', // Validasi tipe dan ukuran file
            ],
            [
                'produk.required' => 'Nama Produk Tidak Boleh Kosong',
                'produk.unique' => 'Nama Produk Sudah Ada Dalam Database.',
                'deskripsi.required' => 'Deskripsi Tidak Boleh Kosong',
                'harga.required' => 'Harga Tidak Boleh Kosong',
                'kategori.required' => 'Kategori Tidak Boleh Kosong',
                'foto.image' => 'File harus berupa gambar.',
                'foto.mimes' => 'Format file harus jpeg, png, atau jpg.',
                'foto.max' => 'Ukuran file tidak boleh melebihi 5 MB.',
            ],
        );


        $kategori = Kategori::where('id', $request->kategori)->first()->kategori;
        if ($kategori == 'Kaos') {
            $path = 'produk/kaos';
        } else {
            $path = 'produk/tas';
        }

        $harga = str_replace(['Rp', '.', ' '], '', $request->harga);

        $dt = [
            'id_kategori' => $request->kategori,
            'nama_produk' => $request->produk,
            'deskripsi' => $request->deskripsi,
            'harga' => intval($harga),
            'stok' => $request->stok,
        ];

        $file = $request->file('foto');
        if (@$file != null) {
            $file_foto = $this->upload_foto($path, $file);
            $dt['foto'] = @$file_foto;
        }
        if (!empty($request->id)) {
            $dt['updated_at'] = now()->format('Y-m-d H:i:s');
            $insert = t_produk::where('id', $request->id)->update($dt);
        } else {
            $dt['created_at'] = now()->format('Y-m-d H:i:s');
            $insert = t_produk::insert($dt);
        }

        if ($insert) {
            session()->flash('success', 'Produk berhasil di tambahkan.');
        } else {
            session()->flash('error', 'Produk gagal di tambahkan.');
        }

        if ($kategori == 'Kaos') {
            return redirect()->route('kelola_kaos');
        } else {
            return redirect()->route('kelola_tas');
        }
    }

    function upload_foto($path, $file)
    {
        if (!is_dir(storage_path("app/public/{$path}"))) {
            mkdir(storage_path("app/public/{$path}"), 0777, true);
        }

        $nama = $file->getClientOriginalName();
        $ext = pathinfo($nama, PATHINFO_EXTENSION);
        $namaFile = 'Produk_' . time() . '.' . $ext;
        $file->storeAs("public/{$path}", $namaFile);

        return $path . '/' . $namaFile;
    }
}
