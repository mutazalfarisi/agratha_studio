<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Kategori;
use App\Models\t_produk;
use App\Models\t_transaksi;
use Carbon\Carbon;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class PemesananController extends Controller
{
    // public function index()
    // {
    //     $pemesanan = t_transaksi::with(['refBank', 'refPrioritas', 'admin'])->get();

    //     $data = [
    //         'title' => 'Daftar Pemesanan',
    //         'menu' => 'Manajemen',
    //         'li_active' => 'daftar_pemesanan',
    //         'pemesanan' => $pemesanan,
    //     ];
    //     return view('admin/daftar_pemesanan/index', $data);
    // }

    public function verifikasi()
    {
        $data = [
            'title' => 'Daftar Pemesanan',
            'menu' => 'Manajemen',
            'li_active' => 'daftar_pemesanan',
            'li_sub_active' => 'verifikasi',
        ];
        return view('admin/daftar_pemesanan/index', $data);
    }

    public function belum_verifikasi()
    {
        $data = [
            'title' => 'Daftar Pemesanan',
            'menu' => 'Manajemen',
            'li_active' => 'daftar_pemesanan',
            'li_sub_active' => 'belum_verifikasi',
        ];

        return view('admin/daftar_pemesanan/index', $data);
    }

    public function dataPemesanan(Request $request)
    {
        $data = t_transaksi::with(['refBank', 'refPaket', 'refPrioritas', 'admin'])->whereNull('is_selesai');

        if ($request->tanggal_mulai != null && $request->tanggal_selesai != null) {
            $data->where(function ($query) use ($request) {
                $query->whereBetween('tanggal_mulai', [$request->tanggal_mulai, $request->tanggal_selesai])->orWhereBetween('tanggal_selesai', [$request->tanggal_mulai, $request->tanggal_selesai]);
            });
        } elseif ($request->tanggal_mulai != null && $request->tanggal_selesai == null) {
            $data->where(function ($query) use ($request) {
                $query->where('tanggal_mulai', '>=', $request->tanggal_mulai)->orWhere('tanggal_selesai', '>=', $request->tanggal_mulai);
            });
        } elseif ($request->tanggal_mulai == null && $request->tanggal_selesai != null) {
            $data->where(function ($query) use ($request) {
                $query->where('tanggal_selesai', '<=', $request->tanggal_selesai)->orWhere('tanggal_selesai', '>=', $request->tanggal_selesai);
            });
        }

        $data->where(function ($query) use ($request) {
            if ($request->tipe == 'verifikasi') {
                $query->where('is_verif', '1');
            } else {
                $query->where('is_verif', '0')->orWhereNull('is_verif');
            }
        });

        $data->orderBy('tanggal_mulai','asc')
            ->orderBy('prioritas') // Pastikan relasi refPrioritas memiliki kolom 'prioritas'
            ->orderByRaw('(prioritas + id_paket)') // Kombinasi prioritas dan id_paket
            ->orderBy('tgl_verif');

        $data = $data->get();
        // return $data;

        $data = $data->map(function ($item, $index) {
            return [
                'id' => $item->id,
                'nama' => $item->nama,
                'email' => $item->email,
                'no_hp' => $item->no_hp,
                'paket' => @$item->refPaket->nama_paket,
                'nama_prioritas' => $item->refPrioritas->nama_prioritas,
                'harga' => rupiah($item->harga),
                'anggaran_transport' => rupiah($item->anggaran_transport),
                'tanggal_pemesanan' => tanggal_indonesia($item->created_at),
                'tanggal' => formatrangetanggal($item->tanggal_mulai, $item->tanggal_selesai),
                'lokasi' => $item->kabupaten . ', ' . $item->alamat,
                'harga_total' => rupiah($item->harga + $item->anggaran_transport),
                'pembayaran' => @$item->refBank->nama_bank,
                'no_rek' => @$item->refBank->no_rek,
                'bukti_transfer' => $item->bukti_transfer,
                'nama_admin' => @$item->admin->nama,
                'is_admin' => @$item->is_admin,
                'tgl_verif' => $item->tgl_verif,
                'is_verif' => $item->is_verif,
                'item' => $item,
            ];
        });

        return DataTables::of($data)->addIndexColumn()->make(true);
    }

    function verifikasi_data(Request $request)
    {
        $dt = t_transaksi::where('id', $request->id)->first();

        $dt_update = [
            'is_admin' => Auth::user()->id,
            'tgl_verif' => date('Y-m-d H:i:s'),
        ];
        
        if ($dt->is_verif == '1') {
            $dt_update['is_verif'] = '0';
        } else {
            $dt_update['is_verif'] = '1';
        }

        try {
            t_transaksi::where('id', $request->id)->update($dt_update);

            return response()->json(
                [
                    'status' => true,
                    'pesan' => 'Berhasil Menyimpan',
                    'data' => $dt_update,
                ],
                200,
            );
        } catch (\Throwable $th) {
            return response()->json(
                [
                    'status' => false,
                    'pesan' => 'Gagal Menyimpan',
                    'data' => $dt_update,
                ],
                500,
            );
        }
    }

    function verifikasi_data_selesai(Request $request)
    {
        $dt_update = [
            'is_selesai' => 1,
        ];
        
        try {
            t_transaksi::where('id', $request->id)->update($dt_update);

            return response()->json(
                [
                    'status' => true,
                    'pesan' => 'Berhasil Menyimpan',
                    'data' => $dt_update,
                ],
                200,
            );
        } catch (\Throwable $th) {
            return response()->json(
                [
                    'status' => false,
                    'pesan' => 'Gagal Menyimpan',
                    'data' => $dt_update,
                ],
                500,
            );
        }
    }

    function upload_foto($path, $file)
    {
        if (!is_dir(storage_path("app/public/{$path}"))) {
            mkdir(storage_path("app/public/{$path}"), 0777, true);
        }

        $nama = $file->getClientOriginalName();
        $ext = pathinfo($nama, PATHINFO_EXTENSION);
        $namaFile = 'Produk_' . time() . '.' . $ext;
        $file->storeAs("public/{$path}", $namaFile);

        return $path . '/' . $namaFile;
    }
}
