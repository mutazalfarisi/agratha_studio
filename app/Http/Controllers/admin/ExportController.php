<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\detail_transaksi;
use App\Models\Kategori;
use App\Models\t_produk;
use App\Models\t_transaksi;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use PDF;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class ExportController extends Controller
{
    public function cetak($id)
    {
        $store = DB::table('store')->first();

        $data = t_transaksi::with(['refBank','refPaket', 'refPrioritas', 'admin'])
            ->where('id', $id)
            ->first();

        $dt = [
            'data' => $data,
            'store' => $store,
        ];

        $pdf = PDF::loadView('admin.cetak.invoice', $dt);
        return $pdf->stream('invoice.pdf');
    }

    public function cetak_rekap($tgl_mulai = null, $tgl_selesai = null){
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        // Menambahkan data header
        $sheet->setCellValue('A1', 'No');
        $sheet->setCellValue('B1', 'Nomor Transaksi');
        $sheet->setCellValue('B1', 'Nama');
        $sheet->setCellValue('C1', 'Email');
        $sheet->setCellValue('D1', 'Nomor HP');
        $sheet->setCellValue('E1', 'Paket');
        $sheet->setCellValue('F1', 'Prioritas');
        $sheet->setCellValue('G1', 'Tanggal Pemesanan');
        $sheet->setCellValue('H1', 'Tanggal Acara');
        $sheet->setCellValue('I1', 'Lokasi');
        $sheet->setCellValue('J1', 'Harga');
        $sheet->setCellValue('K1', 'Anggaran Transport');
        $sheet->setCellValue('L1', 'Harga Total');
        $sheet->setCellValue('M1', 'Pembayaran');
        $sheet->setCellValue('N1', 'Status');

        // Menambahkan data pengguna
        $data = t_transaksi::with(['refBank','refPaket', 'refPrioritas', 'admin']);

        if ($tgl_mulai != null && $tgl_selesai != null) {
            $data->where(function ($query) use ($tgl_mulai,$tgl_selesai) {
                $query->whereBetween('tanggal_mulai', [$tgl_mulai, $tgl_selesai])->orWhereBetween('tanggal_selesai', [$tgl_mulai, $tgl_selesai]);
            });
        } elseif ($tgl_mulai != null && $tgl_selesai == null) {
            $data->where(function ($query) use ($tgl_mulai,$tgl_selesai) {
                $query->where('tanggal_mulai', '>=', $tgl_mulai)->orWhere('tanggal_selesai', '>=', $tgl_mulai);
            });
        } elseif ($tgl_mulai == null && $tgl_selesai != null) {
            $data->where(function ($query) use ($tgl_mulai,$tgl_selesai) {
                $query->where('tanggal_selesai', '<=', $tgl_selesai)->orWhere('tanggal_selesai', '>=', $tgl_selesai);
            });
        }

        $data->where(function ($query) use ($tgl_mulai,$tgl_selesai) {
                $query->where('is_verif', '1');
        });

        $data = $data->get();

        $row = 2;
        $no = 1;
        foreach ($data as $dt) {
            $sheet->setCellValue('A' . $row, $no++);
            $sheet->setCellValue('B' . $row, $dt->nomor_transaksi);
            $sheet->setCellValue('C' . $row, $dt->nama);
            $sheet->setCellValue('D' . $row, $dt->email);
            $sheet->setCellValue('E' . $row, $dt->no_hp);
            $sheet->setCellValue('F' . $row, $dt->refPaket->nama_paket);
            $sheet->setCellValue('G' . $row, $dt->refPrioritas->nama_prioritas);
            $sheet->setCellValue('H' . $row, $dt->created_at);
            $sheet->setCellValue('I' . $row, formatrangetanggal($dt->tanggal_mulai, $dt->tanggal_selesai));
            $sheet->setCellValue('J' . $row, $dt->kabupaten . ', ' . $dt->alamat);
            $sheet->setCellValue('K' . $row, $dt->harga);
            $sheet->setCellValue('L' . $row, $dt->anggaran_transport);
            $sheet->setCellValue('M' . $row, $dt->refBank->nama_bank);
            $sheet->setCellValue('N' . $row, 'Lunas');
            
            $row++;
        }

        $styleArray = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ],
            ],
        ];
        $sheet->getStyle('A1:N' . $row - 1)->applyFromArray($styleArray);
        

        $writer = new Xlsx($spreadsheet);
        $fileName = 'Laporan Pemesanan.xlsx';

        $writer->save($fileName);

        return response()->download($fileName)->deleteFileAfterSend(true);

    }


    public function cetak_transaksi(Request $request)
    {
        $store = DB::table('store')->first();

        $tglMulai = $request->input('tgl_mulai');
        $tglSelesai = $request->input('tgl_selesai');
        $query = DB::table('t_transaksi as a')->select('a.*', 'b.id_produk', 'd.harga', 'd.stok', 'd.foto', 'd.nama_produk', 'd.deskripsi', 'b.jml_barang', 'c.ukuran', 'f.name')->leftJoin('detail_transaksi as b', 'a.id', '=', 'b.id_transaksi')->leftJoin('ref_ukuran as c', 'b.id_ukuran', '=', 'c.id')->leftJoin('t_produk as d', 'b.id_produk', '=', 'd.id')->leftJoin('users as f', 'a.id_user', '=', 'f.id');

        if ($tglMulai != null || $tglSelesai != null) {
            $query->whereBetween('a.tgl_transaksi', [$tglMulai, $tglSelesai]);
        }
        $data = $query->get();
        $dt = [
            'data' => $data,
            'store' => $store,
        ];

        $pdf = PDF::loadView('admin.cetak.rekap_transaksi', $dt);
        return $pdf->stream('rekap_transaksi.pdf');
    }

    public function cetak_transaksi_bulan($bulan)
    {
        $store = DB::table('store')->first();
        // $data = t_transaksi::select('a.*', 'b.name')->from('t_transaksi as a')->leftjoin('users as b', 'a.id_user', '=', 'b.id')->where('a.status_barang', '3')->whereMonth('a.tgl_terima', '=', now()->format('m'))->where('a.tgl_terima', '!=', 'null')->get();
        $data = t_transaksi::select('a.*', 'b.name')->from('t_transaksi as a')->leftjoin('users as b', 'a.id_user', '=', 'b.id')->where('a.status_barang', '3')->whereMonth('a.tgl_terima', '=', $bulan)->where('a.tgl_terima', '!=', 'null')->get();
        $dt = [
            'data' => $data,
            'store' => $store,
            'bulan' => $bulan,
        ];

        $pdf = PDF::loadView('admin.cetak.transaksi_perbulan', $dt);
        return $pdf->stream('rekap_transaksi_' . getBulan() . '.pdf');
    }
}
