<?php

namespace App\Http\Controllers\front;

use App\Http\Controllers\Controller;
use App\Models\ref_bank;
use App\Models\RefPrioritas;
use App\Models\t_kab;
use App\Models\t_kec;
use App\Models\t_masukan;
use App\Models\t_paket;
use App\Models\t_provinsi;
use App\Models\t_transaksi;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class FrontController extends Controller
{
    function index()
    {
        $dt_paket = t_paket::limit(3)->get();
        $data = [
            'li_active' => 'home',
            'active' => 'beranda',
            'dt_paket' => $dt_paket,
            // 'dt_tas' => $dt_tas,
        ];
        return view('front/home', $data);
    }

    function show_paket()
    {
        $dt_paket = t_paket::get();
        $data = [
            'li_active' => 'home',
            'active' => 'paket',
            'dt_paket' => $dt_paket,
        ];
        return view('front/show_paket', $data);
    }

    function show_galeri()
    {
        $dt_paket = t_paket::get();
        $data = [
            'li_active' => 'home',
            'active' => 'galeri',
            'dt_paket' => $dt_paket,
        ];
        return view('front/show_galeri', $data);
    }

    function kontak_kami()
    {
        $dt_paket = t_paket::get();
        $data = [
            'li_active' => 'home',
            'active' => 'kontak',
            'dt_paket' => $dt_paket,
        ];
        return view('front/kontak_kami', $data);
    }

    function tentang()
    {
        $dt_paket = t_paket::get();
        $data = [
            'li_active' => 'home',
            'active' => 'tentang',
            'dt_paket' => $dt_paket,
        ];
        return view('front/tentang', $data);
    }

    function detail_paket($id)
    {
        $dt_paket = t_paket::where('id',$id)->first();
        $provinsi = t_provinsi::all();
        $prioritas = RefPrioritas::all();
        $bank = ref_bank::all();
        $user = Auth::user();

        $data = [
            'li_active' => 'home',
            'active' => 'paket',
            'user'=>$user,
            'provinsi'=>$provinsi,
            'bank'=>$bank,
            'prioritas'=>$prioritas,
            'row' => $dt_paket,
        ];
        return view('front/detail_paket', $data);
    }

    function store_pesan(Request $request){
        $dt = [
            'nama'=>$request->nama,
            'no_hp'=>$request->no_hp,
            'email'=>$request->email,
            'pesan'=>$request->pesan
        ];

        $insert = t_masukan::create($dt);

        if ($insert) {
            session()->flash('success', 'Masukan Berhasil Dikirim.');
        } else {
            session()->flash('error', 'Masukan Gagal Dikirim.');
        }

        return redirect('kontak_kami');
    }

    function get_pemesanan(Request $request){
        $pemesanan = t_transaksi::with(['refBank'])->where('id',$request->id)->first();
        return $pemesanan;
    }

    function riwayat_pemesanan()
    {
        $pemesanan = t_transaksi::with(['refBank','refPrioritas'])->where('id_user',Auth::user()->id)->get();
        $prioritas = RefPrioritas::all();
        $provinsi = t_provinsi::all();
        // $kabupaten = t_kab::where('province_id',$pemesanan->provinsi_id)->get();
        $kabupaten = t_kab::all();
        // $kecamatan = t_kec::where('city_id',$pemesanan->kabupaten_id)->get();
        $kecamatan = t_kec::all();
        $bank = ref_bank::all();
        $data = [
            // 'li_active' => 'home',
            // 'active' => 'paket',
            'pemesanan' => $pemesanan,
            'prioritas'=>$prioritas,
            'provinsi'=>$provinsi,
            'kabupaten'=>$kabupaten,
            'kecamatan'=>$kecamatan,
            'bank'=>$bank,
            // 'dt_tas' => $dt_tas,
        ];
        return view('front/riwayat_pemesanan', $data);
    }

    function cek()
    {
        $date = '2023-01-01 11:44:45';
        $data = [
            'cek' => Carbon::parse($date)->isoFormat('dddd, D MMMM Y'),
        ];
        return view('front/cek', $data);
    }

    // function search_produk(Request $request)
    // {
    //     $keyword = $request->keyword;
    //     $result = [];
    //     if ($keyword != '' || $keyword != null) {
    //         $result = t_produk::whereNull('deleted_at')
    //             ->where('nama_produk', 'like', '%' . $keyword . '%')
    //             ->limit(5)
    //             ->get();
    //     }
    //     $data = [
    //         'data' => $result,
    //         'keyword' => $keyword,
    //     ];
    //     $view = view('front/result_search', $data);
    //     return $view;
    // }

    // function result_produk($keyword)
    // {
    //     $result = t_produk::whereNull('deleted_at')
    //         ->where('nama_produk', 'like', '%' . $keyword . '%')
    //         ->get();
    //     $data = [
    //         'li_active' => 'search',
    //         'data' => $result,
    //     ];

    //     return view('front/search', $data);
    // }
}
