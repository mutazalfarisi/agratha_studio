<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ref_bank extends Model
{
    protected $table = 'ref_bank';
    protected $guarded = [];
}
