<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class t_provinsi extends Model
{
    protected $table = 'ref_provinsi';
    protected $guarded = [];
}
