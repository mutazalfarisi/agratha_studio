<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\ref_bank;
use Illuminate\Database\Eloquent\SoftDeletes;

class t_transaksi extends Model
{
    protected $table = 't_transaksi';
    protected $guarded = ['id'];
    use SoftDeletes;

    function refBank(){
        return $this->hasOne(ref_bank::class, 'id' ,'id_bank');
    }

    function refPaket(){
        return $this->hasOne(Paket::class, 'id' ,'id_paket');
    }

    function refPrioritas(){
        return $this->hasOne(refPrioritas::class, 'id' ,'prioritas');
    }

    function admin(){
        return $this->hasOne(User::class, 'id' ,'is_admin');
    }
}
