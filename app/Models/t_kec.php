<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class t_kec extends Model
{
    protected $table = 'ref_kec';
    protected $guarded = [];
}
