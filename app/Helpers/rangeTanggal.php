<?php

// namespace App\Helpers;

use Carbon\Carbon;

// class rangeTanggal
// {
function formatrangetanggal($startDate, $endDate)
{
    $start = Carbon::parse($startDate);
    $end = Carbon::parse($endDate);

    // Check if the dates are on the same day
    if ($start->isSameDay($end)) {
        return $start->format('d M Y');
    }

    // Check if the dates are within the same month
    if ($start->isSameMonth($end)) {
        return $start->format('d') . ' - ' . $end->format('d M Y');
    }

    // Check if the dates are within the same year
    if ($start->isSameYear($end)) {
        return $start->format('d M') . ' - ' . $end->format('d M Y');
    }

    // Default format for different months or years
    return $start->format('d M Y') . ' - ' . $end->format('d M Y');
}
// }
