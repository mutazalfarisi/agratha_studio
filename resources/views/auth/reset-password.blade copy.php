
<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Hugo 0.84.0">
    <title>{{ $title ?? 'Agratha Studio.' }}</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.0/jquery.min.js"
        integrity="sha512-3gJwYpMe3QewGELv8k/BX9vcqhryRdzRMxVfq6ngyWXwo03GFEzjsUm8Q7RZcHPHksttq7/GFoxjCVUjkjvPdw=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <link href='https://unpkg.com/boxicons@2.1.4/css/boxicons.min.css' rel='stylesheet'>
    <link rel="stylesheet" href="{{ asset('css/style_admin.css') }}">
    <script src="https://unpkg.com/boxicons@2.1.4/dist/boxicons.js"></script>

</head>

<body>
    <main class="text-center">
        <form method="POST" action="{{ route('password.store') }}">
            @csrf
            <div class="row form-signin d-flex align-items-center justify-content-center">
                <div class="col-4">
                    <h1 class="mb-3 h3 fw-bold">Agratha Studio</h1>
                    <input type="hidden" name="token" value="{{ $token }}">
                    <div class="form-floating">
                        <input type="text" name="email" style="margin-bottom: 10px;" class="form-control" id="floatingInput" value="{{ @$email }}">
                        <label for="floatingInput">Email</label>
                    </div>
                    <div class="form-floating">
                        <input type="password" name="password" class="form-control" id="floatingPassword" placeholder="Password">
                        <label for="floatingPassword">Password</label>
                    </div>
                    <div class="form-floating">
                        <input type="password" name="confirm_password" class="form-control" id="floatingConfirmPassword" placeholder="Konfirmasi Password">
                        <label for="floatingConfirmPassword">Konfirmasi Password</label>
                    </div>
                    <button class="w-100 btn-primary" type="submit">Ganti Password</button>
                </div>
            </div>
        </form>
    </main>
    <p class="mt-5 text-center text-muted">© 2024 Agratha Studio. All rights reserved.</p>
</body>

</html>
