<!DOCTYPE html>
<html>
<head>
    <title>Struk Pembelian</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            width: 148mm; /* A5 width */
            margin: 0 auto;
            padding: 10mm;
            box-sizing: border-box;
        }
        .receipt {
            border: 1px solid #ddd;
            padding: 20px;
        }
        .header {
            text-align: center;
            margin-bottom: 20px;
        }
        .header h2 {
            margin: 0;
        }
        .details, .items, .totals {
            width: 100%;
            margin-bottom: 20px;
        }
        .details td, .items th, .items td, .totals td {
            padding: 5px;
            border-bottom: 1px solid #ddd;
        }
        .items th {
            text-align: left;
        }
        .items td {
            text-align: right;
        }
        .items td:first-child {
            text-align: left;
        }
        .totals td {
            font-weight: bold;
        }
        .totals td:last-child {
            text-align: right;
        }
    </style>
</head>
<body>
    <div class="receipt">
        <div class="header">
            <h2>{{ @$store->nama_toko }}</h2>
            <p style="font-size:10px;">{{ ucwords(strtolower($store->alamat_toko)) }}, {{ $store->kabupaten.' Kecamatan '.$store->kecamatan.' Provinsi ' .$store->provinsi}}<br>No. Telp: {{ $store->no_hp  }}</p>
        </div>
        <table class="details">
            <tr>
                <td>No. Transaksi:</td>
                <td>{{ $data->no_transaksi }}</td>
            </tr>
            <tr>
                <td>Pemesan:</td>
                <td>{{ $data->nama }}</td>
            </tr>
            <tr>
                <td>Tanggal Verifikasi:</td>
                <td>{{ $data->tgl_verif }}</td>
            </tr>
            <tr>
                <td>Verifikasi Oleh:</td>
                <td>{{ ucwords($data->admin->nama) }}</td>
            </tr>
            {{-- <tr>
                <td>Kasir:</td>
                <td>Nama Kasir</td>
            </tr> --}}
        </table>
        <table class="items">
            <thead>
                <tr>
                    <th>Detail</th>
                    <th>Harga</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        Paket : <b>{{ $data->refPaket->nama_paket }}</b><br>
                        Prioritas : <b>{{ $data->refPrioritas->nama_prioritas }}</b>
                    </td>
                    <td>Rp {{ rupiah($data->harga) }}</td>
                </tr>
                <tr>
                    <td>Anggaran Transport</td>
                    <td>Rp {{ rupiah($data->anggaran_transport) }}</td>
                </tr>
                <!-- Tambahkan barang lain di sini -->
            </tbody>
        </table>
        <table class="totals" style="margin-bottom: 30px;">
            <tr>
                <td>Metode Pembayaran:</td>
                <td>{{ $data->refBank->nama_bank }}</td>
            </tr>
        </table>
        
        <table class="totals">
            <tr>
                <td>Total:</td>
                <td>Rp {{ rupiah($data->harga + $data->anggaran_transport ) }}</td>
            </tr>
        </table>
        <div class="footer">
            <p>Terima kasih telah Mempercayakan Kepada Kami!</p>
        </div>
    </div>
</body>
</html>
