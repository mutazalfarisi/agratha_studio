@extends('admin.template.main')

@section('content')
    <!-- Begin Page Content -->
    <div class="container-fluid">
        <!-- Page Heading -->
        <!-- Content Row -->
        <div class="row">
            <div class="col-12">
                <div class="row">
                    <div class="card" style="width: 100%;">
                        <div class="p-5 card-body ">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <form action="{{ route('simpan_paket') }}" enctype="multipart/form-data" method="post">
                                @csrf
                                @if (@$id)
                                    <input type="hidden" name="id" value="{{ $id }}">
                                @endif
                                <div class="row">
                                    <div class="col-12">
                                        <div class="mb-3">
                                            <label for="paket" class="form-label">Nama Paket</label>
                                            <input type="text" class="form-control" name="paket" id="paket"
                                                value="{{ @$paket->nama_paket }}" placeholder="Masukkan Nama Paket">
                                        </div>
                                        <div class="mb-3">
                                            <label for="deskripsi" class="form-label">Deskripsi Paket</label>
                                            <textarea name="deskripsi" class="form-control" name="deskripsi" id="deskripsi" rows="3" placeholder="Masukkan Deskripsi Paket">{{ @$paket->deskripsi }}</textarea>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        @if (!empty(@$paket->foto))
                                            <a href="{{ asset('storage') . '/' . @$paket->foto }}" target="_blank"
                                                class="mb-3 shadow-sm d-none d-sm-inline-block btn btn-sm btn-success">Lihat
                                                Foto Sebelumnya</a>
                                        @endif
                                        <div class="mb-3">
                                            <label for="foto" class="form-label">Upload Foto</label>
                                            <input type="file" class="form-control" name="foto" id="foto" accept="image/*">
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="mb-3">
                                            <label for="harga" class="form-label">Harga</label>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-3">
                                                <span class="form-label">Sangat Mendesak : </span>
                                                <input type="text" class="form-control isNumber" name="sangat_mendesak" id="sangat_mendesak"
                                                    value="{{ @$paket->sangat_mendesak }}" placeholder="Rp">
                                            </div>
                                            <div class="col-lg-3">
                                                <span class="form-label">Mendesak : </span>
                                                <input type="text" class="form-control isNumber" name="mendesak" id="mendesak"
                                                    value="{{ @$paket->mendesak }}" placeholder="Rp">
                                            </div>
                                            <div class="col-lg-3">
                                                <span class="form-label">Biasa : </span>
                                                <input type="text" class="form-control isNumber" name="biasa" id="biasa"
                                                    value="{{ @$paket->biasa }}" placeholder="Rp">
                                            </div>
                                            <div class="col-lg-3">
                                                <span class="form-label">Tidak Mendesak : </span>
                                                <input type="text" class="form-control isNumber" name="tidak_mendesak" id="tidak_mendesak"
                                                    value="{{ @$paket->tidak_mendesak }}" placeholder="Rp">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="mt-3">
                                    <a href="{{ route('paket') }}" class="shadow-sm d-none d-sm-inline-block btn btn-sm btn-danger">Kembali</a>
                                    <button type="submit" class="shadow-sm d-none d-sm-inline-block btn btn-sm btn-primary">Simpan</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End of Main Content -->
@endsection
