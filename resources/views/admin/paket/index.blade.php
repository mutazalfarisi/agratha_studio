@extends('admin.template.main')

@section('content')
    <!-- Begin Page Content -->
    {{-- <div class="container-fluid">
        <!-- Page Heading -->
        <div class="row">
            <div class="col-sm-7">
                <div class="text-start text-sm-left">
                    <div class="card-body">
                        <h6 class="fw-bold"><span class="text-muted fw-light">{{ @$menu }} /</span> {{ @$title }}</h6>
                    </div>
              </div>
            </div>
        </div> --}}
    <!-- Content Row -->
    <div class="row">
        @if (session('success'))
            <div class="alert alert-primary">
                {{ session('success') }}
            </div>
        @endif

        @if (session('error'))
            <div class="alert alert-danger">
                {{ session('error') }}
            </div>
        @endif

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="mb-4 col-lg-12 order-0">
            <div class="card">
                <div class="card-body">
                    <div class="p-3 pt-0 mb-3 justify-content-end d-flex">
                        <a href="{{ route('tambah_paket') }}"
                            class="shadow-sm d-none d-sm-inline-block btn btn-sm btn-primary">
                            Tambah Paket
                        </a>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-fixed table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama</th>
                                    <th>Deskripsi</th>
                                    <th>Foto</th>
                                    <th>Harga</th>
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            {{-- <tbody>
                                    @php
                                        $no = 1;
                                    @endphp
                                    @foreach ($dt_user as $row)
                                        <tr>
                                            <td style="width:5%!important;">{{ $no++ }}</td>
                                            <td style="width:20%!important;">{{ $row->username }}</td>
                                            <td style="width:20%!important;">
                                                {{ $row->name }}<br>
                                                <b>Email : </b>{{ $row->email }}<br>
                                                <b>Nomor HP : </b>{{ $row->no_hp }}
                                            </td>
                                            <td style="width:30%!important;">
                                                {{ $row->detail_alamat }}
                                            </td>
                                            <td class="text-center" style="width:10%!important;">
                                                @if ($jml > 1)
                                                    <button
                                                        class="shadow-sm d-none d-sm-inline-block btn btn-sm btn-danger"
                                                        onclick="hapus({{ $row->id }})"><i class="text-white fas fa-trash fa-sm fa-fw"></i></button>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody> --}}
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <script>
        var dataPaket = "{{ route('dataPaket') }}"
        var HapusPaket = "{{ route('HapusPaket') }}"
        var token = '{{ csrf_token() }}';
    </script>
    <script src="{{ asset('admin/paket/index.js') }}"></script>


    <!-- End of Main Content -->
@endsection
