@extends('admin.template.main')

@section('content')
    <!-- Begin Page Content -->
    <!-- Content Row -->
    <div class="row">
        @if (session('success'))
            <div class="alert alert-primary">
                {{ session('success') }}
            </div>
        @endif

        @if (session('error'))
            <div class="alert alert-danger">
                {{ session('error') }}
            </div>
        @endif

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="mb-4 col-lg-12 order-0">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-fixed table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama</th>
                                    <th>Pesan</th>
                                    <th>Status</th>
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $no = 1;
                                @endphp
                                @foreach ($data as $row)
                                    <tr>
                                        <td style="width:5%!important;">{{ $no++ }}</td>
                                        <td style="width:20%!important;">
                                            Nama : <b>{{ $row->nama }}</b><br>
                                            Nomor HP : <b>{{ $row->no_hp }}</b><br>
                                            Email : <b>{{ $row->email }}</b><br>
                                        </td>
                                        <td style="width:30%!important;">
                                            {{ $row->pesan }}
                                        </td>
                                        <td class="text-center">
                                            @if ($row->status == '1')
                                                <span class="badge bg-success">Sudah Di Tinjaklanjuti</span>
                                            @else
                                                <span class="badge bg-danger">Belum Di tinjaklanjuti</span>
                                            @endif
                                        </td>
                                        <td class="text-center" style="width:10%!important;">
                                            <button class="btn btn-success btn-sm" onclick="ceklist({{ $row->id }})"><i
                                                    class="bx bx-check"></i></button>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <script>
        // var dataPaket = "{{ route('dataPaket') }}"
        // var HapusPaket = "{{ route('HapusPaket') }}"
        var token = '{{ csrf_token() }}';
    </script>
    <script>
        $(document).ready(function() {
            $("#dataTable").DataTable();
        })

        function ceklist(id) {
            Swal.fire({
                text: "Anda yakin ingin menceklist ini?",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                cancelButtonText: 'Batal',
                confirmButtonText: 'Ya, Checklist'
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: '/admin/ceklist_masukan',
                        type: 'post',
                        data: {
                            id: id,
                            _token: token
                        },
                        dataType: 'json',
                        success: function(res) {
                            if (res) {
                                Swal.fire(
                                    'Berhasil!',
                                    res.pesan,
                                    'success'
                                )
                                setTimeout(function() {
                                    location.reload(
                                        true
                                        ); // true mengabaikan cache dan memaksa memuat ulang dari server
                                }, 1000);
                            } else {
                                Swal.fire(
                                    'Gagal!',
                                    res.pesan,
                                    'error'
                                )
                            }
                        }
                    })
                }
            })
        }
    </script>
    {{-- <script src="{{ asset('admin/paket/index.js') }}"></script> --}}


    <!-- End of Main Content -->
@endsection
