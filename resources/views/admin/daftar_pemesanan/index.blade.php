@extends('admin.template.main')

@section('content')
    <!-- Begin Page Content -->
    {{-- <div class="container-fluid">
        <!-- Page Heading -->
        <div class="row">
            <div class="col-sm-7">
                <div class="text-start text-sm-left">
                    <div class="card-body">
                        <h6 class="fw-bold"><span class="text-muted fw-light">{{ @$menu }} /</span> {{ @$title }}</h6>
                    </div>
              </div>
            </div>
        </div> --}}
    <!-- Content Row -->
    <div class="row">
        @if (session('success'))
            <div class="alert alert-primary">
                {{ session('success') }}
            </div>
        @endif

        @if (session('error'))
            <div class="alert alert-danger">
                {{ session('error') }}
            </div>
        @endif

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="mb-4 col-lg-12 order-0">
            <div class="card">
                <div class="card-body">
                    <div class="pt-3 pb-3">
                        <div class="form-group">
                            <div class="row d-flex align-items-end">
                                <div class="col-4">
                                    <label for="">Tanggal Mulai</label>
                                    <input type="date" name="tanggal_mulai" id="tanggal_mulai" class="form-control">
                                </div>
                                <div class="col-4">
                                    <label for="">Tanggal Selesai</label>
                                    <input type="date" name="tanggal_selesai" id="tanggal_selesai" class="form-control">
                                </div>
                                <div class="col-2">
                                    <button class="btn btn-primary" id="terapkan">Terapkan</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-fixed table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Pemesan</th>
                                    <th>Detail Pemesanan</th>
                                    <th>Tanggal & Lokasi</th>
                                    <th>Pembayaran</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            {{-- <tbody>
                                @foreach ($pemesanan as $item)
                                    <tr style="font-size: 12px;">
                                        <td style="vertical-align: top;">{{ $loop->iteration }}</td>
                                        <td style="vertical-align: top;">
                                            Nama: <b>{{ $item->nama }}</b><br><br>
                                            Email: <b>{{ $item->email }}</b><br><br>
                                            Nomor HP: <b>{{ $item->no_hp }}</b>
                                        </td>
                                        <td style="vertical-align: top;">
                                            Paket: <b>{{ $item->paket }}</b><br><br>
                                            Prioritas: <b>{{ $item->refPrioritas->nama_prioritas }}</b><br><br>
                                            Harga: <b>Rp. {{ rupiah($item->harga) }}</b><br><br>
                                            Anggaran Transport: <b>Rp. {{ rupiah($item->anggaran_transport) }}</b><br><br>
                                            Tanggal Pemesanan: <b>{{ tanggal_indonesia($item->created_at) }}</b><br>
                                        </td>
                                        <td style="vertical-align: top;">
                                            Tanggal:
                                            <b>{{ formatrangetanggal($item->tanggal_mulai, $item->tanggal_selesai) }}</b><br><br>
                                            Lokasi: <b>{{ $item->kabupaten }}, {{ $item->alamat }}</b><br><br>
                                        </td>
                                        <td style="vertical-align: top;">
                                            Harga Total : <b>Rp.
                                                {{ rupiah($item->harga + $item->anggaran_transport) }}</b><br><br>
                                                    Pembayaran : <b>{{ @$item->refBank->nama_bank }}</b><br><br>
                                                    Nomor : <b>{{ @$item->refBank->no_rek }}</b><br><br>

                                            @if ($item->bukti_transfer != null)
                                                <a href="{{ url('storage/' . $item->bukti_transfer) }}" target="_blank"
                                                    class="btn btn-primary btn-sm">Bukti Pembayaran</a>
                                            @endif
                                        </td>
                                        <td style="vertical-align: top;">
                                            @if ($item->bukti_transfer != null && $item->is_admin == null)
                                                <span class="badge bg-success"><b>Upload Bukti Pembayaran</b></span>
                                            @elseif($item->bukti_transfer != null && $item->is_admin != 1)
                                                <span class="badge bg-danger"><b>Bukti Pembayaran Tidak Valid</b></span>
                                            @else
                                                <span class="badge bg-success"><b>Upload Bukti Pembayaran</b></span>
                                                @if ($item->is_verif == '1')
                                                    <span class="mt-2 badge bg-success">Verifikasi <br><br>
                                                        Oleh : {{ @$item->admin->nama }} <br><br>
                                                        Tanggal :
                                                        {{ $item->tgl_verif != null ? tanggal_indonesia(@$item->tgl_verif) : '' }}</span>
                                                @else
                                                    <span class="mt-2 badge bg-danger">Belum Verifikasi <br><br>
                                                        Oleh : {{ @$item->admin->nama }} <br><br>
                                                        Tanggal :
                                                        {{ $item->tgl_verif != null ? tanggal_indonesia(@$item->tgl_verif) : '' }}</span>
                                                @endif
                                            @endif
                                        </td>
                                        <td style="vertical-align: top;">
                                            @if ($item->is_verif == '1')
                                                <button class="btn btn-sm btn-danger btn-verifikasi"
                                                    onclick="verifikasi('{{ $item->id }}','{{ $item->bukti_transfer }}','batal_verifikasi')">Batalkan
                                                    Verifikasi</button>
                                            @else
                                                <button class="btn btn-sm btn-warning btn-verifikasi"
                                                    onclick="verifikasi('{{ $item->id }}','{{ $item->bukti_transfer }}','verifikasi')">Verifikasi</button>
                                            @endif

                                            <a href="https://wa.me/{{ $item->no_hp }}" target="_blank"
                                                class="mt-2 btn btn-sm btn-success"><i class='bx bxl-whatsapp'></i> Chat</a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody> --}}
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <script>
        var dataPemesanan = "{{ route('dataPemesanan') }}"
        var tipe = '{{ $li_sub_active }}';
        // var HapusPaket = "{{ route('HapusPaket') }}"
        var token = '{{ csrf_token() }}';
    </script>
    <script src="{{ asset('admin/daftar_pemesanan/index.js') }}"></script>
    <script>
        // $(document).ready(function() {
        //     $("#dataTable").DataTable()
        // })
    </script>
    {{-- <script src="{{ asset('admin/paket/index.js') }}"></script> --}}


    <!-- End of Main Content -->
@endsection
