@extends('admin.template.main')

@section('content')
    <!-- Begin Page Content -->
    <div class="container-fluid">
        <!-- Page Heading -->
        <div class="mb-4 d-sm-flex align-items-center justify-content-between">
            <h1 class="mb-0 text-gray-800 h3">{{ @$page_title }}</h1>
        </div>
        <!-- Content Row -->
        <div class="row">
            <div class="col-12">
                <div class="row">
                    <div class="card" style="width: 100%;">
                        <div class="card-body">
                            @if (session('success'))
                                <div class="alert alert-success">
                                    {{ session('success') }}
                                </div>
                            @endif

                            @if (session('error'))
                                <div class="alert alert-danger">
                                    {{ session('error') }}
                                </div>
                            @endif
                            <ul class="mb-3 nav nav-pills" id="pills-tab" role="tablist">
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link active" id="tab-persetujuan" data-toggle="pill"
                                        data-target="#pills-home" type="button" role="tab" aria-controls="pills-home"
                                        aria-selected="true">Persetujuan</button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link" id="tab-proses" data-toggle="pill" data-target="#pills-profile"
                                        type="button" role="tab" aria-controls="pills-profile"
                                        aria-selected="false">Proses</button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link" id="tab-selesai" data-toggle="pill"
                                        data-target="#pills-contact" type="button" role="tab"
                                        aria-controls="pills-contact" aria-selected="false">Selesai</button>
                                </li>
                            </ul>
                            <div class="tab-content" id="pills-tabContent">
                                <div class="tab-pane fade show active" id="pills-home" role="tabpanel"
                                    aria-labelledby="tab-persetujuan">
                                    <div class="table-responsive">
                                        <table class="table table-bordered" id="data-acc" width="100%" cellspacing="0">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Nomor Transaksi</th>
                                                    <th>Nama</th>
                                                    <th>Nomor HP</th>
                                                    <th>Alamat</th>
                                                    <th>Tanggal Transaksi</th>
                                                    <th>Detail Transaksi</th>
                                                    <th>Bukti Pembayaran</th>
                                                    <th>Status</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody style="font-size: 12px;">
                                                @php
                                                    $no = 1;
                                                @endphp
                                                @foreach ($dt_acc as $row)
                                                    <tr>
                                                        <td style="width:5%!important;">{{ $no++ }}</td>
                                                        <td style="width:10%!important;">{{ $row->no_transaksi }}</td>
                                                        <td style="width:10%!important;">{{ $row->nama }}</td>
                                                        <td style="width:10%!important;">{{ $row->no_hp }}</td>
                                                        <td style="width:15%!important;">
                                                            <font style="font-weight: bold">Provinsi : </font>
                                                            {{ $row->provinsi }}<br>
                                                            <font style="font-weight: bold">Kabupaten : </font>
                                                            {{ $row->kabupaten }}<br>
                                                            <font style="font-weight: bold">Alamat Lengkap : </font>
                                                            {{ $row->detail_alamat }}
                                                        </td>
                                                        <td style="width:10%!important;">{{ tanggal_indonesia($row->tgl_transaksi) }}</td>
                                                        <td
                                                            style="width:10%!important;text-align:center;vertical-align:middle;">
                                                            <button class="btn btn-primary btn-sm"
                                                                onclick="detailtransaksi('{{ $row->id }}')">Lihat
                                                                Detail</button>
                                                        </td>
                                                        <td
                                                            style="width:10%!important;text-align:center;vertical-align:middle;">
                                                            <a class="btn btn-sm btn-primary"
                                                                href="{{ asset('storage/' . $row->bukti_transfer) }}"
                                                                target="_blank">Lihat Gambar</a>
                                                        </td>
                                                        <td
                                                            style="width:10%!important;text-align:center;vertical-align:middle;">
                                                            <p class="badge badge-danger badge-sm">Menunggu Validasi</p>
                                                        </td>
                                                        <td class="text-center"
                                                            style="width:10%!important;vertical-align:middle;">
                                                            <button
                                                                class="shadow-sm d-none d-sm-inline-block btn btn-sm btn-success"
                                                                data-id="{{ $row->id }}" data-tipe="setuju"
                                                                onclick="validasi(this)"><i
                                                                    class="text-white fas fa-check fa-sm fa-fw"></i></button>
                                                            <button onclick="validasi(this)" data-id="{{ $row->id }}"
                                                                data-tipe="tolak"
                                                                class="shadow-sm d-none d-sm-inline-block btn btn-sm btn-danger"><i
                                                                    class="text-white fas fa-times fa-sm fa-fw"></i></button>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="tab-proses">
                                    <div class="table-responsive">
                                        <table class="table table-bordered" id="data-proses" width="100%" cellspacing="0">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Nomor Transaksi</th>
                                                    <th>Nama</th>
                                                    <th>Nomor HP</th>
                                                    <th>Alamat</th>
                                                    <th>Tanggal Transaksi</th>
                                                    <th>Detail Transaksi</th>
                                                    <th>Status</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody style="font-size: 12px;">
                                                @php
                                                    $no = 1;
                                                @endphp
                                                @foreach ($dt_dikirim as $row)
                                                    <tr>
                                                        <td style="width:5%!important;">{{ $no++ }}</td>
                                                        <td style="width:10%!important;">{{ $row->no_transaksi }}</td>
                                                        <td style="width:10%!important;">{{ $row->nama }}</td>
                                                        <td style="width:10%!important;">{{ $row->no_hp }}</td>
                                                        <td style="width:15%!important;">
                                                            <font style="font-weight: bold">Provinsi : </font>
                                                            {{ $row->provinsi }}<br>
                                                            <font style="font-weight: bold">Kabupaten : </font>
                                                            {{ $row->kabupaten }}<br>
                                                            <font style="font-weight: bold">Alamat Lengkap : </font>
                                                            {{ $row->detail_alamat }}
                                                        </td>
                                                        <td style="width:10%!important;">{{ tanggal_indonesia($row->tgl_transaksi) }}</td>
                                                        <td
                                                            style="width:10%!important;text-align:center;vertical-align:middle;">
                                                            <button class="btn btn-primary btn-sm"
                                                                onclick="detailtransaksi('{{ $row->id }}')">Lihat
                                                                Detail</button>
                                                        </td>
                                                        <td style="width:10%!important;text-align:center;vertical-align:middle;">
                                                            @if ($row->status_barang == '1')
                                                                <p class="badge badge-primary badge-sm">Sedang diKemas</p>
                                                            @elseif($row->status_barang == '2')
                                                                <p class="badge badge-info badge-sm">Sedang Perjalanan</p>
                                                            @endif
                                                        </td>
                                                        <td style="width:10%!important;text-align:center;vertical-align:middle;">
                                                            @if ($row->status_barang == '1')
                                                                <button class="btn btn-info btn-sm" onclick="ganti_status(2,'{{ $row->id }}')">Kirim Paket</button>
                                                            @elseif($row->status_barang == '2')
                                                                <button class="btn btn-info btn-sm" onclick="ganti_status(3,'{{ $row->id }}')">Selesaikan</button>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="pills-contact" role="tabpanel"
                                    aria-labelledby="tab-selesai">
                                    <div class="table-responsive">
                                        <table class="table table-bordered" id="data-selesai" width="100%"
                                            cellspacing="0">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Nomor Transaksi</th>
                                                    <th>Nama</th>
                                                    <th>Nomor HP</th>
                                                    <th>Alamat</th>
                                                    <th>Tanggal Transaksi</th>
                                                    <th>Detail Transaksi</th>
                                                    <th>Status</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody style="font-size: 12px;">
                                                @php
                                                    $no = 1;
                                                @endphp
                                                @foreach ($dt_selesai as $row)
                                                <tr>
                                                    <td style="width:5%!important;">{{ $no++ }}</td>
                                                    <td style="width:10%!important;">{{ $row->no_transaksi }}</td>
                                                    <td style="width:10%!important;">{{ $row->nama }}</td>
                                                    <td style="width:10%!important;">{{ $row->no_hp }}</td>
                                                    <td style="width:15%!important;">
                                                        <font style="font-weight: bold">Provinsi : </font>
                                                        {{ $row->provinsi }}<br>
                                                        <font style="font-weight: bold">Kabupaten : </font>
                                                        {{ $row->kabupaten }}<br>
                                                        <font style="font-weight: bold">Alamat Lengkap : </font>
                                                        {{ $row->detail_alamat }}
                                                    </td>
                                                    <td style="width:10%!important;">{{ tanggal_indonesia($row->tgl_transaksi) }}</td>
                                                    <td
                                                        style="width:10%!important;text-align:center;vertical-align:middle;">
                                                        <button class="btn btn-primary btn-sm"
                                                            onclick="detailtransaksi('{{ $row->id }}')">Lihat
                                                            Detail</button>
                                                    </td>
                                                    <td style="width:10%!important;text-align:center;vertical-align:middle;">
                                                        <p class="badge badge-success badge-sm">Selesai</p>
                                                    </td>
                                                    <td style="width:10%!important;text-align:center;vertical-align:middle;">
                                                        <a href="{{ url('/cetak/transaksi') . '/' . $row->id }}" target="_blank" class="shadow-sm d-none d-sm-inline-block btn btn-sm btn-warning"><i
                                                            class="text-white fas fa-print fa-sm fa-fw"></i></a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- Modal Detail Transaksi --}}
    <div class="modal fade" id="modal-transaksi" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Detail Transaksi</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="table-detail-transaksi"></div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Tutup</button>
                </div>
            </div>
        </div>
    </div>

    <script>
        function detailtransaksi(id) {
            $("#modal-transaksi").modal('show');
            $("#table-detail-transaksi").html(
                '<div style="text-align:center;"><img src="{{ asset('loading.gif') }}"></div>');
            $.ajax({
                url: `{{ route('detail_transaksi') }}`,
                type: 'get',
                dataType: 'json',
                data: {
                    id: id
                },
                success: function(res) {
                    $("#table-detail-transaksi").html(res);
                }
            })
        }
    </script>
    <!-- End of Main Content -->
@endsection
