@extends('admin.template.main')

@section('content')
    <!-- Begin Page Content -->
    {{-- <div class="container-fluid">
        <!-- Page Heading -->
        <div class="row">
            <div class="col-sm-7">
                <div class="text-start text-sm-left">
                    <div class="card-body">
                        <h6 class="fw-bold"><span class="text-muted fw-light">{{ @$menu }} /</span> {{ @$title }}</h6>
                    </div>
              </div>
            </div>
        </div> --}}
    <!-- Content Row -->
    <div class="row">
        @if (session('success'))
            <div class="alert alert-primary">
                {{ session('success') }}
            </div>
        @endif

        @if (session('error'))
            <div class="alert alert-danger">
                {{ session('error') }}
            </div>
        @endif

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="mb-4 col-lg-12 order-0">
            <div class="card">
                <div class="card-body">
                    <div class="pt-3 pb-3">
                        <div class="form-group">
                            <div class="row d-flex align-items-end">
                                <div class="col-4">
                                    <label for="">Tanggal Mulai</label>
                                    <input type="date" name="tanggal_mulai" id="tanggal_mulai" class="form-control">
                                </div>
                                <div class="col-4">
                                    <label for="">Tanggal Selesai</label>
                                    <input type="date" name="tanggal_selesai" id="tanggal_selesai" class="form-control">
                                </div>
                                <div class="col-2">
                                    <button class="btn btn-primary" id="terapkan">Terapkan</button>
                                </div>
                                <div class="col-2">
                                    <a href="{{ url('admin/cetak_rekap') }}" target="_blank" class="btn btn-success" id="cetak_rekap">Cetak Laporan</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-fixed table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Pemesan</th>
                                    <th>Detail Pemesanan</th>
                                    <th>Tanggal & Lokasi</th>
                                    <th>Pembayaran</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <script>
        var dataLaporan = "{{ route('dataLaporan') }}"
        // var HapusPaket = "{{ route('HapusPaket') }}"
        var token = '{{ csrf_token() }}';
    </script>
    <script src="{{ asset('admin/laporan_pemesanan/index.js') }}"></script>
    <script>
        // $(document).ready(function() {
        //     $("#dataTable").DataTable()
        // })
    </script>
    {{-- <script src="{{ asset('admin/paket/index.js') }}"></script> --}}


    <!-- End of Main Content -->
@endsection
