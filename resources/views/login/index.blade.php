<!DOCTYPE html>
<html lang="en">

<head>
    <title>{{ @$title ?? 'Agratha Studio.' }}</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="images/icons/favicon.ico" />
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_login') }}/vendor/bootstrap/css/bootstrap.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css"
        href="{{ asset('assets_login') }}/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css"
        href="{{ asset('assets_login') }}/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_login') }}/vendor/animate/animate.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_login') }}/vendor/css-hamburgers/hamburgers.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_login') }}/vendor/animsition/css/animsition.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_login') }}/vendor/select2/select2.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css"
        href="{{ asset('assets_login') }}/vendor/daterangepicker/daterangepicker.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_login') }}/css/util.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_login') }}/css/main.css">
    <!--===============================================================================================-->
</head>

<body style="background-color: #666666;">
    <div class="limiter">
        <div class="container-login100">
            <div class="wrap-login100">
                <form action="{{ route('login_user') }}" method="post" class="login100-form validate-form">
                    @csrf
                    <span class="login100-form-title p-b-43">
                        Login to continue
                    </span>

                    @if (session('success'))
                        <div class="m-3 alert alert-success">
                            <span style="font-size: 12px;">{{ session('success') }}</span>
                        </div>
                    @endif

                    @if (session('error'))
                        <div class="m-3 alert alert-danger">
                            <span style="font-size: 12px;">{{ session('error') }}</span>
                        </div>
                    @endif
                    @if ($errors->any())
                        <div class="m-3 alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li style="font-size: 12px;">{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <div class="wrap-input100 validate-input" data-validate = "Valid email is required: ex@abc.xyz">
                        <input class="input100" type="text" name="email">
                        <span class="focus-input100"></span>
                        <span class="label-input100">Email</span>
                    </div>


                    <div class="wrap-input100 validate-input" data-validate="Password is required">
                        <input class="input100" type="password" name="password">
                        <span class="focus-input100"></span>
                        <span class="label-input100">Password</span>
                    </div>

                    <div class="w-full flex-sb-m p-t-3 p-b-32">
                        {{-- <div class="contact100-form-checkbox">
                            <input class="input-checkbox100" id="ckb1" type="checkbox" name="remember-me">
                            <label class="label-checkbox100" for="ckb1">
                                Remember me
                            </label>
                        </div> --}}

                        <div>
                            <a href="#" class="forgot">
                                Forgot Password?
                            </a>
                        </div>
                    </div>


                    <div class="container-login100-form-btn">
                        <button type="submit" class="login100-form-btn">
                            Login
                        </button>
                    </div>

                    <div class="text-center" style="margin-top: 30px;font-size:14px;">
                        <div>Belum Punya Akun?</div>
                        <div><a href="{{ route('register') }}" style="text-decoration: none;">Daftar Sekarang</a>
                        </div>
                    </div>

                    {{-- <div class="container-login100-form-btn">
						<a href="{{ route('register') }}" class="login100-form-btn" style="color: white;">
							Registrasi
						</a>
					</div> --}}
                    {{-- <div class="text-center p-t-46 p-b-20">
						<span class="txt2">
							or sign up using
						</span>
					</div> --}}

                    {{-- <div class="login100-form-social flex-c-m">
						<a href="#" class="login100-form-social-item flex-c-m bg1 m-r-5">
							<i class="fa fa-facebook-f" aria-hidden="true"></i>
						</a>

						<a href="#" class="login100-form-social-item flex-c-m bg2 m-r-5">
							<i class="fa fa-twitter" aria-hidden="true"></i>
						</a>
					</div> --}}
                </form>

                <div class="login100-more"
                    style="background-image: url('{{ asset('assets_login') }}/images/login.jpg');">
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modallupapassword" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form action="{{ url('/forgot-password') }}" method="post">
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Lupa Password</h5>
                    </div>
                    <div class="modal-body">
                        <div class="form_lupa">
                            <p style="font-size: 13px;">Silahkan masukkan email anda, sistem akan
                                mengirim link ke email
                                anda untuk mereset password anda.</p>
                            <div class="wrap-input100">
                                <input class="input100" type="text" name="email">
                                <span class="focus-input100"></span>
                                <span class="label-input100">Email</span>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn-cstm-red" style="color: white;" data-dismiss="modal"
                            id="modal_tutup">Tutup</button>
                        <button type="submit" class="btn-cstm-blue">Kirim</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!--===============================================================================================-->
    <script src="{{ asset('assets_login') }}/vendor/jquery/jquery-3.2.1.min.js"></script>
    <!--===============================================================================================-->
    <script src="{{ asset('assets_login') }}/vendor/animsition/js/animsition.min.js"></script>
    <!--===============================================================================================-->
    <script src="{{ asset('assets_login') }}/vendor/bootstrap/js/popper.js"></script>
    <script src="{{ asset('assets_login') }}/vendor/bootstrap/js/bootstrap.min.js"></script>
    <!--===============================================================================================-->
    <script src="{{ asset('assets_login') }}/vendor/select2/select2.min.js"></script>
    <!--===============================================================================================-->
    <script src="{{ asset('assets_login') }}/vendor/daterangepicker/moment.min.js"></script>
    <script src="{{ asset('assets_login') }}/vendor/daterangepicker/daterangepicker.js"></script>
    <!--===============================================================================================-->
    <script src="{{ asset('assets_login') }}/vendor/countdowntime/countdowntime.js"></script>
    <!--===============================================================================================-->
    <script src="{{ asset('assets_login') }}/js/main.js"></script>

    <script>
        $(document).ready(function() {
            $('.forgot').click(function() {
                console.log("MASUK");
                $("#modallupapassword").modal('show')
            })
        })
    </script>

</body>

</html>
