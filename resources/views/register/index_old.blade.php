<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.0/jquery.min.js"
        integrity="sha512-3gJwYpMe3QewGELv8k/BX9vcqhryRdzRMxVfq6ngyWXwo03GFEzjsUm8Q7RZcHPHksttq7/GFoxjCVUjkjvPdw=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <link href='https://unpkg.com/boxicons@2.1.4/css/boxicons.min.css' rel='stylesheet'>
    <title>Marketplace</title>
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <script src="{{ asset('js/script.js') }}"></script>
    @include('login.js')
    <link rel="stylesheet" href="{{ asset('owlcarophpusel/dist/assets/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('owlcarousel/dist/assets/owl.theme.default.min.css') }}">
</head>

<body style="overflow:hidden;background-color:rgb(40, 47, 61);">
    {{-- <img src="{{ asset('images/wave.png') }}" id="wave_register" --}}
    style="width:100%;position: relative;top:470px;z-index:-1;">
    <div class="p-4" style="position: absolute;top:0px;">
        <div class="shadow-sm card bg-light" style="height:90vh;box-sizing: border-box;">
            <div class="row">
                <div class="d-none d-lg-block col-12 col-lg-4 col-xl-4">
                    <div class="title-market d-flex justify-content-center">
                        <img src="{{ asset('images/model3.jpg') }}" class="rounded shadow-sm"
                            style="width: 100%;object-fit:cover;height:90vh;">
                    </div>
                </div>
                <div class="p-3 col-12 col-lg-8 col-xl-8">
                    <p>
                    <h4 class="text-center fw-bold">Sign Up</h4>
                    </p>
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <div class="p-4 mt-3 form-login" style="max-height:70vh;overflow-y:auto">
                        <form action="{{ url('register') }}" method="post">
                            @csrf
                            <div class="row d-flex">
                                <div class="col-xl-6">
                                    <div class="form-input">
                                        <input type="text" name="name" placeholder="" value="{{ old('name') }}"
                                            required>
                                        <label for="name">Name<font style="color: red;">*</font></label>
                                    </div>
                                </div>
                                <div class="col-xl-6">
                                    <div class="form-input">
                                        <input type="text" name="username" placeholder=""
                                            value="{{ old('username') }}" required>
                                        <label for="username">Username<font style="color: red;">*</font></label>
                                    </div>
                                </div>
                                <div class="col-xl-6">
                                    <div class="form-input">
                                        <input type="number" name="no_hp" placeholder=""
                                            value="{{ old('no_hp') }}"required>
                                        <label for="no_hp">Number Phone<font style="color: red;">*</font></label>
                                    </div>
                                </div>
                                <div class="col-xl-6">
                                    <div class="form-input">
                                        <input type="text" name="email" placeholder=""
                                            value="{{ old('email') }}"required>
                                        <label for="email">Email<font style="color: red;">*</font></label>
                                    </div>
                                </div>
                                <div class="col-xl-6">
                                    <div class="form-input">
                                        <input type="password" name="password" placeholder="" required>
                                        <label for="password">Password<font style="color: red;">*</font></label>
                                    </div>
                                </div>
                                <div class="col-xl-6">
                                    <div class="form-input">
                                        <input type="password" name="confirm_password" placeholder="" required>
                                        <label for="confirm_password">Confirm Password<font style="color: red;">*</font>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-input">
                                <button class="p-2 border shadow-sm btn btn-primary rounded-pill w-100"
                                    style="font-size: 14px;margin-top:30px;">Daftar</button>
                            </div>
                        </form>
                        {{-- <p style="font-size: 14px;text-align:center;margin-top:1rem;">atau</p>
                        <div class="form-input">
                            <button class="p-2 border shadow-sm btn btn-light rounded-pill w-100"
                                style="font-size: 14px;"><img src="{{ asset('images/icon/google.svg') }}"
                                    style="height: 30px;">Daftar dengan
                                Google</button>
                        </div> --}}

                        <div class="text-center" style="margin-top: 30px;font-size:14px;">
                            <div>Sudah Punya Akun?</div>
                            <div><a href="{{ url('login') }}" style="text-decoration: none;">Masuk Sekarang</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"
        integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js"
        integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous">
    </script>
    {{-- <script src="jquery.min.js"></script> --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.0/jquery.min.js"
        integrity="sha512-3gJwYpMe3QewGELv8k/BX9vcqhryRdzRMxVfq6ngyWXwo03GFEzjsUm8Q7RZcHPHksttq7/GFoxjCVUjkjvPdw=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="{{ asset('owlcarousel/dist/owl.carousel.min.js') }}"></script>
</body>

</html>
