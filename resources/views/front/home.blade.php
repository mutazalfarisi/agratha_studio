@extends('front.layout.default')

@section('content')
    <!-- Start Hero Section -->
			<div class="hero">
				<div class="container">
					<div class="row justify-content-between">
						<div class="col-lg-5">
							<div class="intro-excerpt">
								<h1>Menghidupkan Momen Melalui Fotografi</h1>
								<p class="mb-4">Agratha Studio, di mana setiap foto menceritakan kisah yang hidup dan memukau.</p>
								<p><a href="" class="btn btn-secondary me-2">Pesan Sekarang</a><a href="#" class="btn btn-white-outline">Jelajahi</a></p>
							</div>
						</div>
						<div class="col-lg-7">
							<div class="hero-img-wrap">
								{{-- <img src="{{ asset('') }}images/couch.png" class="img-fluid"> --}}
							</div>
						</div>
					</div>
				</div>
			</div>
		<!-- End Hero Section -->

		<!-- Start Product Section -->
		<div class="product-section">
			<div class="container">
				<div class="row">
					<!-- Start Column 1 -->
					<div class="mb-5 col-md-12 col-lg-3 mb-lg-0">
						<h2 class="mb-4 section-title">Telusuri Beragam Paket Kami.</h2>
						<p class="mb-4">Telusuri beragam paket layanan kami yang dirancang untuk memenuhi segala kebutuhan fotografi Anda dengan sempurna. </p>
						<p><a href="{{ url('/paket') }}" class="btn">Jelajahi</a></p>
					</div> 
					<!-- End Column 1 -->
					<!-- Start Column 2 -->
					@foreach ($dt_paket as $item)
						<div class="mb-5 col-12 col-md-4 col-lg-3 mb-md-0">
							<a class="product-item" href="{{ url('/detail_paket/'. $item->id) }}">
								<img src="{{ asset("/storage/". $item->foto) }}" class="img-fluid product-thumbnail">
								<span class="text-secondary">Paket</span>
								<h3 class="product-title">{{ $item->nama_paket }}</h3>
								{{-- <strong class="product-price">Rp. {{ $item-> }}</strong> --}}
								<div class="d-flex justify-content-center icon-cross">
									<button class="btn btn-sm btn-dark">Selengkapnya</button>
								</div>
							</a>
						</div> 
					@endforeach
					<!-- End Column 2 -->
					<!-- Start Column 2 -->
					{{-- <div class="mb-5 col-12 col-md-4 col-lg-3 mb-md-0">
						<a class="product-item" href="cart.html">
							<img src="{{ asset('') }}images/company-profil.jpg" class="img-fluid product-thumbnail">
							<h3 class="product-title">Company Profil</h3>
							<strong class="product-price">$50.00</strong>

							<div class="d-flex justify-content-center icon-cross">
								<button class="btn btn-sm btn-dark">Selengkapnya</button>
							</div>
						</a>
					</div>  --}}
					<!-- End Column 2 -->

					<!-- Start Column 3 -->
					{{-- <div class="mb-5 col-12 col-md-4 col-lg-3 mb-md-0">
						<a class="product-item" href="cart.html">
							<img src="{{ asset('') }}images/product-2.png" class="img-fluid product-thumbnail">
							<h3 class="product-title">Kruzo Aero Chair</h3>
							<strong class="product-price">$78.00</strong>

							<div class="d-flex justify-content-center icon-cross">
								<button class="btn btn-sm btn-dark">Selengkapnya</button>
							</div>
						</a>
					</div> --}}
					<!-- End Column 3 -->

					<!-- Start Column 4 -->
					{{-- <div class="mb-5 col-12 col-md-4 col-lg-3 mb-md-0">
						<a class="product-item" href="cart.html">
							<img src="{{ asset('') }}images/product-3.png" class="img-fluid product-thumbnail">
							<h3 class="product-title">Ergonomic Chair</h3>
							<strong class="product-price">$43.00</strong>

							<span class="icon-cross">
								<img src="{{ asset('') }}images/cross.svg" class="img-fluid">
							</span>
						</a>
					</div> --}}
					<!-- End Column 4 -->

				</div>
			</div>
		</div>
		<!-- End Product Section -->

		<!-- Start We Help Section -->
		<div class="we-help-section">
			<div class="container">
				<div class="row justify-content-between">
					<div class="mb-5 col-lg-7 mb-lg-0">
						<div class="imgs-grid">
							<div class="grid grid-1"><img src="{{ asset('') }}images/jalan.jpg" alt="Untree.co"></div>
							<div class="grid grid-2"><img src="{{ asset('') }}images/bunga.jpg" alt="Untree.co"></div>
							<div class="grid grid-3"><img src="{{ asset('') }}images/ketik.jpg" alt="Untree.co"></div>
						</div>
					</div>
					<div class="col-lg-5 ps-lg-5">
						<h2 class="mb-4 section-title">Telusuri Galeri Kami.</h2>
						<p>Jelajahi dunia melalui kumpulan visual kami yang memukau di Galeri kami. Dari potret alam yang menakjubkan hingga momen-momen penuh emosi, setiap gambar menceritakan cerita yang unik. Saksikan keindahan yang ditangkap melalui lensa kami, dan biarkan diri Anda terinspirasi oleh keajaiban yang ada di sekitar kita.</p>

						{{-- <ul class="my-4 list-unstyled custom-list">
							<li>Donec vitae odio quis nisl dapibus malesuada</li>
							<li>Donec vitae odio quis nisl dapibus malesuada</li>
							<li>Donec vitae odio quis nisl dapibus malesuada</li>
							<li>Donec vitae odio quis nisl dapibus malesuada</li>
						</ul> --}}
						<p><a herf="#" class="btn">Jelajahi</a></p>
					</div>
				</div>
			</div>
		</div>
		<!-- End We Help Section -->

		<!-- Start Why Choose Us Section -->
		<div class="why-choose-section">
			<div class="container">
				<div class="row justify-content-between">
					<div class="col-lg-6">
						<h2 class="section-title">Kenapa Memilih Kami</h2>
						{{-- <p>Donec vitae odio quis nisl dapibus malesuada. Nullam ac aliquet velit. Aliquam vulputate velit imperdiet dolor tempor tristique.</p> --}}

						<div class="my-5 row">
							<div class="col-6 col-md-6">
								<div class="feature">
									<div class="icon">
										<img src="{{ asset('') }}images/truck.svg" alt="Image" class="imf-fluid">
									</div>
									<h3>Harga &amp; terjangkau</h3>
									<p>Diskusikan kebutuhan anda, kami akan berikan penawaran terbaik untuk anda.</p>
								</div>
							</div>

							<div class="col-6 col-md-6">
								<div class="feature">
									<div class="icon">
										<img src="{{ asset('') }}images/bag.svg" alt="Image" class="imf-fluid">
									</div>
									<h3>Terpecaya</h3>
									<p>Kami telah membantu berbagai client, lokal ataupun internasional.</p>
								</div>
							</div>

							<div class="col-6 col-md-6">
								<div class="feature">
									<div class="icon">
										<img src="{{ asset('') }}images/support.svg" alt="Image" class="imf-fluid">
									</div>
									<h3>Foto di Lokasi Anda</h3>
									<p>Anda bebas menentukan lokasi pemotretan.</p>
								</div>
							</div>

							<div class="col-6 col-md-6">
								<div class="feature">
									<div class="icon">
										<img src="{{ asset('') }}images/return.svg" alt="Image" class="imf-fluid">
									</div>
									<h3>Team yang Profesional</h3>
									<p>Lebih dari 500++ klien terpuaskan & hasil terbukti Berkualitas.</p>
								</div>
							</div>

						</div>
					</div>

					<div class="col-lg-5">
						<div class="img-wrap">
							<img src="{{ asset('') }}images/why-choose-us-img.jpg" alt="Image" class="img-fluid">
						</div>
					</div>

				</div>
			</div>
		</div>
		<!-- End Why Choose Us Section -->

		<!-- Start Popular Product -->
		{{-- <div class="popular-product">
			<div class="container">
				<div class="row">
					<div class="mb-4 col-12 col-md-6 col-lg-4 mb-lg-0">
						<div class="product-item-sm d-flex">
							<div class="thumbnail">
								<img src="{{ asset('') }}images/product-1.png" alt="Image" class="img-fluid">
							</div>
							<div class="pt-3">
								<h3>Nordic Chair</h3>
								<p>Donec facilisis quam ut purus rutrum lobortis. Donec vitae odio </p>
								<p><a href="#">Read More</a></p>
							</div>
						</div>
					</div>
					<div class="mb-4 col-12 col-md-6 col-lg-4 mb-lg-0">
						<div class="product-item-sm d-flex">
							<div class="thumbnail">
								<img src="{{ asset('') }}images/product-2.png" alt="Image" class="img-fluid">
							</div>
							<div class="pt-3">
								<h3>Kruzo Aero Chair</h3>
								<p>Donec facilisis quam ut purus rutrum lobortis. Donec vitae odio </p>
								<p><a href="#">Read More</a></p>
							</div>
						</div>
					</div>
					<div class="mb-4 col-12 col-md-6 col-lg-4 mb-lg-0">
						<div class="product-item-sm d-flex">
							<div class="thumbnail">
								<img src="{{ asset('') }}images/product-3.png" alt="Image" class="img-fluid">
							</div>
							<div class="pt-3">
								<h3>Ergonomic Chair</h3>
								<p>Donec facilisis quam ut purus rutrum lobortis. Donec vitae odio </p>
								<p><a href="#">Read More</a></p>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div> --}}
		<!-- End Popular Product -->

		<!-- Start Testimonial Slider -->
		<div class="testimonial-section">
			<div class="container">
				<div class="row">
					<div class="mx-auto text-center col-lg-7">
						<h2 class="section-title">Testimonials</h2>
					</div>
				</div>

				<div class="row justify-content-center">
					<div class="col-lg-12">
						<div class="text-center testimonial-slider-wrap">

							<div id="testimonial-nav">
								<span class="prev" data-controls="prev"><span class="fa fa-chevron-left"></span></span>
								<span class="next" data-controls="next"><span class="fa fa-chevron-right"></span></span>
							</div>

							<div class="testimonial-slider">
								
								<div class="item">
									<div class="row justify-content-center">
										<div class="mx-auto col-lg-8">

											<div class="text-center testimonial-block">
												<blockquote class="mb-5">
													<p>&ldquo;Donec facilisis quam ut purus rutrum lobortis. Donec vitae odio quis nisl dapibus malesuada. Nullam ac aliquet velit. Aliquam vulputate velit imperdiet dolor tempor tristique. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Integer convallis volutpat dui quis scelerisque.&rdquo;</p>
												</blockquote>

												<div class="author-info">
													<div class="author-pic">
														<img src="{{ asset('') }}images/person-1.png" alt="Maria Jones" class="img-fluid">
													</div>
													<h3 class="font-weight-bold">Maria Jones</h3>
													<span class="mb-3 position d-block">CEO, Co-Founder, XYZ Inc.</span>
												</div>
											</div>

										</div>
									</div>
								</div> 
								<!-- END item -->

								<div class="item">
									<div class="row justify-content-center">
										<div class="mx-auto col-lg-8">

											<div class="text-center testimonial-block">
												<blockquote class="mb-5">
													<p>&ldquo;Donec facilisis quam ut purus rutrum lobortis. Donec vitae odio quis nisl dapibus malesuada. Nullam ac aliquet velit. Aliquam vulputate velit imperdiet dolor tempor tristique. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Integer convallis volutpat dui quis scelerisque.&rdquo;</p>
												</blockquote>

												<div class="author-info">
													<div class="author-pic">
														<img src="{{ asset('') }}images/person-1.png" alt="Maria Jones" class="img-fluid">
													</div>
													<h3 class="font-weight-bold">Maria Jones</h3>
													<span class="mb-3 position d-block">CEO, Co-Founder, XYZ Inc.</span>
												</div>
											</div>

										</div>
									</div>
								</div> 
								<!-- END item -->

								<div class="item">
									<div class="row justify-content-center">
										<div class="mx-auto col-lg-8">

											<div class="text-center testimonial-block">
												<blockquote class="mb-5">
													<p>&ldquo;Donec facilisis quam ut purus rutrum lobortis. Donec vitae odio quis nisl dapibus malesuada. Nullam ac aliquet velit. Aliquam vulputate velit imperdiet dolor tempor tristique. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Integer convallis volutpat dui quis scelerisque.&rdquo;</p>
												</blockquote>

												<div class="author-info">
													<div class="author-pic">
														<img src="{{ asset('') }}images/person-1.png" alt="Maria Jones" class="img-fluid">
													</div>
													<h3 class="font-weight-bold">Maria Jones</h3>
													<span class="mb-3 position d-block">CEO, Co-Founder, XYZ Inc.</span>
												</div>
											</div>

										</div>
									</div>
								</div> 
								<!-- END item -->

							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- End Testimonial Slider -->

		<!-- Start Blog Section -->
		{{-- <div class="blog-section">
			<div class="container">
				<div class="mb-5 row">
					<div class="col-md-6">
						<h2 class="section-title">Recent Blog</h2>
					</div>
					<div class="col-md-6 text-start text-md-end">
						<a href="#" class="more">View All Posts</a>
					</div>
				</div>

				<div class="row">

					<div class="mb-4 col-12 col-sm-6 col-md-4 mb-md-0">
						<div class="post-entry">
							<a href="#" class="post-thumbnail"><img src="{{ asset('') }}images/post-1.jpg" alt="Image" class="img-fluid"></a>
							<div class="post-content-entry">
								<h3><a href="#">First Time Home Owner Ideas</a></h3>
								<div class="meta">
									<span>by <a href="#">Kristin Watson</a></span> <span>on <a href="#">Dec 19, 2021</a></span>
								</div>
							</div>
						</div>
					</div>

					<div class="mb-4 col-12 col-sm-6 col-md-4 mb-md-0">
						<div class="post-entry">
							<a href="#" class="post-thumbnail"><img src="{{ asset('') }}images/post-2.jpg" alt="Image" class="img-fluid"></a>
							<div class="post-content-entry">
								<h3><a href="#">How To Keep Your Furniture Clean</a></h3>
								<div class="meta">
									<span>by <a href="#">Robert Fox</a></span> <span>on <a href="#">Dec 15, 2021</a></span>
								</div>
							</div>
						</div>
					</div>

					<div class="mb-4 col-12 col-sm-6 col-md-4 mb-md-0">
						<div class="post-entry">
							<a href="#" class="post-thumbnail"><img src="{{ asset('') }}images/post-3.jpg" alt="Image" class="img-fluid"></a>
							<div class="post-content-entry">
								<h3><a href="#">Small Space Furniture Apartment Ideas</a></h3>
								<div class="meta">
									<span>by <a href="#">Kristin Watson</a></span> <span>on <a href="#">Dec 12, 2021</a></span>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div> --}}
		<!-- End Blog Section -->	

@endsection
