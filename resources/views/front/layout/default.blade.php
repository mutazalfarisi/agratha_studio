{{-- <!doctype html> --}}
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="Untree.co">
    {{-- <link rel="shortcut icon" href="favicon.png"> --}}

    <meta name="description" content="" />
    <meta name="keywords" content="bootstrap, bootstrap4" />
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Bootstrap CSS -->
    <link href="{{ asset('css') }}/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css" rel="stylesheet">
    <link href="{{ asset('css') }}/tiny-slider.css" rel="stylesheet">
    <link href="{{ asset('css') }}/style.css" rel="stylesheet">
    <script src="{{ asset('assets_admin') }}/vendor/libs/jquery/jquery.js"></script>
    <script src="{{ asset('assets_admin') }}/select2/select2.min.js"></script>
    <link rel="stylesheet" href="{{ asset('assets_admin') }}/datatable/datatable.min.css" />

    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <title>Agratha Studio </title>
</head>

<body>

    <!-- Start Header/Navigation -->
    <nav class="custom-navbar navbar navbar-expand-md navbar-dark bg-dark" arial-label="Furni navigation bar">

        <div class="container">
            <a class="navbar-brand" href="index.html">Agratha<span>.</span></a>

            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarsFurni"
                aria-controls="navbarsFurni" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarsFurni">
                <ul class="mb-2 custom-navbar-nav navbar-nav ms-auto mb-md-0">
                    <li class="{{ @$active == 'beranda' ? 'nav-item active' : '' }}">
                        <a class="nav-link" href="{{ route('home') }}">Beranda</a>
                    </li>
                    <li class="{{ @$active == 'galeri' ? 'nav-item active' : '' }}"><a class="nav-link"
                            href="{{ url('/galeri') }}">Galeri</a></li>
                    <li class="{{ @$active == 'paket' ? 'nav-item active' : '' }}"><a class="nav-link"
                            href="{{ url('/paket') }}">Paket</a></li>
                    <li class="{{ @$active == 'kontak' ? 'nav-item active' : '' }}"><a class="nav-link"
                            href="{{ url('/kontak_kami') }}">Kontak Kami</a></li>
                    <li class="{{ @$active == 'tentang' ? 'nav-item active' : '' }}"><a class="nav-link"
                            href="{{ url('/tentang_kami') }}">Tentang Kami</a></li>
                </ul>

                <ul class="mb-2 custom-navbar-cta navbar-nav mb-md-0 ms-5">
                    @if (!empty(Auth::user()->username))
                        <li>
                            <div class="dropdown">
                                <button class="btn btn-secondary btn-sm dropdown-toggle" type="button"
                                    id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                    <b><i class="me-2 fa-regular fa-user"></i>{{ ucwords(Auth::user()->nama) }}</b>
                                </button>
                                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                    <li>
                                        <a class="dropdown-item"><i class="me-2 fa-regular fa-user"></i>
                                            {{ ucwords(Auth::user()->nama) }}</a>
                                    </li>
                                    <li><a class="dropdown-item" href="{{ route('riwayat_pemesanan') }}">Riwayat
                                            Pemesanan</a></li>
                                    <li><a class="dropdown-item" style="cursor:pointer" id="logout">Logout</a></li>
                                </ul>
                            </div>
                        </li>
                    @else
                        <li><a class="nav-link" href="{{ route('login') }}"><img
                                    src="{{ asset('') }}images/user.svg"></a></li>
                    @endif
                </ul>
            </div>
        </div>

    </nav>
    <!-- End Header/Navigation -->
    @yield('content')

    <!-- Start Footer Section -->
    <footer class="footer-section">
        <div class="container relative">

            <div class="sofa-img">
                {{-- <img src="{{ asset('') }}images/sofa.png" alt="Image" class="img-fluid"> --}}
                <img src="{{ asset('') }}images/camera.png" alt="Image" class="img-fluid">
            </div>

            <div class="row">
                <div class="col-lg-8">
                    <div class="subscription-form">
                        {{-- <h3 class="d-flex align-items-center"><span class="me-1"><img src="{{ asset('') }}images/envelope-outline.svg" alt="Image" class="img-fluid"></span><span>Subscribe to Newsletter</span></h3> --}}

                        {{-- <form action="#" class="row g-3">
								<div class="col-auto">
									<input type="text" class="form-control" placeholder="Enter your name">
								</div>
								<div class="col-auto">
									<input type="email" class="form-control" placeholder="Enter your email">
								</div>
								<div class="col-auto">
									<button class="btn btn-primary">
										<span class="fa fa-paper-plane"></span>
									</button>
								</div>
							</form> --}}

                    </div>
                </div>
            </div>

            <div class="mb-5 row g-5">
                <div class="col-lg-4">
                    <div class="mb-4 footer-logo-wrap"><a href="#" class="footer-logo">Agratha<span>.</span></a>
                    </div>
                    <p class="mb-4">Agratha Studio adalah destinasi utama untuk layanan fotografi profesional. Kami
                        mengkhususkan diri dalam mengabadikan momen-momen berharga dengan kualitas tinggi dan sentuhan
                        seni. Dari potret pribadi hingga pemotretan produk komersial, kami menawarkan berbagai layanan
                        yang disesuaikan dengan kebutuhan klien.</p>

                    {{-- <ul class="list-unstyled custom-social">
							<li><a href="#"><span class="fa fa-brands fa-facebook-f"></span></a></li>
							<li><a href="#"><span class="fa fa-brands fa-twitter"></span></a></li>
							<li><a href="#"><span class="fa fa-brands fa-instagram"></span></a></li>
							<li><a href="#"><span class="fa fa-brands fa-linkedin"></span></a></li>
						</ul> --}}
                </div>

                <div class="col-lg-8">
                    <div class="row links-wrap">
                        <div class="col-6 col-sm-6 col-md-4">
                            <h6 class="text-dark"><b>Navbar</b></h6>
                            <ul class="list-unstyled">
                                <li><a href="#">Beranda</a></li>
                                <li><a href="#">Galeri</a></li>
                                <li><a href="#">Paket</a></li>
                                <li><a href="#">Kontak Kami</a></li>
                                <li><a href="#">Tentang Kami</a></li>
                            </ul>
                        </div>

                        <div class="col-6 col-sm-6 col-md-4">
                            <h6 class="text-dark"><b>Paket</b></h6>
                            <ul class="list-unstyled">
                                <li><a href="#">Company Profil</a></li>
                                <li><a href="#">Paket Event</a></li>
                                <li><a href="#">Paket Wedding</a></li>
                            </ul>
                        </div>

                        {{-- <div class="col-6 col-sm-6 col-md-4">
								<h6 class="text-dark"><b>Galeri</b></h6>
								<ul class="list-unstyled">
									<li><a href="#">Company Profil</a></li>
									<li><a href="#">Paket Event</a></li>
									<li><a href="#">Paket Wedding</a></li>
								</ul>
							</div> --}}

                        <div class="col-6 col-sm-6 col-md-4">
                            <h6 class="text-dark"><b>Pemesanan</b></h6>
                            <ul class="list-unstyled">
                                <li><a href="#">Login</a></li>
                                <li><a href="#">Registrasi</a></li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div>

            <div class="border-top copyright">
                <div class="pt-4 row">
                    <div class="col-lg-6">
                        <p class="mb-2 text-center text-lg-start">Copyright &copy;
                            <script>
                                document.write(new Date().getFullYear());
                            </script>. All Rights Reserved.
                            <!-- License information: https://untree.co/license/ -->
                        </p>
                    </div>

                    {{-- <div class="text-center col-lg-6 text-lg-end">
							<ul class="list-unstyled d-inline-flex ms-auto">
								<li class="me-4"><a href="#">Terms &amp; Conditions</a></li>
								<li><a href="#">Privacy Policy</a></li>
							</ul>
						</div> --}}

                </div>
            </div>

        </div>
    </footer>
    <!-- End Footer Section -->

    <script>
        var BASE_URL = "{{ url('') }}";
        var url_assets = "{{ asset('storage') }}";
        var token = $('meta[name="csrf-token"]').attr('content');
    </script>
    <script src="{{ asset('/customer/main.js') }}"></script>
    <script src="{{ asset('') }}js/bootstrap.bundle.min.js"></script>
    <script src="{{ asset('assets_admin') }}/datatable/datatable.min.js"></script>
    <script src="{{ asset('') }}js/tiny-slider.js"></script>
    <script src="{{ asset('') }}js/custom.js"></script>
</body>

</html>
