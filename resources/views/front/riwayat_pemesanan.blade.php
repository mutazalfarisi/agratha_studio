@extends('front.layout.default')

@section('content')
    <!-- Start Hero Section -->
    <div class="hero">
        <div class="container">
            <div class="row justify-content-between">
                <div class="col-lg-5">
                    <div class="intro-excerpt">
                        <h1>Riwayat Pemesanan</h1>
                    </div>
                </div>
                <div class="col-lg-7">
                    <div class="hero-img-wrap">
                        {{-- <img src="{{ asset('') }}images/couch.png" class="img-fluid"> --}}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Hero Section -->


    <div class="untree_co-section" id="pesan-sekarang">
        <div class="container">
            @if (session('success'))
                <div class="alert alert-primary">
                    {{ session('success') }}
                </div>
            @endif

            @if (session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
            @endif

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="row">
                <div class="mb-5 col-lg-12 mb-md-0">
                    <table class="table" id="datatable">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Pemesan</th>
                                <th>Detail Pemesanan</th>
                                <th>Tanggal & Lokasi</th>
                                <th>Pembayaran</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($pemesanan as $item)
                                <tr style="font-size: 12px;">
                                    <td>{{ $loop->iteration }}</td>
                                    <td>
                                        Nama: <b>{{ $item->nama }}</b><br>
                                        Email: <b>{{ $item->email }}</b><br>
                                        Nomor HP: <b>{{ $item->no_hp }}</b>
                                    </td>
                                    <td>
                                        Paket: <b>{{ $item->paket }}</b><br>
                                        Prioritas: <b>{{ $item->refPrioritas->nama_prioritas }}</b><br>
                                        Harga: <b>Rp. {{ rupiah($item->harga) }}</b><br>
                                        Anggaran Transport: <b>Rp. {{ rupiah($item->anggaran_transport) }}</b><br>
                                        Tanggal Pemesanan: <b>{{ tanggal_indonesia($item->created_at) }}</b><br>
                                    </td>
                                    <td>
                                        Tanggal:
                                        <b>{{ formatrangetanggal($item->tanggal_mulai, $item->tanggal_selesai) }}</b><br>
                                        Lokasi: <b>{{ $item->kabupaten }}, {{ $item->alamat }}</b><br>
                                    </td>
                                    <td>
                                        Harga Total : <b>Rp. {{ rupiah($item->harga + $item->anggaran_transport) }}</b><br>
                                        Pembayaran : <b>{{ @$item->refBank->nama_bank }}</b><br>
                                        Nomor : <b>{{ @$item->refBank->no_rek }}</b><br>
                                    </td>
                                    <td>
                                        @if ($item->bukti_transfer == null)
                                            <span class="badge bg-success"><b>Belum Upload Bukti Pembayaran</b></span>
                                        @else
                                            @if ($item->is_verif == '1')
                                                <span class="mt-2 badge bg-success">Verifikasi <br><br>
                                                    Oleh : {{ @$item->admin->nama }} <br><br>
                                                    Tanggal :
                                                    {{ $item->tgl_verif != null ? tanggal_indonesia(@$item->tgl_verif) : '' }}</span>
                                            @elseif($item->is_verif == '0')
                                                <span class="mt-2 badge bg-danger">Belum Verifikasi <br><br>
                                                    Oleh : {{ @$item->admin->nama }} <br><br>
                                                    Tanggal :
                                                    {{ $item->tgl_verif != null ? tanggal_indonesia(@$item->tgl_verif) : '' }}</span>
                                            @else
                                                <span class="badge bg-success"><b>Proses Verifikasi</b></span>
                                            @endif
                                        @endif
                                    </td>
                                    <td>
                                        <div class="dropdown">
                                            <a class="btn btn-secondary dropdown-toggle" href="#" role="button"
                                                id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
                                                Aksi
                                            </a>

                                            <ul class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                                <li><a class="dropdown-item" style="cursor: pointer;"
                                                        onclick="edit('{{ $item->id }}')">Edit</a>
                                                </li>
                                                <li><a class="dropdown-item" style="cursor: pointer;"
                                                        onclick="upload('{{ $item->id }}')">Upload
                                                        Pembayaran</a></li>
                                                @if ($item->is_verif == '0' || $item->is_verif == null)
                                                    <li><a class="dropdown-item" style="cursor: pointer;"
                                                            onclick="hapus('{{ $item->id }}')">Hapus</a></li>
                                                @endif
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-edit" tabindex="-1" aria-labelledby="modal-editLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modal-editLabel">Edit Transaksi</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form action="{{ route('pemesanan.store') }}" method="post" id="form-edit">
                    <div class="modal-body">
                        <div class="text-center loading">Loading...</div>
                        <div class="modal-form">
                            @csrf
                            <div class=" form-group row">
                                <h2 class="mb-3 text-black h3"><b>Form Pemesanan</b></h2>
                            </div>
                            <div class="form-group row">
                                <div class="mt-2 form-group row">
                                    <div class="col-md-4">
                                        <label for="paket" class="text-black">Paket <span
                                                class="text-danger">*</span></label></label>
                                        <input type="text" readonly class="form-control bg-secondary text-light fw-bold"
                                            name="paket" id="paket" value="">
                                        <input type="hidden" class="form-control bg-secondary text-light fw-bold"
                                            name="id_paket" id="id_paket" value="">
                                        <input type="hidden" class="form-control bg-secondary text-light fw-bold"
                                            name="id_transaksi" id="id_transaksi" value="">
                                        <input type="hidden" class="form-control bg-secondary text-light fw-bold"
                                            name="id_user" id="id_user" value="">
                                    </div>
                                    <div class="col-md-4">
                                        <label for="prioritas" class="text-black">Prioritas <span
                                                class="text-danger">*</span></label></label>
                                        <select name="prioritas" id="prioritas" class="form-control" required>
                                            <option value="">Pilih Prioritas</option>
                                            @foreach ($prioritas as $item)
                                                <option value="{{ $item->id }}">{{ $item->nama_prioritas }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <label for="harga" class="text-black">Harga <span
                                                class="text-danger">*</span></label></label>
                                        <input name="harga" id="harga" readonly class="form-control"
                                            placeholder="Rp. ">
                                        </input>
                                    </div>
                                </div>

                                <div class="mt-2 form-group row">
                                    <div class="col-md-4">
                                        <label for="nama" class="text-black">Nama <span
                                                class="text-danger">*</span></label>
                                        <input type="text" class="form-control" placeholder="Masukkan Nama"
                                            id="nama" name="nama" value="">
                                    </div>
                                    <div class="col-md-4">
                                        <label for="email" class="text-black">Email <span
                                                class="text-danger">*</span></label>
                                        <input type="text" placeholder="Masukkan Email" class="form-control"
                                            id="email" name="email" value="">
                                    </div>
                                    <div class="col-md-4">
                                        <label for="no_hp" class="text-black">Nomor HP
                                            <spanclass="text-danger">*</span></label>
                                        <input type="text" placeholder="Masukkan Nomor HP" class="form-control"
                                            id="no_hp" name="no_hp" value="">
                                    </div>
                                </div>

                                <div class="mt-2 form-group row">
                                    <div class="col-md-4">
                                        <label for="provinsi" class="text-black">Provinsi</label>
                                        <select name="provinsi" id="provinsi" class="form-control select2">
                                            <option value="">Pilih Provinsi</option>
                                            @foreach ($provinsi as $item)
                                                <option value="{{ $item->province_id }}">{{ $item->province_name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <label for="kabupaten" class="text-black">Kabupaten</label>
                                        <select name="kabupaten" id="kabupaten" class="form-control select2">
                                            <option value="">Pilih Kabupaten</option>
                                            @foreach ($kabupaten as $item)
                                                <option value="{{ $item->city_id }}">{{ $item->city_name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <label for="kecamatan" class="text-black">Kecamatan</label>
                                        <select name="kecamatan" id="kecamatan" class="form-control select2">
                                            <option value="">Pilih Kecamatan</option>
                                            @foreach ($kecamatan as $item)
                                                <option value="{{ $item->subdistrict_id }}">{{ $item->subdistrict_name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="mt-2 form-group row">
                                    <div class="col-md-12">
                                        <label for="alamat" class="text-black">Alamat<span
                                                class="text-danger">*</span></label>
                                        <textarea name="alamat" id="alamat" class="form-control" cols="30" rows="5"
                                            placeholder="Masukkan Alamat"></textarea>
                                    </div>
                                </div>

                                <div class="mt-2 form-group row">
                                    <div class="col-md-4">
                                        <label for="tanggal_mulai" class="text-black">Tanggal Mulai<span
                                                class="text-danger">*</span></label>
                                        <input type="date" class="form-control" id="tanggal_mulai"
                                            name="tanggal_mulai">
                                    </div>
                                    <div class="col-md-4">
                                        <label for="tanggal_selesai" class="text-black">Tanggal Selesai<span
                                                class="text-danger">*</span></label>
                                        <input type="date" class="form-control" id="tanggal_selesai"
                                            name="tanggal_selesai">
                                    </div>
                                    <div class="col-md-4">
                                        <label for="anggaran_transport" class="text-black">Anggaran Transport<span
                                                class="text-danger">*</span></label>
                                        <input type="text" class="form-control isrupiah" id="anggaran_transport"
                                            name="anggaran_transport" placeholder="Masukkan Anggaran Transport">
                                    </div>
                                </div>

                                <div class="mt-2 form-group row">
                                    <div class="col-md-4">
                                        <label for="bank" class="text-black">Pembayaran<span
                                                class="text-danger">*</span></label>
                                        <select name="bank" id="bank" class="form-control select2">
                                            <option value="">Pilih Pembayaran</option>
                                            @foreach ($bank as $item)
                                                <option value="{{ $item->id }}">{{ $item->nama_bank }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-upload" tabindex="-1" aria-labelledby="modal-uploadLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modal-uploadLabel">Upload Bukti Transaksi</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="text-center loading">Loading...</div>
                <div class="modal-bukti">
                    <form action="{{ route('upload.bukti.store') }}" method="post" enctype="multipart/form-data"
                        id="form-upload">
                        @csrf
                        <div class="modal-body">
                            <div class="p-3 modal-form">
                                <div class="form-group row">
                                    <div class="mt-2 form-group row">
                                        <div class="col-md-12">
                                            <label for="upload" class="text-black">Upload Bukti Transfer <span
                                                    class="text-danger">*</span></label></label>
                                            <input type="file" name="bukti_transfer" class="form-control">
                                            <input type="hidden" class="form-control bg-secondary text-light fw-bold"
                                                name="id_transaksi" id="id_transaksi" value=""><br>
                                            <a href="" target="_blank" class="btn btn-primary"
                                                id="lihat_bukti_file">Lihat
                                                File</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
                                <button type="submit" class="btn btn-primary">Upload</button>
                            </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    </div>

    <script>
        $(document).ready(function() {
            $("#datatable").DataTable({
                'responsive': true
            })
        })

        function edit(id) {
            $("#modal-edit .loading").show();
            $("#modal-edit .modal-form").hide();
            $("#modal-edit").modal('show');
            $.ajax({
                url: "{{ url('/get_pemesanan') }}",
                type: 'get',
                dataType: 'json',
                data: {
                    id: id
                },
                success: function(res) {
                    $("#paket").val(res.paket);
                    $("#id_paket").val(res.id_paket);
                    $("#modal-edit #id_transaksi").val(res.id);
                    $("#id_user").val(res.id_user);
                    $("#prioritas").val(res.prioritas);
                    $("#harga").val(res.harga);
                    $("#nama").val(res.nama);
                    $("#email").val(res.email);
                    $("#no_hp").val(res.no_hp);
                    $("#provinsi").val(res.provinsi_id);
                    $("#kabupaten").val(res.kabupaten_id);
                    $("#kecamatan").val(res.kecamatan_id);
                    $("#alamat").val(res.alamat);
                    $("#tanggal_mulai").val(res.tanggal_mulai);
                    $("#tanggal_selesai").val(res.tanggal_selesai);
                    $("#anggaran_transport").val(res.anggaran_transport);
                    $("#bank").val(res.id_bank);

                    $("#modal-edit .loading").hide();
                    $("#modal-edit .modal-form").show();
                }
            })
        }

        function upload(id) {
            $("#modal-upload").modal('show');
            $("#modal-upload .loading").show();
            $("#modal-upload .modal-bukti").hide();
            $("#modal-upload #lihat_bukti_file").hide();
            $.ajax({
                url: "{{ url('/get_pemesanan') }}",
                type: 'get',
                dataType: 'json',
                data: {
                    id: id
                },
                success: function(res) {
                    $("#modal-upload #id_transaksi").val(res.id);
                    if (res.bukti_transfer != null) {
                        $("#modal-upload #lihat_bukti_file").show();
                        $("#modal-upload #lihat_bukti_file").attr('href', url_assets + '/' + res
                            .bukti_transfer);
                    } else {
                        $("#modal-upload #lihat_bukti_file").hide();
                    }
                    $("#modal-upload .modal-bukti").show();
                    $("#modal-upload .loading").hide();
                }
            })
        }

        function hapus(id) {
            Swal.fire({
                text: 'Apakah Anda Ingin Menghapus Ini?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                cancelButtonText: 'Batal',
                confirmButtonText: 'Ya, Hapus'
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: `/hapus_pemesanan`,
                        type: 'post',
                        dataType: 'json',
                        data: {
                            id: id,
                            _token: token
                        },
                        success: function(res) {
                            if (res.status) {
                                Swal.fire(
                                    'Berhasil',
                                    res.pesan,
                                    'success'
                                )
                            } else {
                                Swal.fire(
                                    'Gagal',
                                    res.pesan,
                                    'error'
                                )
                            }
                            setTimeout(function() {
                                location.reload(true);
                            }, 1000);
                        }
                    });
                }
            })
        }
    </script>
    <script src="{{ asset('/customer/detail_paket.js') }}"></script>
@endsection
