@extends('front.layout.default')

@section('content')
    <!-- Start Hero Section -->
    <div class="hero">
        <div class="container">
            <div class="row justify-content-between">
                <div class="col-lg-5">
                    <div class="intro-excerpt">
                        <h1>Tentang Kami</h1>
                    </div>
                </div>
                <div class="col-lg-7">
                    <div class="hero-img-wrap">
                        {{-- <img src="{{ asset('') }}images/couch.png" class="img-fluid"> --}}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Hero Section -->

    <div class="untree_co-section product-section before-footer-section">
        <div class="container">
            <div class="row">
                <div class="row">
                    <div class="col-lg-6">
                        <img src="{{ asset('assets_login/images/login.jpg'); }} " class="img-fluid" alt="Agratha Studio">
                    </div>
                    <div class="col-lg-6">
                        <h2 class="mb-4">Tentang Kami</h2>
                        <p>
                            Selamat datang di Agratha Studio, platform terbaik untuk jasa pembuatan fotografer dan
                            videografer profesional. Kami bangga menjadi pilihan utama bagi individu, bisnis, dan organisasi
                            yang mencari solusi kreatif dan berkualitas tinggi untuk keperluan fotografi dan videografi
                            mereka.
                        </p>
                        <p>
                            Di Agratha Studio, kami memahami bahwa setiap momen berharga dan setiap proyek memiliki cerita
                            unik yang ingin disampaikan. Dengan tim profesional yang berdedikasi dan peralatan canggih, kami
                            siap membantu Anda mengabadikan momen-momen penting dan membuat konten visual yang mengesankan.
                        </p>
                        <p>
                            Kami menawarkan berbagai layanan, termasuk:
                        </p>
                        <ul>
                            <li>Fotografi pernikahan</li>
                            <li>Fotografi acara dan pesta</li>
                            <li>Fotografi produk dan komersial</li>
                            <li>Videografi pernikahan</li>
                            <li>Videografi acara dan pesta</li>
                            <li>Produksi video promosi dan iklan</li>
                        </ul>
                        <p>
                            Dengan pengalaman bertahun-tahun dan portofolio yang mengesankan, Agratha Studio berkomitmen
                            untuk memberikan hasil terbaik yang sesuai dengan visi dan harapan Anda. Kami menggabungkan
                            seni, kreativitas, dan teknologi untuk menciptakan karya yang tidak hanya indah dilihat, tetapi
                            juga memiliki nilai emosional dan cerita yang kuat.
                        </p>
                        <p>
                            Terima kasih telah mempercayakan momen berharga Anda kepada kami. Mari bekerja sama dan buat
                            sesuatu yang luar biasa!
                        </p>
                    </div>
                </div>
                {{-- @foreach ($dt_paket as $item)
				<div class="mb-5 col-12 col-md-4 col-lg-3 mb-md-0">
					<a class="product-item" href="{{ url('/detail_paket/'. $item->id) }}">
						<img src="{{ asset("/storage/". $item->foto) }}" class="img-fluid product-thumbnail">
						<span class="text-secondary">Paket</span>
						<h3 class="product-title">{{ $item->nama_paket }}</h3>
						<div class="d-flex justify-content-center icon-cross">
							<button class="btn btn-sm btn-dark">Selengkapnya</button>
						</div>
					</a>
				</div> 
			@endforeach --}}
            </div>
        </div>
    </div>
@endsection
