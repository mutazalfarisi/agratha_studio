@extends('front.layout.default')

@section('content')
    <!-- Start Hero Section -->
    <div class="hero">
        <div class="container">
            <div class="row justify-content-between">
                <div class="col-lg-5">
                    <div class="intro-excerpt">
                        <h1>Kontak Kami</h1>
                    </div>
                </div>
                <div class="col-lg-7">
                    <div class="hero-img-wrap">
                        {{-- <img src="{{ asset('') }}images/couch.png" class="img-fluid"> --}}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Hero Section -->

    {{-- <div class="untree_co-section product-section before-footer-section"> --}}
    <div class="container mt-4">
        @if (session('success'))
            <div class="alert alert-primary">
                {{ session('success') }}
            </div>
        @endif

        @if (session('error'))
            <div class="alert alert-danger">
                {{ session('error') }}
            </div>
        @endif

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <p>Jika Anda memiliki pertanyaan atau butuh informasi lebih lanjut, silakan hubungi kami melalui formulir di
            bawah ini.</p>

        <form action="{{ route('store_pesan') }}" method="POST">
            @csrf
            <div class="form-group">
                <label for="name">Nama</label>
                <input type="text" class="form-control" id="nama" name="nama" required placeholder="Masukkan Nama Anda">
            </div>
            <div class="form-group">
                <label for="no_hp">Nomor HP</label>
                <input type="number" class="form-control" id="no_hp" name="no_hp" required placeholder="Masukkan Nomor HP Anda">
            </div>
            <div class="form-group">
                <label for="email">Email</label>
                <input type="email" class="form-control" id="email" name="email" required placeholder="Masukkan Email Anda">
            </div>
            <div class="form-group">
                <label for="message">Pesan</label>
                <textarea class="form-control" id="message" rows="5" name="pesan" required placeholder="Masukkan Pesan Anda"></textarea>
            </div>
            <button type="submit" class="mt-3 btn btn-primary">Kirim</button>
        </form>
    </div>
    {{-- <div class="container">
		  <div class="row">
			@foreach ($dt_paket as $item)
				<div class="mb-5 col-12 col-md-4 col-lg-3 mb-md-0">
					<a class="product-item" href="{{ url('/detail_paket/'. $item->id) }}">
						<img src="{{ asset("/storage/". $item->foto) }}" class="img-fluid product-thumbnail">
						<span class="text-secondary">Paket</span>
						<h3 class="product-title">{{ $item->nama_paket }}</h3>
						<div class="d-flex justify-content-center icon-cross">
							<button class="btn btn-sm btn-dark">Selengkapnya</button>
						</div>
					</a>
				</div> 
			@endforeach
		  </div>
	</div> --}}
    {{-- </div> --}}
@endsection
