@extends('front.layout.default')

@section('content')
    <!-- Start Hero Section -->
    <div class="hero">
        <div class="container">
            <div class="row justify-content-between">
                <div class="col-lg-5">
                    <div class="intro-excerpt">
                        <h1>Galeri</h1>
                    </div>
                </div>
                <div class="col-lg-7">
                    <div class="hero-img-wrap">
                        {{-- <img src="{{ asset('') }}images/camera1.png" class="img-fluid"> --}}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Hero Section -->

    <div class="untree_co-section product-section before-footer-section">
        <div class="container">
            <div class="row row-cols-md-3">
                <div class="mb-4 col-md-4 mb-lg-0">
                    <img src="https://jasafotojogja.com/wp-content/uploads/2023/10/20230604134013_IMG_8267.webp"
                        class="mb-4 rounded w-100 shadow-1-strong" alt="Gallery 4" />
                    <img src="https://jasafotojogja.com/wp-content/uploads/2023/10/IMG_8650.webp"
                        class="mb-4 rounded w-100 shadow-1-strong" alt="Gallery 5" />
                </div>
                <div class="mb-4 col-md-4 mb-lg-0">
                    <iframe class="mb-4 w-100" src="https://www.youtube.com/embed/6xmjrvgpG4M?si=yDY9cLe9cqoFcZN3" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" height="240" allowfullscreen></iframe>
                    <iframe class="mb-4 w-100" src="https://www.youtube.com/embed/6xmjrvgpG4M?si=yDY9cLe9cqoFcZN3" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" height="240" allowfullscreen></iframe>
                    <img src="https://jasafotojogja.com/wp-content/uploads/2023/10/20221015123022_IMG_0282.webp"
                        class="mb-4 rounded w-100 shadow-1-strong" alt="Gallery 8" />
                    <img src="https://jasafotojogja.com/wp-content/uploads/2023/10/20221015123022_IMG_0282.webp"
                        class="mb-4 rounded w-100 shadow-1-strong" alt="Gallery 8" />
                </div>
                <div class="mb-4 col-md-4 mb-lg-0">
                    <img src="https://jasafotojogja.com/wp-content/uploads/2023/10/20230604134013_IMG_8267.webp"
                        class="mb-4 rounded w-100 shadow-1-strong" alt="Gallery 4" />
                    <img src="https://jasafotojogja.com/wp-content/uploads/2023/10/IMG_8650.webp"
                        class="mb-4 rounded w-100 shadow-1-strong" alt="Gallery 5" />
                </div>
            </div>
        </div>
    </div>
@endsection
