@extends('front.layout.default')

@section('content')
	<!-- Start Hero Section -->
	<div class="hero">
		<div class="container">
			<div class="row justify-content-between">
				<div class="col-lg-5">
					<div class="intro-excerpt">
						<h1>Daftar Paket</h1>
					</div>
				</div>
				<div class="col-lg-7">
					<div class="hero-img-wrap">
                        {{-- <img src="{{ asset('') }}images/couch.png" class="img-fluid"> --}}
                    </div>
				</div>
			</div>
		</div>
	</div>
<!-- End Hero Section -->

<div class="untree_co-section product-section before-footer-section">
	<div class="container">
		  <div class="row">
			@foreach ($dt_paket as $item)
				<div class="mb-5 col-12 col-md-4 col-lg-3 mb-md-0">
					<a class="product-item" href="{{ url('/detail_paket/'. $item->id) }}">
						<img src="{{ asset("/storage/". $item->foto) }}" class="img-fluid product-thumbnail">
						<span class="text-secondary">Paket</span>
						<h3 class="product-title">{{ $item->nama_paket }}</h3>
						<div class="d-flex justify-content-center icon-cross">
							<button class="btn btn-sm btn-dark">Selengkapnya</button>
						</div>
					</a>
				</div> 
			@endforeach
		  </div>
	</div>
</div>
@endsection
