@extends('front.layout.default')

@section('content')
    <!-- Start Hero Section -->
    <div class="hero">
        <div class="container">
            <div class="row justify-content-between">
                <div class="col-lg-7">
                    {{-- <div class="intro-excerpt"> --}}
                    <div>
                        {{-- <span class="text-light" style="font-size: 28px;"><b>Detail Paket</b></span><br> --}}
                        {{-- <span class="text-light" style="font-size: 20px;">{{ $row->nama_paket }}</span><br> --}}
                        <span class="text-light" style="font-size: 28px;"><b>{{ $row->nama_paket }}</b></span><br>
                        {{-- <span class="text-light" style="font-size: 20px;">Deskripsi</span><br> --}}
                        <p>{{ $row->deskripsi }}</p>
                    </div>
                    <div class="row">
                        <div class="col-lg-3">
                            <span class="text-light"><b>Sangat Mendesak</b></span><br>
                            <span class="text-light"><b>Rp. {{ rupiah($row->sangat_mendesak) }}</b></span>
                        </div>
                        <div class="col-lg-3">
                            <span class="text-light"><b>Mendesak</b></span><br>
                            <span class="text-light"><b>Rp. {{ rupiah($row->mendesak) }}</b></span>
                        </div>
                        <div class="col-lg-3">
                            <span class="text-light"><b>Biasa</b></span><br>
                            <span class="text-light"><b>Rp. {{ rupiah($row->biasa) }}</b></span>
                        </div>
                        <div class="col-lg-3">
                            <span class="text-light"><b>Tidak Mendesak</b></span><br>
                            <span class="text-light"><b>Rp. {{ rupiah($row->tidak_mendesak) }}</b></span>
                        </div>
                    </div>
                    <div class="mt-4 row">
                        <div class="col-lg-12">
                            <a href="#pesan-sekarang" class="btn btn-primary" style="width:100%;">Pesan Sekarang</a>
                        </div>
                    </div>
                </div>
                <div class="p-3 col-lg-5">
                    {{-- <div class="hero-img-wrap" style="width: 300px;height: 300px;"> --}}
                    <div style="width: 100%;height: 300px;">
                        <img src="{{ asset('/storage/' . $row->foto) }}"
                            style="width: 100%;height: 100%;object-fit: cover;border-radius:20px;" class="img-fluid">
                        {{-- <img src="{{ asset('/storage/'.$row->foto) }}" style="width: 100%;height: 100%;object-fit: cover;border-radius:20px;top: -30px;left: 40px;z-index: 0;opacity: 50%;" class="img-fluid"> --}}
                    </div>
                    {{-- </div> --}}
                </div>
            </div>
        </div>
    </div>
    <!-- End Hero Section -->

    <style>
        /* Add the blur effect */
        .blur-content {
            filter: blur(8px);
            -webkit-filter: blur(8px);
            pointer-events: none;
            /* Disable all interactions */
            user-select: none;
            /* Prevent text selection */
        }

        .untree_co-section {
            display: flex;
            justify-content: center;
            align-items: center;
            /* Ensure the container takes the full viewport height */
        }

        .return-login {
            position: absolute;
            z-index: 99;
            text-align: center;
        }

        .return-login .btn {
            display: inline-block;
            margin-top: 20px;
        }
    </style>

    <div class="untree_co-section" id="pesan-sekarang">
        @if (empty(@$user))
            <div class="return-login">
                <div class="row">
                    <div class="col-lg-12">
                        <span>Anda Harus Login Untuk Melakukan Pemesanan.</span><br>
                        <a href="{{ url('/login') }}" class="btn">Login Sekarang.</a>
                    </div>
                </div>
            </div>
            <div class="blur-content">
        @endif

        <div class="container">
            <div class="row">
                <div class="mb-5 col-md-12 mb-md-0">
                    @if (session('success'))
                        <div class="alert alert-primary">
                            {{ session('success') }}
                        </div>
                    @endif

                    @if (session('error'))
                        <div class="alert alert-danger">
                            {{ session('error') }}
                        </div>
                    @endif

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="p-3 bg-white border p-lg-5">
                        <form action="{{ route('pemesanan.store') }}" method="post">
                            @csrf
                            <div class=" form-group row">
                                <h2 class="mb-3 text-black h3"><b>Form Pemesanan</b></h2>
                            </div>
                            <div class="form-group row">
                                <div class="mt-2 form-group row">
                                    <div class="col-md-4">
                                        <label for="paket" class="text-black">Paket <span
                                                class="text-danger">*</span></label></label>
                                        <input type="text" readonly class="form-control bg-secondary text-light fw-bold"
                                            name="paket" id="paket" value="{{ $row->nama_paket }}">
                                        <input type="hidden" class="form-control bg-secondary text-light fw-bold"
                                            name="id_paket" id="id_paket" value="{{ $row->id }}">
                                        <input type="hidden" class="form-control bg-secondary text-light fw-bold"
                                            name="id_user" id="id_user" value="{{ $user->id }}">
                                    </div>
                                    <div class="col-md-4">
                                        <label for="prioritas" class="text-black">Prioritas <span
                                                class="text-danger">*</span></label></label>
                                        <select name="prioritas" id="prioritas" class="form-control" required>
                                            <option value="">Pilih Prioritas</option>
                                            @foreach ($prioritas as $item)
                                                <option value="{{ $item->id }}">{{ $item->nama_prioritas }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <label for="harga" class="text-black">Harga <span
                                                class="text-danger">*</span></label></label>
                                        <input name="harga" id="harga" readonly class="form-control"
                                            placeholder="Rp. ">
                                        </input>
                                    </div>
                                </div>

                                <div class="mt-2 form-group row">
                                    <div class="col-md-4">
                                        <label for="nama" class="text-black">Nama <span
                                                class="text-danger">*</span></label>
                                        <input type="text" class="form-control" placeholder="Masukkan Nama"
                                            id="nama" name="nama" value="{{ ucwords(@$user->nama) }}">
                                    </div>
                                    <div class="col-md-4">
                                        <label for="email" class="text-black">Email <span
                                                class="text-danger">*</span></label>
                                        <input type="text" placeholder="Masukkan Email" class="form-control"
                                            id="email" name="email" value="{{ @$user->email }}">
                                    </div>
                                    <div class="col-md-4">
                                        <label for="no_hp" class="text-black">Nomor HP <span
                                                class="text-danger">*</span></label>
                                        <input type="text" placeholder="Masukkan Nomor HP" class="form-control"
                                            id="no_hp" name="no_hp" value="{{ @$user->no_hp }}">
                                    </div>
                                </div>

                                <div class="mt-2 form-group row">
                                    <div class="col-md-4">
                                        <label for="provinsi" class="text-black">Provinsi</label>
                                        <select name="provinsi" id="provinsi" class="form-control select2">
                                            <option value="">Pilih Provinsi</option>
                                            @foreach ($provinsi as $item)
                                                <option value="{{ $item->province_id }}">{{ $item->province_name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <label for="kabupaten" class="text-black">Kabupaten</label>
                                        <select name="kabupaten" id="kabupaten" class="form-control select2">
                                            <option value="">Pilih Kabupaten</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <label for="kecamatan" class="text-black">Kecamatan</label>
                                        <select name="kecamatan" id="kecamatan" class="form-control select2">
                                            <option value="">Pilih Kecamatan</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="mt-2 form-group row">
                                    <div class="col-md-12">
                                        <label for="alamat" class="text-black">Alamat<span
                                                class="text-danger">*</span></label>
                                        <textarea name="alamat" id="alamat" class="form-control" cols="30" rows="5"
                                            placeholder="Masukkan Alamat"></textarea>
                                    </div>
                                </div>

                                {{-- <div class="mt-2 form-group row">
                                    <div class="col-md-4">
                                        <label for="email" class="text-black">Email <span
                                                class="text-danger">*</span></label>
                                        <input type="text" placeholder="Masukkan Email" class="form-control"
                                            id="email" name="email" value="{{ @$user->email }}">
                                    </div>
                                    <div class="col-md-4">
                                        <label for="no_hp" class="text-black">Nomor HP <span
                                                class="text-danger">*</span></label>
                                        <input type="text" placeholder="Masukkan Nomor HP" class="form-control"
                                            id="no_hp" name="no_hp" value="{{ @$user->no_hp }}">
                                    </div>
                                    <div class="col-md-4">
                                        <label for="anggaran_transport" class="text-black">Anggaran Transport <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control isrupiah" id="anggaran_transport"
                                            name="anggaran_transport" placeholder="Masukkan Anggaran Transport">
                                    </div>
                                </div> --}}

                                <div class="mt-2 form-group row">
                                    <div class="col-md-4">
                                        <label for="tanggal_mulai" class="text-black">Tanggal Mulai<span
                                                class="text-danger">*</span></label>
                                        <input type="date" class="form-control" id="tanggal_mulai"
                                            name="tanggal_mulai">
                                    </div>
                                    <div class="col-md-4">
                                        <label for="tanggal_selesai" class="text-black">Tanggal Selesai<span
                                                class="text-danger">*</span></label>
                                        <input type="date" class="form-control" id="tanggal_selesai"
                                            name="tanggal_selesai">
                                    </div>
                                    <div class="col-md-4">
                                        <label for="anggaran_transport" class="text-black">Anggaran Transport<span
                                                class="text-danger">*</span></label>
                                        <input type="text" class="form-control isrupiah" id="anggaran_transport"
                                            name="anggaran_transport" placeholder="Masukkan Anggaran Transport">
                                    </div>
                                </div>
                                <div class="mt-2 form-group row">
                                    <div class="col-md-4">
                                        <label for="bank" class="text-black">Pembayaran<span
                                                class="text-danger">*</span></label>
                                        <select name="bank" id="bank" class="form-control select2">
                                            <option value="">Pilih Pembayaran</option>
                                            @foreach ($bank as $item)
                                                <option value="{{ $item->id }}">{{ $item->nama_bank }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="mt-3 col-md-6">
                                <button type="submit" class="btn btn-primary">Kirim</button>
                            </div>
                        </form>
                    </div>
                    <!-- </form> -->
                </div>
            </div>
        </div>
        @if (empty(@$user->nama))
    </div>
    @endif
    </div>

    <script>
        let sangat_mendesak = '{{ $row->sangat_mendesak }}';
        let mendesak = '{{ $row->mendesak }}';
        let biasa = '{{ $row->biasa }}';
        let tidak_mendesak = '{{ $row->tidak_mendesak }}';
    </script>
    <script src="{{ asset('/customer/detail_paket.js') }}"></script>
@endsection
