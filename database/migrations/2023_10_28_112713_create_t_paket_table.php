<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_paket', function (Blueprint $table) {
            $table->id();
            $table->string('nama_paket')->nullable();
            $table->string('deskripsi')->nullable();
            $table->string('foto')->nullable();
            $table->string('sangat_mendesak')->nullable();
            $table->string('mendesak')->nullable();
            $table->string('biasa')->nullable();
            $table->string('tidak_mendesak')->nullable();
            $table->dateTime('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_pakets');
    }
};
