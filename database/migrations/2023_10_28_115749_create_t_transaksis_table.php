<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_transaksi', function (Blueprint $table) {
            $table->id();
            $table->integer('id_user')->nullable();
            $table->string('nama')->nullable();
            $table->string('email')->nullable();
            $table->string('no_hp')->nullable();
            $table->string('id_paket')->nullable();
            $table->string('paket')->nullable();
            $table->string('prioritas')->nullable();
            $table->string('harga')->nullable();
            $table->string('anggaran_transport')->nullable();
            $table->integer('provinsi_id')->nullable();
            $table->string('provinsi')->nullable();
            $table->string('kabupaten')->nullable();
            $table->string('kabupaten_id')->nullable();
            $table->string('kecamatan')->nullable();
            $table->string('kecamatan_id')->nullable();
            $table->text('alamat')->nullable();
            $table->dateTime('tanggal_mulai')->nullable();
            $table->dateTime('tanggal_selesai')->nullable();
            $table->dateTime('deleted_at')->nullable();
            $table->integer('is_admin')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_transaksis');
    }
};
