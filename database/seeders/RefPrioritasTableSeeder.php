<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RefPrioritasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ref_prioritas')->insert([
            ['nama_prioritas' => 'Sangat Mendesak', 'level' => 1, 'created_at' => now(), 'updated_at' => now()],
            ['nama_prioritas' => 'Mendesak', 'level' => 2, 'created_at' => now(), 'updated_at' => now()],
            ['nama_prioritas' => 'Biasa', 'level' => 3, 'created_at' => now(), 'updated_at' => now()],
            ['nama_prioritas' => 'Tidak Mendesak', 'level' => 4, 'created_at' => now(), 'updated_at' => now()],
        ]);
    }
}
