$(document).ready(function() {
    $("#logout").click(function(){
        console.log(token);
        $.ajax({
            url: BASE_URL + '/logout',
            method:'post',
            data:{
                _token:token
            },
            success:function(res){
                location.reload()
            }
        })
    })
});

function formatRupiah(number) {
    // Convert the number to a string
    var numberString = number.toString();

    // Split the number into integer and decimal parts (if any)
    var split = numberString.split(',');
    var integerPart = split[0];
    var decimalPart = split.length > 1 ? ',' + split[1] : '';

    // Regular expression to add a period (.) as thousand separator
    var regex = /(\d+)(\d{3})/;

    // Loop to add thousand separator to the integer part
    while (regex.test(integerPart)) {
        integerPart = integerPart.replace(regex, '$1' + '.' + '$2');
    }

    // Combine the integer part and decimal part
    return 'Rp ' + integerPart + decimalPart;
}


$(document).ready(function() {
    $(".isrupiah").on('keyup', function() {
        this.value = rupiah(this.value);
    });
});

function rupiah(number) {
    // Convert the number to a string
    var numberString = number.replace(/[^,\d]/g, '').toString();

    // Split the number into integer and decimal parts (if any)
    var split = numberString.split(',');
    var integerPart = split[0];
    var decimalPart = split.length > 1 ? ',' + split[1] : '';

    // Regular expression to add a period (.) as thousand separator
    var regex = /(\d+)(\d{3})/;

    // Loop to add thousand separator to the integer part
    while (regex.test(integerPart)) {
        integerPart = integerPart.replace(regex, '$1' + '.' + '$2');
    }

    // Combine the integer part and decimal part
    return 'Rp ' + integerPart + decimalPart;
}