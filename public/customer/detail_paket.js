$(document).ready(function() {
    // $(".select2").select2();

    $("#prioritas").change(function() {
        if ($("#prioritas").val() == '1') {
            $('#harga').val(formatRupiah(sangat_mendesak))
        } else if($("#prioritas").val() == '2'){
            $('#harga').val(formatRupiah(mendesak))
        } else if($("#prioritas").val() == '3'){
            $('#harga').val(formatRupiah(biasa))
        } else if($("#prioritas").val() == '4'){
            $('#harga').val(formatRupiah(tidak_mendesak))
        }
    })

    $("#provinsi").change(function() {
        $("#kabupaten").html("<option>Loading</option>")
        $("#kecamatan").html("<option>Loading</option>")
        $.ajax({
            url: '/get_kabupaten',
            type: 'get',
            dataType: 'json',
            data: {
                province_id: $("#provinsi").val()
            },
            success: function(res) {
                console.log(res);
                $("#kabupaten").empty();
                let kabupaten = '<option value="">-- Pilih Kabupaten --</option>';
                if (res.length > 0) {
                    $.each(res, function(index, kab) {
                        kabupaten += '<option value="' + kab.city_id + '">' +
                            kab.city_name + '</option>';
                    });
                    $("#kabupaten").append(kabupaten);
                    $("#kecamatan").html("<option>-- Pilih Kecamatan --</option>")
                } else {
                    $("#kabupaten").append(
                        '<option value="">Tidak ada kabupaten ditemukan</option>');
                    $("#kecamatan").html("<option>-- Pilih Kecamatan --</option>")
                }
            }
        })
    })

    $("#kabupaten").change(function() {
        $("#kecamatan").html("<option>Loading</option>")
        $.ajax({
            url: '/get_kecamatan',
            type: 'get',
            dataType: 'json',
            data: {
                city_id: $("#kabupaten").val()
            },
            success: function(res) {
                $("#kecamatan").empty();
                let kecamatan = '<option value="">-- Pilih kecamatan --</option>';
                if (res.length > 0) {
                    $.each(res, function(index, kec) {
                        kecamatan += '<option value="' + kec.subdistrict_id +
                            '">' + kec.subdistrict_name + '</option>';
                    });
                    $("#kecamatan").append(kecamatan);
                } else {
                    $("#kecamatan").append(
                        '<option value="">Tidak ada kecamatan ditemukan</option>');
                }
            }
        })
    })
})