$(document).ready(function(){

})

function hapus(dt){
    let id_cart = $(dt).data('id_cart');
    let csrfToken = $('meta[name="csrf-token"]').attr('content');
    Swal.fire({
        text: "Anda yakin ingin menghapus produk ini?",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Batal',
        confirmButtonText: 'Ya, Hapus'
      }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: '/hapus_keranjang',
                type:'post',
                data:{
                    id:id_cart,
                    _token: csrfToken
                },
                dataType:'json',
                success:function(res){
                    if (res) {
                        Swal.fire(
                            'Berhasil!',
                            res.pesan,
                            'success'
                            )
                        load_cart();
                        setTimeout(function() {
                            location.reload(true); // true mengabaikan cache dan memaksa memuat ulang dari server
                        }, 1000);
                    }else{
                        Swal.fire(
                            'Gagal!',
                            res.pesan,
                            'error'
                            )
                    }
                }
            })
        }
      })
}

function checkout(dt){
    let id_cart = $(dt).data('id_cart');
    let csrfToken = $('meta[name="csrf-token"]').attr('content');
    Swal.fire({
        text: "Anda yakin ingin CheckOut produk ini?",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Batal',
        confirmButtonText: 'Ya, Check Out'
      }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: '/checkout_keranjang',
                type:'post',
                data:{
                    id:id_cart,
                    _token: csrfToken
                },
                dataType:'json',
                success:function(res){
                        window.location.href = "/checkout/"+res; // Ganti URL dengan tujuan redirect
                }
            })
        }
    })
}

function terima(dt){
    let id_transaksi = dt;
    let csrfToken = $('meta[name="csrf-token"]').attr('content');
    Swal.fire({
        text: "Anda yakin sudah menerima pesanan ini?",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Batal',
        confirmButtonText: 'Ya, Sudah'
      }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: '/terima_pesanan',
                type:'post',
                data:{
                    id:id_transaksi,
                    _token: csrfToken
                },
                dataType:'json',
                success:function(res){
                    if (res) {
                        Swal.fire(
                            'Berhasil!',
                            res.pesan,
                            'success'
                            )
                        load_cart();
                        setTimeout(function() {
                            location.reload(true); // true mengabaikan cache dan memaksa memuat ulang dari server
                        }, 1000);
                    }else{
                        Swal.fire(
                            'Gagal!',
                            res.pesan,
                            'error'
                            )
                    }
                }
            })
        }
    })
}

function uploadPay(dt){
    $("#id_transaksi").val($(dt).data('id_transaksi'))
    let bukti = $(dt).data('bukti');
    let link = $(dt).data('link');
    let nama_bank = $(dt).data('nama_bank');
    let rekening = $(dt).data('rekening');

    if (bukti != '') {
        $("#bukti").show()
        $("#lihat_bukti").attr('href', link);
    }else{
        $("#bukti").hide()
    }
    $("#pembayaran").html(nama_bank + ' AS Basecampidn'+' Dengan No : '+rekening)

    $("#uploadPay").modal('show')
}
