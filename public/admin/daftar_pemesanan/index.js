$(document).ready(function() {
    load_table();

    $("#terapkan").on('click',function(){
        load_table();
    })
})

function load_table(){
    $('#dataTable').DataTable().destroy();

    table = $('#dataTable').DataTable({
        serverSide: true,
        processing: true,
        ajax: {
            url: dataPemesanan,
            type: 'get',
            data:{
                tanggal_mulai: $("#tanggal_mulai").val(),
                tanggal_selesai: $("#tanggal_selesai").val(),
                tipe : tipe
            },
            dataType: 'json'
        },
        // order: [[2, 'desc']],
        columns: [{
            data: 'DT_RowIndex'
        },
        {
            data: 'nama',
            render: (data, type, row) => {
                var result = `<div style="font-size:12px;">
                    <span>Nama : <b>${row.nama}</b></span><br>
                    <span>Email : <b>${row.email}</b></span><br>
                    <span>Nomor HP : <b>${row.no_hp}</b></span><br>
                </div>`;
                return result;
            }
        },
        {
            data: 'paket',
            render: (data, type, row) => {
                // console.log(row.ref_prioritas.nama_prioritas);
                var result = `<div style="font-size:12px;">
                    <span>Paket : <b>${row.paket}</b></span><br>
                    <span>Prioritas : <b>${row.nama_prioritas}</b></span><br>
                    <span>Harga : <b> Rp ${row.harga}</b></span><br>
                    <span>Anggaran Transport : <b> Rp ${row.anggaran_transport}</b></span><br>
                    <span>Tanggal Pemesanan : <b>${row.tanggal_pemesanan}</b></span><br>
                </div>`;
                return result;
            }
        },
        {
            data: 'paket',
            render: (data, type, row) => {
                var result = `<div style="font-size:12px;">
                    <span>Tanggal : <b>${row.tanggal}</b></span><br>
                    <span>Lokasi : <b>${row.lokasi}</b></span><br>
                </div>`;
                return result;
            }
        },
        {
            data: 'paket',
            render: (data, type, row) => {
                var result = `<div style="font-size:12px;">
                <span>Harga Total : <b> Rp ${row.harga_total}</b></span><br><br>
                <span>Pembayaran : <b>${row.pembayaran}</b></span><br>`;

                if (row.bukti_transfer != null) {
                    result += `<a href="${ imageUrl +'/'+ row.bukti_transfer }" target="_blank" class="btn btn-primary btn-sm">Bukti Pembayaran</a>` 
                }
                result += '</div>'

                return result;
            }
        },
        {
            data: 'paket',
            render: (data, type, row) => {
                var result = ``;
                result += '<div style="font-size:12px;">';
                if (row.bukti_transfer != null && row.is_admin == null ) {
                    result += `<span class="badge bg-success"><b>Upload Bukti Pembayaran</b></span>`;
                }else if(row.bukti_transfer != null && row.is_admin != 1){
                    result += `<span class="badge bg-danger"><b>Bukti Pembayaran Tidak Valid</b></span>`;
                }else{
                    result += `<span class="badge bg-success"><b>Upload Bukti Pembayaran</b></span>`;
                    if (row.is_verif == '1') {
                        result += `<span class="mt-2 badge bg-success">Verifikasi <br><br>
                            Oleh : ${row.nama_admin}<br><br>
                            Tanggal : ${row.tgl_verif != null ? row.tgl_verif:''}</span>`;
                    } else {
                        result += `<span class="mt-2 badge bg-danger">Belum Verifikasi <br><br>
                            Oleh : ${row.nama_admin}<br><br>
                            Tanggal : ${row.tgl_verif != null ? row.tgl_verif:''}</span>`;
                    }
                }
                result += '</div>';
                return result;
            }
        },
        {
            data: 'paket',
            render: (data, type, row) => {
                var result = ``;
                result += '<div style="font-size:12px;">';
                if (row.is_verif == '1') {
                    result += `<button class="btn btn-sm btn-success btn-selesai mb-2"
                    onclick="selesai('${row.id}')">Selesai</button>`;

                    result += `<button class="btn btn-sm btn-danger btn-verifikasi"
                    onclick="verifikasi('${row.id}','${row.bukti_transfer}','batal_verifikasi')">Batalkan
                    Verifikasi</button>`;
                } else {
                    result += `<button class="btn btn-sm btn-warning btn-verifikasi"
                    onclick="verifikasi('${row.id}','${row.bukti_transfer}','verifikasi')">Verifikasi</button>`;
                }

                result += `<a href="https://wa.me/${row.no_hp}" target="_blank"
                                                class="mt-2 btn btn-sm btn-success"><i class='bx bxl-whatsapp'></i> Chat</a>`;

                result += '</div>'; 
                return result;
            }
        },
        ]
    });
}

$(document).ready(function(){
    $("#dataTable").on('click', '.btn-hapus', function(e){
        e.preventDefault(); // Prevent default action (e.g., following the link)

        let id = $(this).data('id');

        Swal.fire({
            text: "Anda akan menghapus data ini, anda yakin?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'Batal',
            confirmButtonText: 'Ya, Hapus'
          }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    url:HapusPaket,
                    type:'post',
                    data:{
                        id:id,
                        _token: token
                    },
                    dataType:'json',
                    success:function(res){
                        if (res) {
                            Swal.fire(
                                'Terhapus!',
                                res.pesan,
                                'success'
                            )
                        } else {
                            Swal.fire(
                                'Gagal!',
                                res.pesan,
                                'error'
                            )
                        }
                        table.ajax.reload();
                    }
                })
            }
          })
        // Lakukan operasi hapus atau tindakan lainnya di sini
        // Contoh: 
        // $.ajax({
        //     url: '/hapus-data/' + id,
        //     method: 'DELETE',
        //     success: function(response) {
        //         console.log('Data berhasil dihapus');
        //     },
        //     error: function(err) {
        //         console.error('Gagal menghapus data:', err);
        //     }
        // });
    });
});

function hapus(id){
  let csrfToken = $('meta[name="csrf-token"]').attr('content');
  Swal.fire({
      text: "Anda yakin ingin menghapus User ini?",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText: 'Batal',
      confirmButtonText: 'Ya, Hapus'
    }).then((result) => {
      if (result.isConfirmed) {
          $.ajax({
              url: '/admin/hapus_admin',
              type:'post',
              data:{
                  id:id,
                  _token: csrfToken
              },
              dataType:'json',
              success:function(res){
                  if (res) {
                      Swal.fire(
                          'Berhasil!',
                          res.pesan,
                          'success'
                          )
                        // load_table();
                      setTimeout(function() {
                          location.reload(true); // true mengabaikan cache dan memaksa memuat ulang dari server
                      }, 1000);
                  }else{
                      Swal.fire(
                          'Gagal!',
                          res.pesan,
                          'error'
                          )
                  }
              }
          })
      }
    })
}

function verifikasi(id, foto, tipe) {
    if (foto == null || foto == '') {
        Swal.fire(
            'Gagal!',
            'Belum Upload Bukti Transfer.',
            'error'
        )
    } else {
        if (tipe == 'verifikasi') {
            var status = 'verifikasi';
        } else {
            var status = 'batalkan verifikasi';
        }

        Swal.fire({
            text: "Anda yakin ingin " + status + " data ini?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'Batal',
            confirmButtonText: 'Ya, Simpan'
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    url: '/admin/verifikasi_data',
                    type: 'post',
                    data: {
                        id: id,
                        _token: token
                    },
                    dataType: 'json',
                    success: function(res) {
                        if (res) {
                            Swal.fire(
                                'Berhasil!',
                                res.pesan,
                                'success'
                            )
                            setTimeout(function() {
                                location.reload(
                                    true
                                ); // true mengabaikan cache dan memaksa memuat ulang dari server
                            }, 1000);
                        } else {
                            Swal.fire(
                                'Gagal!',
                                res.pesan,
                                'error'
                            )
                        }
                    }
                })
            }
        })
    }
}

function selesai(id) {
        Swal.fire({
            text: "Anda yakin ingin menyelesaikan data ini?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'Batal',
            confirmButtonText: 'Ya, Simpan'
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    url: '/admin/verifikasi_data_selesai',
                    type: 'post',
                    data: {
                        id: id,
                        _token: token
                    },
                    dataType: 'json',
                    success: function(res) {
                        if (res) {
                            Swal.fire(
                                'Berhasil!',
                                res.pesan,
                                'success'
                            )
                            setTimeout(function() {
                                location.reload(
                                    true
                                ); // true mengabaikan cache dan memaksa memuat ulang dari server
                            }, 1000);
                        } else {
                            Swal.fire(
                                'Gagal!',
                                res.pesan,
                                'error'
                            )
                        }
                    }
                })
            }
        })
}
