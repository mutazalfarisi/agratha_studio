$(document).ready(function() {
    load_table();
})

function load_table(){
    table = $('#dataTable').DataTable({
        serverSide: true,
        processing: true,
        ajax: {
            url: dataPaket,
            type: 'get',
            dataType: 'json'
        },
        order: [[2, 'desc']],
        columns: [{
            data: 'DT_RowIndex'
        },
        {
            data: 'nama_paket',
        },
        {
            data: 'deskripsi',
        },
        {
            data: 'foto',
            render: (data, type, row) => {
                var result = `<img src="`+imageUrl+`/${data}" style="width:100px;">`;
                return result;
            }
        },
        {
            data: '',
            render: (data, type, row) => {
                var result = `
                <span>Sangat Mendesak : <b> Rp ${Rupiah(row.sangat_mendesak)}</b></span><br>
                <span>Mendesak : <b> Rp ${Rupiah(row.mendesak)}</b></span><br>
                <span>Biasa : <b> Rp ${Rupiah(row.biasa)}</b></span><br>
                <span>Tidak Mendesak : <b> Rp ${Rupiah(row.tidak_mendesak)}</b></span><br>
                `;
                return result;
            }
        },
        {
            data: '',
            render: (data, type, row) => {
                let button = '';
                button += `<div class="mb-1 text-center">
                    <button class="btn btn-danger btn-hapus" data-id="${row.id}"><i class="bx bx-trash"></i></button>
                    <a href="${BASE_URL +'/admin/edit_paket/'+row.id}" class="btn btn-warning" ><i class="bx bx-edit"></i></a>
                </div>`;
                return button;
            }
        },
        ]
    });
}

$(document).ready(function(){
    $("#dataTable").on('click', '.btn-hapus', function(e){
        e.preventDefault(); // Prevent default action (e.g., following the link)

        let id = $(this).data('id');

        Swal.fire({
            text: "Anda akan menghapus data ini, anda yakin?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'Batal',
            confirmButtonText: 'Ya, Hapus'
          }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    url:HapusPaket,
                    type:'post',
                    data:{
                        id:id,
                        _token: token
                    },
                    dataType:'json',
                    success:function(res){
                        if (res) {
                            Swal.fire(
                                'Terhapus!',
                                res.pesan,
                                'success'
                            )
                        } else {
                            Swal.fire(
                                'Gagal!',
                                res.pesan,
                                'error'
                            )
                        }
                        table.ajax.reload();
                    }
                })
            }
          })
        // Lakukan operasi hapus atau tindakan lainnya di sini
        // Contoh: 
        // $.ajax({
        //     url: '/hapus-data/' + id,
        //     method: 'DELETE',
        //     success: function(response) {
        //         console.log('Data berhasil dihapus');
        //     },
        //     error: function(err) {
        //         console.error('Gagal menghapus data:', err);
        //     }
        // });
    });
});

function hapus(id){
  let csrfToken = $('meta[name="csrf-token"]').attr('content');
  Swal.fire({
      text: "Anda yakin ingin menghapus User ini?",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText: 'Batal',
      confirmButtonText: 'Ya, Hapus'
    }).then((result) => {
      if (result.isConfirmed) {
          $.ajax({
              url: '/admin/hapus_admin',
              type:'post',
              data:{
                  id:id,
                  _token: csrfToken
              },
              dataType:'json',
              success:function(res){
                  if (res) {
                      Swal.fire(
                          'Berhasil!',
                          res.pesan,
                          'success'
                          )
                        // load_table();
                      setTimeout(function() {
                          location.reload(true); // true mengabaikan cache dan memaksa memuat ulang dari server
                      }, 1000);
                  }else{
                      Swal.fire(
                          'Gagal!',
                          res.pesan,
                          'error'
                          )
                  }
              }
          })
      }
    })
}
