$(document).ready(function() {
    load_table();
    $("#btn-tambah").click(function(){
      $("#modal-tambah").modal('show')
    })
})

function load_table(){
    $('#dataTable').DataTable().destroy();
    $('#dataTable').DataTable({
      responsive: true
    });
}

function hapus(id){
  let csrfToken = $('meta[name="csrf-token"]').attr('content');
  Swal.fire({
      text: "Anda yakin ingin menghapus User ini?",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText: 'Batal',
      confirmButtonText: 'Ya, Hapus'
    }).then((result) => {
      if (result.isConfirmed) {
          $.ajax({
              url: '/admin/hapus_admin',
              type:'post',
              data:{
                  id:id,
                  _token: csrfToken
              },
              dataType:'json',
              success:function(res){
                  if (res) {
                      Swal.fire(
                          'Berhasil!',
                          res.pesan,
                          'success'
                          )
                        // load_table();
                      setTimeout(function() {
                          location.reload(true); // true mengabaikan cache dan memaksa memuat ulang dari server
                      }, 1000);
                  }else{
                      Swal.fire(
                          'Gagal!',
                          res.pesan,
                          'error'
                          )
                  }
              }
          })
      }
    })
}
